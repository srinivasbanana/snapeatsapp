package app5.test5.bananaapps.snapeatsapp.activity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Banana on 14-Aug-17.
 */

public class ShopAddress implements Parcelable {

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getScloseingtime() {
        return scloseingtime;
    }

    public void setScloseingtime(String scloseingtime) {
        this.scloseingtime = scloseingtime;
    }

    public String getSlat() {
        return slat;
    }

    public void setSlat(String slat) {
        this.slat = slat;
    }

    public String getSlong() {
        return slong;
    }

    public void setSlong(String slong) {
        this.slong = slong;
    }

    public String getSopeningtime() {
        return sopeningtime;
    }

    public void setSopeningtime(String sopeningtime) {
        this.sopeningtime = sopeningtime;
    }

    public double getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getSaid() {
        return said;
    }

    public void setSaid(int said) {
        this.said = said;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getUastatus() {
        return uastatus;
    }

    public void setUastatus(int uastatus) {
        this.uastatus = uastatus;
    }

    @SerializedName("address1")
    @Expose
    private String address1;

    @SerializedName("address2")
    @Expose
    private String address2;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("pin")
    @Expose
    private String pin;

    @SerializedName("scloseingtime")
    @Expose
    private String scloseingtime;

    @SerializedName("slat")
    @Expose
    private String slat;

    @SerializedName("slong")
    @Expose
    private String slong;

    @SerializedName("sopeningtime")
    @Expose
    private String sopeningtime;

    @SerializedName("phone")
    @Expose
    private double phone;

    @SerializedName("said")
    @Expose
    private int said;

    @SerializedName("sid")
    @Expose
    private int sid;

    @SerializedName("uastatus")
    @Expose
    private int uastatus;


    public static final Creator<ShopAddress> CREATOR = new Creator<ShopAddress>() {
        @Override
        public ShopAddress createFromParcel(Parcel in) {
            return new ShopAddress(in);
        }

        @Override
        public ShopAddress[] newArray(int size) {
            return new ShopAddress[size];
        }
    };

    protected ShopAddress(Parcel in) {
        address1 = in.readString();
        address2 = in.readString();
        city = in.readString();
        phone = in.readDouble();
        pin=in.readString();
        said=in.readInt();
        scloseingtime=in.readString();
        sid=in.readInt();
        slat=in.readString();
        slong=in.readString();
        sopeningtime=in.readString();
        uastatus=in.readInt();

    }

    protected ShopAddress(double d,String address1,String address2,String city,double phone,String pin,int said,String scloseingtime,int sid,String slat,String slong,String sopeningtime,int uastatus){
        this.address1=address1;
        this.address1=address1;
        this.address2=address2;
        this.city=city;
        this.phone=phone;
        this.pin=pin;
        this.said=said;
        this.scloseingtime=scloseingtime;
        this.sid=sid;
        this.slat=slat;
        this.slong = slong;
        this.sopeningtime = sopeningtime;
        this.uastatus=uastatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address1);
        dest.writeString(address1);
        dest.writeString(city);
        dest.writeDouble(phone);
        dest.writeString(pin);
        dest.writeInt(said);
        dest.writeString(scloseingtime);
        dest.writeInt(sid);
        dest.writeString(slat);
        dest.writeString(slong);
        dest.writeString(sopeningtime);
        dest.writeInt(uastatus);
    };
}
