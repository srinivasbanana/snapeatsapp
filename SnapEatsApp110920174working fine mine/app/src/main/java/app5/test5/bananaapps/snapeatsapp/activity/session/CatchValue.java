package app5.test5.bananaapps.snapeatsapp.activity.session;

/**
 * Created by Android on 1/22/2016.
 */
public class CatchValue {

/*
    public static final String MENU_LIST = "menu_list";

    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";

    public static final String SHOP_ID = "shop_id";
    public static final String USER_ID = "user_id";*/

    public static final String PRODUCT_BRAND = "product_brand";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_QUANTITY = "product_quantity";
    public static final String PRODUCT_PRICE = "product_price";
    public static final String PRODUCT_DESC = "product_desc";
    public static final String PRODUCT_BASE_IMAGE = "product_Base_image";

    public static final String Delivary_adrs0="Delivary_adrs0";
    public static final String Delivary_adrs1="Delivary_adrs1";
    public static final String Delivary_location="Delivary_location";
    public static final String Delivary_pincode="Delivary_pincode";
    public static final String Delivary_country="Delivary_country";

    public static final String MENU_LIST = "menu_list";

    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String PASSWORD = "password";

    public static final String SHOP_ID = "shop_id";
    public static final String USER_ID = "user_id";
    public static final String Address1 = "address1";
    public static final String Address2 = "address2";

    public static final String ChangeAddress1 = "change_address1";
    public static final String ChangeAddress2 = "change_address2";
    public static final String ChangeLocation = "change_location";
    public static final String ChangePincode = "change_pincode";

    public static final String AddressChanged = "address_changed";
    public static final String DRIVER_TIP = "driver_tip";
    public static final String CHECKOUT_ADDRESS = "checkout_from";

    public static final String CardName = "CardName";
    public static final String CardNumber = "CardNumber";
    public static final String CardMonth = "CardMonth";
    public static final String CardExpYear = "CardExpYear";

    public static final String VERSION_ID = "version_id";
    public static final String DEVICE_ID = "device_id";

    public static final String SELECTED_DATE = "selected_date";
    public static final String TEMPLE_NAME = "temple_name";
    public static final String TEMPLE_ID = "temple_id";
    public static final String TEMPLE_CODE = "temple_code";

    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_TYPE = "room_type";

    public static final String TRUST_NAME = "trust_name";
    public static final String TRUST_ID = "trust_id";


    public static final String TIME_SLOTS = "time_slots";
    public static final String PRE_COUNTS = "pre_count";
    public static final String SLOT_IDS = "slot_ids";
    public static final String SLOT_TIME = "slot_time";
    public static final String SLOT_ID = "slot_id";
    public static final String ROOM_ID = "room_id";
    public static final String NO_OF_TICKETS = "num_tickets";
    public static final String TOTAL_COST = "total_cost";

    public static final String ORDER_FROM = "order_from";

    public static final String ALL_DATA = "all_inform";
    public static final String OS_TYPE = "os_type";
    public static final String BOOK_FOR_STATUS = "gender_value";
    public static final String PILGRIM_DETAILS = "pilgrim_details";


    public static final String OCCASION_NAME = "occasion";
    public static final String OCCASION_ID = "occasion_id";
    public static final String CALENDAR_ID = "calendar_id";
    public static final String ROOM_QTY = "room_qty";
    public static final String CART_COUNT = "cart_count";
    public static final String ADDRESS1 = "address1";
    public static final String ADDRESS2 = "address2";
    public static final String COUNTRY_ID = "country";
    public static final String STATE_ID = "state";
    public static final String DISTRICT_ID = "district";
    public static final String CITY_ID = "city";
    public static final String MANDAL = "mandal";
    public static final String VILLAGE = "village";
    public static final String ZIPCODE = "zipcode";

    public static final String IMAGE_DATA = "image_data";
    public static final String USER_AUTH_MODE = "auth_mode";
    public static final String OPEN_SERVICE = "open_service";
    public static final String PERSON_ALLOWED = "person_allowed";
    public static final String DISTRICT_NAME = "district_name";
    public static final String CITY_NAME = "city_name";
    public static final String DEITIES_NAME = "deities_name";
    public static final String CART_EDIT = "cart_edit";
    public static final String CART_EDIT_DETAILS = "cart_edit_details";
    public static final String CART_ID = "cart_id";
    public static final String CART_ITEM_ID = "cart_item_id";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String PROFILE_NAME = "profile_name";
    public static final String WEB_LINK = "link";
    public static final String WEB_ACTIVITY_TITLE = "web_title";
    public static final String ORDER_ID = "order_id";
    public static final String ORDER_UNIQUE_ID = "order_unique_id";
    public static final String DEVICE_IP = "ip_address";
    public static final String PAYMENT_TOKEN = "payment_token";
    public static final String PAYMENT_RESPONSE_URL = "response_url";
    public static final String VIEW_PILGRIM_FROM_PRE_CONF = "view_pilgrim_from_pre_conf";
    public static final String LANGUAGE = "language";
    public static final String CART_BACK_STATUS = "cart_back_status";
    public static final String USER_PIC = "user_pic";
    public static final String OSERVICE_ID = "oservice_id";
    public static final String H_PERSON_NAME = "h_person_name";
    public static final String H_PERSON_MOBILE = "h_person_mobile";
    public static final String H_PERSON_EMAIL = "h_person_email";
    public static final String ORDER_ITEM_ID = "order_item_id";
    public static final String APP_LANGUAGE ="app_Lang" ;
}
