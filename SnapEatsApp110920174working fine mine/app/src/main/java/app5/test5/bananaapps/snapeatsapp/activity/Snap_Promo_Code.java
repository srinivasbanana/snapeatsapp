package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;

/**
 * Created by Banana on 7/11/2017.
 */

public class Snap_Promo_Code extends BaseActivity {
    EditText promoCode;
    Button apply;
    TextView toolbartitle;
    String intent;
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.snap_promo_code);
        promoCode = (EditText)findViewById(R.id.promoCode);
        apply =(Button)findViewById(R.id.apply);
        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);
        intent=getIntent().getStringExtra("from_class");


        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor2 = settings.edit();
                editor2.putString("promoCode",promoCode.getText().toString());
                editor2.commit();

                Intent promo_code = new Intent(Snap_Promo_Code.this, Basket.class);
                promo_code.putExtra("from_class", intent);
                startActivity(promo_code);

              //  startActivity(new Intent(Snap_Promo_Code.this, Basket.class));
               // finish();
            }
        });
    }
}
