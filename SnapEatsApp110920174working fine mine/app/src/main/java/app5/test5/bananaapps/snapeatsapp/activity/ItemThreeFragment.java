/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    Â Â  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package app5.test5.bananaapps.snapeatsapp.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.CameraCapture;
import app5.test5.bananaapps.snapeatsapp.activity.payment.PaymentActivity;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class ItemThreeFragment extends Fragment {
    Toolbar toolbar;
    TextView toolbartitle;
    ImageView cam_button,photo,backarrow,photo2,photo3;
    LinearLayout capturedata,capturedataResultBasket;
    RelativeLayout capturedataResult;
    Button save,cancel,checkOut;
    private boolean permission;
    private static final int PERMISSION_REQUEST_CODE = 200;
    static final int CAM_REQUEST = 1;
    private String userChooseTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String profileImg;
    ProgressDialog progressDialog;
    ConnectionDetector cd;
    Boolean isInternetPresent = false;
    EditText productBrand,productName,productQuantity,productPrice,productDis;
    private String productBrandString,productNameString,productQuantityString,productPriceString,productDisString;
    String userId;
    public static ItemThreeFragment newInstance() {
        ItemThreeFragment fragment = new ItemThreeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v;
        v=inflater.inflate(R.layout.fragment_item_three, container, false);

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectionAvailable();

        toolbar = (Toolbar)v.findViewById(R.id.toolbar);
        toolbartitle = (TextView) v.findViewById(R.id.toolbar_title);
        cam_button = (ImageView) v.findViewById(R.id.cam_button);
        photo = (ImageView) v.findViewById(R.id.photo);
        photo2 = (ImageView) v.findViewById(R.id.photo2);
        backarrow = (ImageView) v.findViewById(R.id.backArrow);
        capturedata = (LinearLayout)v.findViewById(R.id.capturedata);
        capturedataResultBasket = (LinearLayout)v.findViewById(R.id.capturedataResultBasket);
        capturedataResult = (RelativeLayout)v.findViewById(R.id.capturedataResult);
        save = (Button)v.findViewById(R.id.save);
        cancel = (Button)v.findViewById(R.id.cancel);
        checkOut = (Button)v.findViewById(R.id.checkOut);
        backarrow = (ImageView)v.findViewById(R.id.backArrow);
        productBrand=(EditText) v.findViewById(R.id.brand);
        productName=(EditText) v.findViewById(R.id.produt);
        productQuantity=(EditText) v.findViewById(R.id.quantity);
        productPrice=(EditText) v.findViewById(R.id.etEmail);
        productDis=(EditText) v.findViewById(R.id.describeProduct);
        userId = (String) Cache.getData(CatchValue.USER_ID, getActivity());


        Typeface custom_font2 = Typeface.createFromAsset(getActivity().getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);
        cam_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {getResources().getString(R.string.text_take_photo), getResources().getString(R.string.text_choose_from_gallery)};
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                builder.setTitle(getResources().getString(R.string.text_add_photo));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        boolean result = CameraCapture.checkPermission(getActivity());

                        if (items[item].equals(getResources().getString(R.string.text_take_photo))) {
                            userChooseTask = getResources().getString(R.string.text_take_photo);
                            if (result)
                                cameraIntent();
                        } else if (items[item].equals(getResources().getString(R.string.text_choose_from_gallery))) {
                            userChooseTask = getResources().getString(R.string.text_choose_from_gallery);
                            if (result)
                                galleryIntent();
                        }
                    }
                });
                builder.show();
            }


        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photo2.buildDrawingCache();
                Bitmap bitmap =  photo2.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                byte[] b = baos.toByteArray();
                profileImg = Base64.encodeToString(b, Base64.DEFAULT);

                productBrandString=productBrand.getText().toString();
                productNameString=productName.getText().toString();
                productQuantityString=productQuantity.getText().toString();
                productPriceString=productPrice.getText().toString();
                productDisString=productDis.getText().toString();


                Cache.putData(CatchValue.PRODUCT_BRAND, getActivity(), productBrandString, Cache.CACHE_LOCATION_DISK);
                Cache.putData(CatchValue.PRODUCT_NAME, getActivity(), productNameString, Cache.CACHE_LOCATION_DISK);
                Cache.putData(CatchValue.PRODUCT_QUANTITY, getActivity(), productQuantityString, Cache.CACHE_LOCATION_DISK);
                Cache.putData(CatchValue.PRODUCT_PRICE, getActivity(), productPriceString, Cache.CACHE_LOCATION_DISK);
                Cache.putData(CatchValue.PRODUCT_DESC, getActivity(), productDisString, Cache.CACHE_LOCATION_DISK);
                Cache.putData(CatchValue.PRODUCT_BASE_IMAGE, getActivity(), profileImg, Cache.CACHE_LOCATION_DISK);
                Cache.putData(CatchValue.USER_ID, getActivity(), userId, Cache.CACHE_LOCATION_DISK);

                if(TextUtils.isEmpty(userId)&& userId==null)
                {

                    Intent cardPay= new Intent(getActivity(), Login.class);
                    cardPay.putExtra("fromProduct", "fromPhoto");
                    startActivity(cardPay);
                }
                else {
                    Intent cardPay = new Intent(getActivity(), PaymentActivity.class);
                   /* Cache.putData(CatchValue.PRODUCT_BRAND, getActivity(), productBrandString, Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PRODUCT_NAME, getActivity(), productNameString, Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PRODUCT_QUANTITY, getActivity(), productQuantityString, Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PRODUCT_PRICE, getActivity(), productPriceString, Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PRODUCT_DESC, getActivity(), productDisString, Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PRODUCT_BASE_IMAGE, getActivity(), profileImg, Cache.CACHE_LOCATION_DISK);*/
                    cardPay.putExtra("fromProduct", "fromProduct");
                    startActivity(cardPay);
                }
            }
        });

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbartitle.setText("Photo-order");
                capturedataResult.setVisibility(View.VISIBLE);
                capturedataResultBasket.setVisibility(View.INVISIBLE);
                backarrow.setVisibility(View.INVISIBLE);
            }
        });

        return v;

    }


    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.text_select_file)), SELECT_FILE);
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean read = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean write = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    if (read && write)
                        Toast.makeText(getContext(), "Permission: granted Successfully", Toast.LENGTH_SHORT);
                    else {
                        Toast.makeText(getContext(), "Permission Denied, Please allow permission.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }



    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".PNG");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        photo2.setImageBitmap(thumbnail);
        capturedata.setVisibility(View.INVISIBLE);
        capturedataResult.setVisibility(View.VISIBLE);
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        photo2.setImageBitmap(bm);
        capturedata.setVisibility(View.INVISIBLE);
        capturedataResult.setVisibility(View.VISIBLE);
    }
}

