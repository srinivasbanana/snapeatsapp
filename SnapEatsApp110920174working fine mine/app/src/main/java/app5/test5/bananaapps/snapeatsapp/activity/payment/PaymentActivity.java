package app5.test5.bananaapps.snapeatsapp.activity.payment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.AddressChange;
import app5.test5.bananaapps.snapeatsapp.activity.Basket;
import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.CardDetails;
import app5.test5.bananaapps.snapeatsapp.activity.HomePage;
import app5.test5.bananaapps.snapeatsapp.activity.ItemOneFragmentTabsView;
import app5.test5.bananaapps.snapeatsapp.activity.OrderPlaced;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;
import app5.test5.bananaapps.snapeatsapp.activity.SubCategory;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;


public class PaymentActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    private DependencyHandler mDependencyHandler;
    EditText userName,cardNumber,month,year,cvv;
    String cardString,monthString,yearString,cvvString;
    Button payNow,cancel;
    ImageView backArrow;
    TextView textView;
    BucketDB bucketDB;
    String userId;
    String paymentToken;
    Double totalAmount=0.0;
    String itemId,itemQunt,itemPrice;
    JSONObject itemObject,resultJsonObject;
    JSONArray itemArray;
    ProgressDialog progressDialog;
    private List<SubCategoryList> savedQty;
    String add1,add2,loc,pcode,county;
    int shopId=0,driver_tip=0;
    String shopid;
    String totalamount,order_from;
    Card card;
    private String productBrandString,productNameString,productQuantityString,productPriceString,productDisString,productBaseImage;
    private String product;
    String s1,s2,s3,s4;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);
        userName=(EditText)findViewById(R.id.txt_username);
        cardNumber=(EditText)findViewById(R.id.txt_cardNumber);
        month=(EditText)findViewById(R.id.txt_month);
        year=(EditText)findViewById(R.id.year);
        cvv=(EditText)findViewById(R.id.txt_cvv);
        payNow=(Button)findViewById(R.id.PayNow);
        cancel=(Button)findViewById(R.id.cancel);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        textView = (TextView) findViewById(R.id.card_details_show);

        bucketDB=new BucketDB(PaymentActivity.this);
        savedQty=bucketDB.getAllDetails();
      //  if(!TextUtils.isEmpty(userId)){
            userId = (String) Cache.getData(CatchValue.USER_ID, PaymentActivity.this);
      //  }
      //  Toast.makeText(this, "userid payment "+userId, Toast.LENGTH_SHORT).show();

        totalAmount=(Double)Cache.getData(CatchValue.TOTAL_COST, PaymentActivity.this);

       /* if(totalamount==null){
            totalAmount=0.0;
        }
        else{
            totalAmount=Double.parseDouble(totalamount);
        }*/
        product=getIntent().getStringExtra("fromProduct");

        if(!product.equalsIgnoreCase("fromProduct")) {
            shopId = (Integer) Cache.getData(CatchValue.SHOP_ID, PaymentActivity.this);
        }
        productBrandString = (String) Cache.getData(CatchValue.PRODUCT_BRAND, PaymentActivity.this);
        productNameString = (String) Cache.getData(CatchValue.PRODUCT_NAME, PaymentActivity.this);
        productQuantityString = (String) Cache.getData(CatchValue.PRODUCT_QUANTITY, PaymentActivity.this);
        productPriceString = (String) Cache.getData(CatchValue.PRODUCT_PRICE, PaymentActivity.this);
        productDisString = (String) Cache.getData(CatchValue.PRODUCT_DESC, PaymentActivity.this);
        productBaseImage = (String) Cache.getData(CatchValue.PRODUCT_BASE_IMAGE, PaymentActivity.this);
        driver_tip = (Integer) Cache.getData(CatchValue.DRIVER_TIP, PaymentActivity.this);
       /* if(shopid==null){
            shopId=0;
        }
        else{
            shopId=Integer.parseInt(shopid);
        }*/
        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);
     /*   add1 = settings.getString("adrs0", "");
        add2 = settings.getString("adrs1", "");
        loc = settings.getString("locality", "");
        pcode = settings.getString("postalcode", "");
        county = settings.getString("countryname", "");*/

        add1 = (String) Cache.getData(CatchValue.Delivary_adrs0, PaymentActivity.this);
        add2 = (String) Cache.getData(CatchValue.Delivary_adrs1, PaymentActivity.this);
        loc =(String) Cache.getData(CatchValue.Delivary_location, PaymentActivity.this);
        pcode =(String) Cache.getData(CatchValue.Delivary_pincode, PaymentActivity.this);
        county = (String) Cache.getData(CatchValue.Delivary_country, PaymentActivity.this);

        product=getIntent().getStringExtra("fromProduct");
        itemObject=new JSONObject();

        order_from = (String) Cache.getData(CatchValue.ORDER_FROM, PaymentActivity.this);

        //  mDependencyHandler = new DependencyHandler(this, (CardInputWidget) findViewById(R.id.card_input_widget), (ListView) findViewById(R.id.listview));

        Button saveButton = (Button) findViewById(R.id.save);
        cardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String string = charSequence.toString();
                if(string.trim().length()==2){
                    year.requestFocus();
                    cardValid();
                }
                else {
                    month.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String string = charSequence.toString();
                if(string.trim().length()==2){
                    cvv.requestFocus();
                    cardValid();
                }
                else {
                    year.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String string = charSequence.toString();
                if(string.trim().length()==3){
                    cardValid();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        s1 = (String) Cache.getData(CatchValue.CardName, PaymentActivity.this);
        s2 = (String) Cache.getData(CatchValue.CardNumber, PaymentActivity.this);
        s3 = (String) Cache.getData(CatchValue.CardMonth, PaymentActivity.this);
        s4 = (String) Cache.getData(CatchValue.CardExpYear, PaymentActivity.this);

        userName.setText(s1);
        cardNumber.setText(s2);
        month.setText(s3);
        year.setText(s4);


        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(cardString)){
                    cardNumber.setError("Please Enter Card Number");
                    cardNumber.requestFocus();
                }
                else if(TextUtils.isEmpty(monthString)){
                    month.setError("Please Enter Month");
                    month.requestFocus();
                }
                else if(TextUtils.isEmpty(yearString)){
                    year.setError("Please Enter Year");
                    year.requestFocus();
                }
                else if(TextUtils.isEmpty(cvvString)){
                    cvv.setError("Please Enter CVV Number");
                    cvv.requestFocus();
                }
                else{
                    //Toast.makeText()
                    showProgressDialog();
                    new Stripe(getApplicationContext()).createToken(
                            card,
                            "pk_test_OQ3Me8UtSTNp9q7XRK7nlYBB",
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    dismissProgressDialog();
                                    if(!TextUtils.isEmpty(token.getId())) {
                                        if(product.equalsIgnoreCase("fromProduct")){
                                            new PhotoOrder().execute(token.getId(),add1,add2,pcode,loc);
                                        }
                                        else {
                                            new PaymentTask().execute(token.getId(), add1, add2, String.valueOf(shopId), loc, pcode, county);
                                        }
                                    }
                                    else{
                                        showToastMessage("Please try again");
                                    }
                                }
                                public void onError(Exception error) {
                                    Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                }
                            });

                }

            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // mDependencyHandler.attachRxTokenController(saveRxButton);

    }

    private void cardValid() {

        cardString=cardNumber.getText().toString();
        cardString.replace(" ","");
        monthString=month.getText().toString();
        yearString=year.getText().toString();
        cvvString=cvv.getText().toString();

        if(TextUtils.isEmpty(cardString)){
        }
        else if(TextUtils.isEmpty(monthString)){
        }
        else if(TextUtils.isEmpty(yearString)){}
        else if(TextUtils.isEmpty(cvvString)){
        }
        else {
            card = new Card(cardString, Integer.parseInt(monthString), Integer.parseInt(yearString), cvvString);
            if (card.validateCard()) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText("Invalid card Details");
            }
        }
    }


    public  class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String string = s.toString();
            if(string.trim().length()==19){
                month.requestFocus();
                cardValid();
            }
            else{
                cardNumber.requestFocus();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    }


    public void showProgressDialog() {
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class PaymentTask extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Report doInBackground(String... params) {
            try {
                JSONObject jObject = new JSONObject();
                itemArray= new JSONArray();

                jObject.put("said", 1);
                jObject.put("uid", userId);
                jObject.put("totamt", totalAmount);
              //  jObject.put("drivertip", 10);
                jObject.put("drivertip", driver_tip);
                jObject.put("pcode", 1);
                jObject.put("order_notes", "Test order notes");
                jObject.put("payment_id", params[0]);
                jObject.put("D_Address1", params[1]);
                jObject.put("D_Address2", params[2]);
                jObject.put("Shop_Id", params[3]);
                jObject.put("City", params[4]);
                jObject.put("PinCode", params[5]);

                for(int i=0;i<savedQty.size();i++){
                    itemObject=new JSONObject();
                    itemId=savedQty.get(i).getScid();
                    itemQunt=savedQty.get(i).getQuantity();
                    itemPrice=savedQty.get(i).getTotPrice();
                    itemObject.put("item_Id",itemId);
                    itemObject.put("item_qty",itemQunt);
                    itemObject.put("item_price",itemPrice);
                    itemArray.put(itemObject);
                }

                jObject.put("OrderItems", itemArray);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/Order/PostOrder");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                gerLoginResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again");
            }
        }
    }



    private void gerLoginResponse(Report response) {
        try {
            resultJsonObject = response.getJsonObject();
            if(resultJsonObject.length()>0&&resultJsonObject!=null) {
                if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("200")) {
                    String resultJson = resultJsonObject.getString("Message");
                    showToastMessage(resultJson);
                    bucketDB.removeAllDataFromOrder();
                    Intent i = new Intent(getApplicationContext(),OrderPlaced.class);
                    startActivity(i);
                }
                else {
                    showToastMessage(response.getMessage());
                }
            }
            else {
                showToastMessage(response.getMessage());
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }

    private class PhotoOrder extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Report doInBackground(String... params) {
            try {
                JSONObject jObject = new JSONObject();
                itemArray= new JSONArray();
                jObject.put("brand", productBrandString);
                jObject.put("item_name", productNameString);
                jObject.put("qty", productQuantityString);
                jObject.put("price", productPriceString);
                jObject.put("uid", userId);
                jObject.put("payment_id", params[0]);
               // jObject.put("drivertip", 10);
                jObject.put("drivertip", driver_tip);
                jObject.put("D_Address1", params[1]);
                jObject.put("D_Address2", params[2]);
                jObject.put("PinCode", params[3]);
                jObject.put("City", params[4]);
                jObject.put("image", productBaseImage);
                jObject.put("description", productDisString);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/Order/PhotoOrder");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                gerPhotoOrderResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again");
            }
        }
    }



    private void gerPhotoOrderResponse(Report response) {
        try {
            resultJsonObject = response.getJsonObject();
            if(resultJsonObject.length()>0&&resultJsonObject!=null) {
                if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("200")) {
                    String resultJson = resultJsonObject.getString("Message");
                    showToastMessage(resultJson);
                    bucketDB.removeAllDataFromOrder();
                    Intent i = new Intent(getApplicationContext(),OrderPlaced.class);
                    startActivity(i);
                }
                else {
                    showToastMessage(response.getMessage());
                }
            }
            else {
                showToastMessage(response.getMessage());
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }



    private void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}








/*
package app5.test5.bananaapps.snapeatsapp.activity.payment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.Basket;
import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.HomePage;
import app5.test5.bananaapps.snapeatsapp.activity.ItemOneFragmentTabsView;
import app5.test5.bananaapps.snapeatsapp.activity.OrderPlaced;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;
import app5.test5.bananaapps.snapeatsapp.activity.SubCategory;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;


public class PaymentActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    private DependencyHandler mDependencyHandler;
    EditText userName,cardNumber,month,year,cvv;
    String cardString,monthString,yearString,cvvString;
    Button payNow,cancel;
    ImageView backArrow;
    TextView textView;
    BucketDB bucketDB;
    String userId;
    String paymentToken;
    Double totalAmount;
    String itemId,itemQunt,itemPrice;
    JSONObject itemObject,resultJsonObject;
    JSONArray itemArray;
    ProgressDialog progressDialog;
    private List<SubCategoryList> savedQty;
    String add1,add2,loc,pcode,county;
    int shopId;
    Card card;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);
        userName=(EditText)findViewById(R.id.txt_username);
        cardNumber=(EditText)findViewById(R.id.txt_cardNumber);
        month=(EditText)findViewById(R.id.txt_month);
        year=(EditText)findViewById(R.id.year);
        cvv=(EditText)findViewById(R.id.txt_cvv);
        payNow=(Button)findViewById(R.id.PayNow);
        cancel=(Button)findViewById(R.id.cancel);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        textView = (TextView) findViewById(R.id.card_details_show);

        bucketDB=new BucketDB(PaymentActivity.this);
        savedQty=bucketDB.getAllDetails();
        userId = (String) Cache.getData(CatchValue.USER_ID, PaymentActivity.this);
        totalAmount=(Double) Cache.getData(CatchValue.TOTAL_COST, PaymentActivity.this);
        shopId=(Integer) Cache.getData(CatchValue.SHOP_ID, PaymentActivity.this);
        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);
        add1 = settings.getString("adrs0", "");
        add2 = settings.getString("adrs1", "");
        loc = settings.getString("locality", "");
        pcode = settings.getString("postalcode", "");
        county = settings.getString("countryname", "");

        itemObject=new JSONObject();

      //  mDependencyHandler = new DependencyHandler(this, (CardInputWidget) findViewById(R.id.card_input_widget), (ListView) findViewById(R.id.listview));

        Button saveButton = (Button) findViewById(R.id.save);
        cardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String string = charSequence.toString();
                if(string.trim().length()==2){
                    year.requestFocus();
                    cardValid();
                }
                else {
                    month.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
       year.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

           }

           @Override
           public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               String string = charSequence.toString();
               if(string.trim().length()==2){
                   cvv.requestFocus();
                   cardValid();
               }
               else {
                   year.requestFocus();
               }

           }

           @Override
           public void afterTextChanged(Editable editable) {

           }
       });
     cvv.addTextChangedListener(new TextWatcher() {
         @Override
         public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

         }

         @Override
         public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
             String string = charSequence.toString();
             if(string.trim().length()==3){
                 cardValid();
             }
         }
         @Override
         public void afterTextChanged(Editable editable) {

         }
     });



        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(cardString)){
                    cardNumber.setError("Please Enter Card Number");
                    cardNumber.requestFocus();
                }
                else if(TextUtils.isEmpty(monthString)){
                    month.setError("Please Enter Month");
                    month.requestFocus();
                }
               else if(TextUtils.isEmpty(yearString)){
                    year.setError("Please Enter Year");
                    year.requestFocus();
                }
                else if(TextUtils.isEmpty(cvvString)){
                    cvv.setError("Please Enter CVV Number");
                    cvv.requestFocus();
                }
            else{
                        //Toast.makeText()
                        showProgressDialog();
                        new Stripe(getApplicationContext()).createToken(
                                card,
                                "pk_test_OQ3Me8UtSTNp9q7XRK7nlYBB",
                                new TokenCallback() {
                                    public void onSuccess(Token token) {
                                        dismissProgressDialog();
                                        if(!TextUtils.isEmpty(token.getId())) {
                                            new PaymentTask().execute(token.getId(),add1,add2, String.valueOf(shopId),loc,pcode,county);
                                        }
                                        else{
                                            showToastMessage("Please try again");
                                        }
                                    }
                                    public void onError(Exception error) {
                                        Toast.makeText(getApplicationContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });

                    }

                }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

       // mDependencyHandler.attachRxTokenController(saveRxButton);

    }

    private void cardValid() {

        cardString=cardNumber.getText().toString();
        cardString.replace(" ","");
        monthString=month.getText().toString();
        yearString=year.getText().toString();
        cvvString=cvv.getText().toString();

        if(TextUtils.isEmpty(cardString)){
        }
        else if(TextUtils.isEmpty(monthString)){
        }
        else if(TextUtils.isEmpty(yearString)){}
        else if(TextUtils.isEmpty(cvvString)){
        }
        else {
            card = new Card(cardString, Integer.parseInt(monthString), Integer.parseInt(yearString), cvvString);
            if (card.validateCard()) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText("Invalid card Details");
            }
        }
    }


    public  class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String string = s.toString();
            if(string.trim().length()==19){
                month.requestFocus();
                cardValid();
            }
            else{
               cardNumber.requestFocus();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    }


    public void showProgressDialog() {
        progressDialog = new ProgressDialog(PaymentActivity.this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

        private class PaymentTask extends AsyncTask<String,Void,Report> {

            Report response = new Report();

            @Override
            protected void onPreExecute() {
                showProgressDialog();
            }

            @Override
            protected Report doInBackground(String... params) {
                try {
                    JSONObject jObject = new JSONObject();
                    itemArray= new JSONArray();

                    jObject.put("said", 1);
                    jObject.put("uid", userId);
                    jObject.put("totamt", totalAmount);
                    jObject.put("drivertip", 10);
                    jObject.put("pcode", 1);
                    jObject.put("order_notes", "test order nites");
                    jObject.put("payment_id", params[0]);
                    jObject.put("D_Address1", params[1]);
                    jObject.put("D_Address2", params[2]);
                    jObject.put("Shop_Id", params[3]);
                    jObject.put("City", params[4]);
                    jObject.put("PinCode", params[5]);

                    for(int i=0;i<savedQty.size();i++){
                        itemObject=new JSONObject();
                        itemId=savedQty.get(i).getScid();
                        itemQunt=savedQty.get(i).getQuantity();
                        itemPrice=savedQty.get(i).getTotPrice();
                        itemObject.put("item_Id",itemId);
                        itemObject.put("item_qty",itemQunt);
                        itemObject.put("item_price",itemPrice);
                        itemArray.put(itemObject);
                    }

                    jObject.put("OrderItems", itemArray);
                    response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/Order/PostOrder");
                } catch (JSONException e) {
                    showToastMessage("Server couldn't respond,Please try again");
                }
                return response;
            }

            @Override
            protected void onPostExecute(Report response) {
                dismissProgressDialog();
                if (response!=null){
                    gerLoginResponse(response);
                }
                else {
                    showToastMessage("Server couldn't respond,Please try again");
                }
            }
        }



    private void gerLoginResponse(Report response) {
        try {
            resultJsonObject = response.getJsonObject();
            if(resultJsonObject.length()>0&&resultJsonObject!=null) {
                if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("200")) {
                    String resultJson = resultJsonObject.getString("Message");
                    showToastMessage(resultJson);
                    bucketDB.removeAllDataFromOrder();
                    Intent i = new Intent(getApplicationContext(),OrderPlaced.class);
                    startActivity(i);
                }
                else {
                    showToastMessage(response.getMessage());
                }
            }
            else {
                showToastMessage(response.getMessage());
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }
    private void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
*/
