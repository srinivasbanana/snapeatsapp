package app5.test5.bananaapps.snapeatsapp.activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import com.stripe.android.model.Card;

/**
 * Created by BananaApps on 6/5/2017.
 */

public class Payment extends BaseActivity {
    ImageView gray,green,backArrow;
    Button payNow,cancel;
    EditText bname,cardNumber,month,year,cvv;
    int status = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment);
     //   gray = (ImageView)findViewById(R.id.upGrey);
        // green = (ImageView)findViewById(R.id.upGreen);
        backArrow = (ImageView)findViewById(R.id.backArrow);
        payNow = (Button) findViewById(R.id.PayNow);
        cancel = (Button) findViewById(R.id.cancel);
        bname = (EditText)findViewById(R.id.bname);
       // cardNumber = (EditText)findViewById(R.id.cardNumber);
      //  month = (EditText)findViewById(R.id.month);
        year = (EditText)findViewById(R.id.year);
        cvv = (EditText)findViewById(R.id.txt_cvv);

        cardNumber.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(19)});
        month.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(2)});
        year.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(4)});
        cvv.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(3)});


       /* gray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gray.setVisibility(View.INVISIBLE);
                green.setVisibility(View.VISIBLE);
            }
        });

        green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                green.setVisibility(View.INVISIBLE);
                gray.setVisibility(View.VISIBLE);

            }
        });*/

        //
        gray.setBackgroundResource(R.drawable.thumbs_up_grey2);

        gray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (status == 0) {
                    //gray.setBackgroundResource(R.mipmap.listgrid);
                    gray.setBackgroundResource(R.drawable.thumbs_up_grey2);
                    status++;
                } else if (status == 1) {
                    //   gray.setBackgroundResource(R.mipmap.grid);
                    gray.setBackgroundResource(R.drawable.thumbs_up_green);
                    status--;
                }

            }
        });

        //
        backArrow.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(Payment.this, CheckOut.class));
                finish();
            }
        });

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(bname.getText().toString().length()==0)
                {
                    Toast.makeText(getApplication(),"Please enter name",Toast.LENGTH_SHORT).show();
                }
                else
                if(cardNumber.getText().toString().length()==0)
                {
                    Toast.makeText(getApplication(),"Please enter card number",Toast.LENGTH_SHORT).show();
                }
                else
                if(month.getText().toString().length()==0)
                {
                    Toast.makeText(getApplication(),"Please enter month",Toast.LENGTH_SHORT).show();
                }
                else
                if(year.getText().toString().length()==0)
                {
                    Toast.makeText(getApplication(),"Please enter year",Toast.LENGTH_SHORT).show();
                }
                else
                if(cvv.getText().toString().length()==0)
                {
                    Toast.makeText(getApplication(),"Please enter cvv number",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Card card = new Card(cardNumber.getText().toString().trim(),Integer.parseInt(month.getText().toString().trim()),Integer.parseInt(year.getText().toString().trim()), cvv.getText().toString().trim());
                    // Remember to validate the card object before you use it to save time.
                    if (!card.validateCard()) {
                        Toast.makeText(getApplication(),"Not Valid",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        //   Toast.makeText(getApplication(),"Valid",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(Payment.this, OrderPlaced.class));
                        finish();
                    }


                }

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Payment.this, HomePage.class));
                finish();
            }
        });
    }
}
