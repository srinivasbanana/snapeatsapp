package app5.test5.bananaapps.snapeatsapp.activity.BucketDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by govt on 18-04-2017.
 */

public class BucketDB extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "ChampCashLocalDB";
    // ChampCash table name
    private static final String TABLE_BUCKET = "champcash";

     Context con;

    public static final String INDEX_ID= "id";
    private static final String ITEM_SCID = "item_scid";
    private static final String ITEM_ID = "item_id";
    private static final String FOOD_TYPE = "food_type";
    private static final String FOOD_NAME = "food_name";
    private static final String FOOD_DESC = "food_desc";
    private static final String PRICE = "price";
    private static final String F_IMAGE_URL = "f_image_url";
    private static final String QUANTITY = "quantity";
    private static final String TOTAL_PRICE = "total_price";
    private static final String SERIAL_NO = "serial_no";


    public BucketDB(Context applicationContext) {

        super(applicationContext, DATABASE_NAME, null, DATABASE_VERSION);
        this.con=applicationContext;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TEMPLES_TABLE = "CREATE TABLE " + TABLE_BUCKET + "("
                + ITEM_SCID + " TEXT,"
                + ITEM_ID + " TEXT,"
                + FOOD_TYPE + " TEXT,"
                + FOOD_NAME + " TEXT,"
                + FOOD_DESC + " TEXT ,"
                + PRICE + " TEXT,"
                + F_IMAGE_URL + " TEXT,"
                + QUANTITY  + " TEXT,"
                + TOTAL_PRICE + " TEXT, "
                + SERIAL_NO  + " TEXT" + ")";

        db.execSQL(CREATE_TEMPLES_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUCKET);

        // Creating tables again
        onCreate(db);
    }

    public void addChampcash(SubCategoryList champcash) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ITEM_SCID, champcash.getScid());
        values.put(ITEM_ID, champcash.getId());
        values.put(FOOD_TYPE, champcash.getFoodType());
        values.put(FOOD_NAME, champcash.getName());
        values.put(FOOD_DESC, champcash.getItemDesc());
        values.put(PRICE, champcash.getPrice());
        values.put(F_IMAGE_URL, champcash.getfImageurl());
        values.put(QUANTITY, champcash.getQuantity());
        values.put(TOTAL_PRICE, champcash.getTotPrice());
        values.put(SERIAL_NO, champcash.getSerialNo());

        // Inserting Row

        long result = db.insert(TABLE_BUCKET, null, values);
        System.out.println(result);
        db.close(); // Closing database connection

    }

    public void removeChampcash() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_BUCKET);
        db.close();
    }


    public void updateChampcash(String serialno,String quantity,String totalPrice) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(QUANTITY, quantity);
        values.put(TOTAL_PRICE, totalPrice);
        // update Row
        db.update(TABLE_BUCKET,values, "ITEM_SCID = '"+serialno+"'",null);
        db.close(); // Closing database connection
      //  Toast.makeText(con, "Item count updated successfully", Toast.LENGTH_SHORT).show();

    }
    public void removeChampcash(String serialno) {

        SQLiteDatabase db = this.getWritableDatabase();
        // update Row
        db.execSQL("DELETE FROM "+TABLE_BUCKET+" WHERE "+ITEM_SCID+"='"+serialno+"'");
        db.close(); // Closing database connection
        Toast.makeText(con, "Item removed successfully", Toast.LENGTH_SHORT).show();

    }

    public void removeAllDataFromChampcash() {

        SQLiteDatabase db = this.getWritableDatabase();
        // update Row
        db.execSQL("DELETE FROM "+TABLE_BUCKET);
        db.close(); // Closing database connection
       // Toast.makeText(con, "Items Removed successfully", Toast.LENGTH_SHORT).show();

    }

    public void removeAllDataFromOrder() {

        SQLiteDatabase db = this.getWritableDatabase();
        // update Row
        db.execSQL("DELETE FROM "+TABLE_BUCKET);
        db.close(); // Closing database connection
    }


    public List<SubCategoryList> getIndividualDetails(String userID ) {

        List<SubCategoryList> champcashList = new ArrayList<SubCategoryList>();

        try {
            // Select All Query
            String query = "SELECT * FROM " + TABLE_BUCKET + " WHERE " + FOOD_TYPE + "='" + userID + "'";;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {

                    SubCategoryList champcash = new SubCategoryList();
                    champcash.setScid(cursor.getString(0));
                    champcash.setId(cursor.getString(1));
                    champcash.setFoodType(cursor.getString(2));
                    champcash.setName(cursor.getString(3));
                    champcash.setItemDesc(cursor.getString(4));
                    champcash.setPrice(cursor.getString(5));
                    champcash.setfImageurl(cursor.getString(6));
                    champcash.setQuantity(cursor.getString(7));
                    champcash.setTotPrice(cursor.getString(8));
                    champcash.setSerialNo(cursor.getString(9));

                    // Adding temples to list
                    champcashList.add(champcash);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return champcashList;
    }



    public List<SubCategoryList> getAllDetails() {

        List<SubCategoryList> champcashList = new ArrayList<SubCategoryList>();

        try {
            // Select All Query
          //  String query = "SELECT * FROM " + TABLE_BUCKET + " ORDER BY "+ " DESC";
            String query = "SELECT * FROM " + TABLE_BUCKET ;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {

                do {

                    SubCategoryList champcash = new SubCategoryList();
                    champcash.setScid(cursor.getString(0));
                    champcash.setId(cursor.getString(1));
                    champcash.setFoodType(cursor.getString(2));
                    champcash.setName(cursor.getString(3));
                    champcash.setItemDesc(cursor.getString(4));
                    champcash.setPrice(cursor.getString(5));
                    champcash.setfImageurl(cursor.getString(6));
                    champcash.setQuantity(cursor.getString(7));
                    champcash.setTotPrice(cursor.getString(8));
                    champcash.setSerialNo(cursor.getString(9));
                    // Adding temples to list
                    champcashList.add(champcash);

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return champcashList;
    }


}
