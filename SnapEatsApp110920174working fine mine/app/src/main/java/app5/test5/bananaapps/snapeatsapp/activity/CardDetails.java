package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.stripe.android.model.Card;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.payment.PaymentActivity;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

/**
 * Created by BananaApps on 6/5/2017.
 */

public class CardDetails extends BaseActivity {
    TextView toolbartitle;

   /* ImageView backArrow;
    Button edit,save;
    EditText name,cardnumber,expDate,cvv;*/

    ImageView gray,green,backArrow;
    Button edit,save;
    EditText bname,cardNumber,month,year;
    String s1,s2,s3,s4;

    String cardString,monthString,yearString;
    Card card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_details);

        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow = (ImageView)findViewById(R.id.backArrow);
        backArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CardDetails.this, HomePage.class));
                finish();
            }
        });

        edit = (Button) findViewById(R.id.Edit);
        save = (Button) findViewById(R.id.Save);
        bname = (EditText)findViewById(R.id.txt_username);
        cardNumber = (EditText)findViewById(R.id.txt_cardNumber);
        month = (EditText)findViewById(R.id.txt_month);
        year = (EditText)findViewById(R.id.year);


        cardNumber.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(19)});
        month.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(2)});
        year.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(4)});


        bname.setEnabled(false);
        cardNumber.setEnabled(false);
        month.setEnabled(false);
        year.setEnabled(false);



        cardNumber.addTextChangedListener(new CardDetails.FourDigitCardFormatWatcher());
        month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String string = charSequence.toString();
                if(string.trim().length()==2){
                    year.requestFocus();
                    cardValid();
                }
                else {
                    month.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String string = charSequence.toString();
                if(string.trim().length()==2){
                  //  cvv.requestFocus();
                    cardValid();
                }
                else {
                    year.requestFocus();
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



        s1 = (String) Cache.getData(CatchValue.CardName, CardDetails.this);
        s2 = (String) Cache.getData(CatchValue.CardNumber, CardDetails.this);
        s3 = (String) Cache.getData(CatchValue.CardMonth, CardDetails.this);
        s4 = (String) Cache.getData(CatchValue.CardExpYear, CardDetails.this);

        bname.setText(s1);
        cardNumber.setText(s2);
        month.setText(s3);
        year.setText(s4);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                s1 = bname.getText().toString().trim();
                s2 = cardNumber.getText().toString().trim();
                s3 = month.getText().toString().trim();
                s4 = year.getText().toString().trim();


                    Cache.putData(CatchValue.CardName, getApplicationContext(), bname.getText().toString().trim(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.CardNumber, getApplicationContext(), cardNumber.getText().toString().trim(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.CardMonth, getApplicationContext(), month.getText().toString().trim(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.CardExpYear, getApplicationContext(), year.getText().toString().trim(), Cache.CACHE_LOCATION_DISK);

                    bname.setEnabled(false);
                    cardNumber.setEnabled(false);
                    month.setEnabled(false);
                    year.setEnabled(false);


                    showToast("Card details saved successfully");
                    Intent i = new Intent(CardDetails.this, HomePage.class);
                    startActivity(i);

            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bname.setEnabled(true);
                cardNumber.setEnabled(true);
                month.setEnabled(true);
                year.setEnabled(true);

            }
        });

      /*  backArrow = (ImageView)findViewById(R.id.backArrow);
        edit = (Button) findViewById(R.id.edit);
        save = (Button) findViewById(R.id.save);
        name = (EditText)findViewById(R.id.bname);
        cardnumber = (EditText)findViewById(R.id.cpnameData);
        expDate = (EditText)findViewById(R.id.expDate);
        cvv = (EditText)findViewById(R.id.cvv);




        name.setEnabled(false);
        cardnumber.setEnabled(false);
        expDate.setEnabled(false);
        cvv.setEnabled(false);

        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CardDetails.this, HomePage.class));
                finish();
            }
        });

       save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setEnabled(false);
                cardnumber.setEnabled(false);
                expDate.setEnabled(false);
                cvv.setEnabled(false);
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setEnabled(true);
                cardnumber.setEnabled(true);
                expDate.setEnabled(true);
                cvv.setEnabled(true);
            }
        });*/
    }
    public  class FourDigitCardFormatWatcher implements TextWatcher {

        // Change this to what you want... ' ', '-' etc..
        private static final char space = ' ';

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String string = s.toString();
            if(string.trim().length()==19){
                month.requestFocus();
                cardValid();
            }
            else{
                cardNumber.requestFocus();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0 && (s.length() % 5) == 0) {
                final char c = s.charAt(s.length() - 1);
                if (space == c) {
                    s.delete(s.length() - 1, s.length());
                }
            }
            // Insert char where needed.
            if (s.length() > 0 && (s.length() % 5) == 0) {
                char c = s.charAt(s.length() - 1);
                // Only if its a digit where there should be a space we insert a space
                if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                    s.insert(s.length() - 1, String.valueOf(space));
                }
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    }
    private void cardValid() {

        cardString=cardNumber.getText().toString();
        cardString.replace(" ","");
        monthString=month.getText().toString();
        yearString=year.getText().toString();


        if(TextUtils.isEmpty(cardString)){
        }
        else if(TextUtils.isEmpty(monthString)){
        }
        else if(TextUtils.isEmpty(yearString)){}

        else {
          /* card = new Card(cardString, Integer.parseInt(monthString), Integer.parseInt(yearString), cvvString);
            if (card.validateCard()) {
                textView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText("Invalid card Details");
            }*/
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(CardDetails.this, HomePage.class));
        finish();

    }
}
