package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

public class MoreMenu extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    TextView profileName,profileMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);
        final String logStatus = settings.getString("logStatus", "");
        // String logStatus = settings.getString("logStatus", "");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        // Toast.makeText(MoreMenu.this,"logStatus "+logStatus,Toast.LENGTH_LONG).show();
        if(logStatus.equals("Skip"))
        {
            //   Toast.makeText(MoreMenu.this,"Skip ",Toast.LENGTH_LONG).show();
            setContentView(R.layout.more_menu_skip);
        }
        else
        {
            //  Toast.makeText(MoreMenu.this,"Login ",Toast.LENGTH_LONG).show();
            setContentView(R.layout.more_menu);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.openDrawer(GravityCompat.START);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_close, R.string.navigation_drawer_open);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        profileName = (TextView) header.findViewById(R.id.profileName);
        profileMail = (TextView) header.findViewById(R.id.profileMail);
        final String fname = settings.getString("fname", "");
        final String email = settings.getString("email", "");
        profileName.setText(fname);
        profileMail.setText(email);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            startActivity(new Intent(MoreMenu.this, HomePage.class));
            finish();
        } else {
            //   Fragment selectedFragment = null;
            startActivity(new Intent(MoreMenu.this, HomePage.class));
            finish();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            startActivity(new Intent(MoreMenu.this, Profile.class));
            finish();
        } else if (id == R.id.nav_OrderHistory) {
            startActivity(new Intent(MoreMenu.this, History.class));
            finish();

        } else if (id == R.id.nav_cardDetails) {
            startActivity(new Intent(MoreMenu.this, CardDetails.class));
            finish();
        }
        else if (id == R.id.nav_faq) {
            startActivity(new Intent(MoreMenu.this, Faqs.class));
            finish();
        }
       /* else if (id == R.id.nav_aboutus) {
            startActivity(new Intent(MoreMenu.this, About.class));
            finish();
        }*/
        else if (id == R.id.nav_contactSupport) {
            startActivity(new Intent(MoreMenu.this, ContactSupport.class));
            finish();
        }
        else if (id == R.id.nav_temsAndConditions) {
            startActivity(new Intent(MoreMenu.this, TermsConditions.class));
            finish();
        }
        else if (id == R.id.nav_logout) {

            AlertDialog.Builder builder = new AlertDialog.Builder(MoreMenu.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            builder.setTitle("Logout");
            builder.setMessage("Do you want to exit?");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Cache.putData(CatchValue.USER_ID, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    startActivity(new Intent(MoreMenu.this, Login.class));
                    finish();
//                                if (authMode.equalsIgnoreCase("G")) {
//                                    if (mGoogleApiClient.isConnected()) {
//                                        Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
//                                        mGoogleApiClient.disconnect();
//                                        mGoogleApiClient.connect();
//                                    }
//                                } else if (authMode.equalsIgnoreCase("F")) {
//                                    LoginManager.getInstance().logOut();
//                                }else{
//                        new SignOutTask().execute();
//                    }

                }
            });

            builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            builder.setIcon(R.mipmap.ic_action_info);
            builder.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}