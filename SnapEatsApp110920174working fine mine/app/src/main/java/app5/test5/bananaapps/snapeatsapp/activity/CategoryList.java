package app5.test5.bananaapps.snapeatsapp.activity;

/**
 * Created by BananaApps on 6/1/2017.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.FoodTypeList;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class CategoryList extends BaseAdapter{

    private Context context;
    public static ArrayList<SubCategoryList> foodTypeLists;
    private ArrayList<String> foodTypeNames;


    private LayoutInflater mInflater;
    Set<String> hs;

    public CategoryList(Context context, ArrayList<SubCategoryList> foodTypeLists, ArrayList<String> foodTypeNames) {
        this.context = context;
        this.foodTypeLists = foodTypeLists;
        this.foodTypeNames=foodTypeNames;
    }

    @Override
    public int getCount() {
        return foodTypeNames.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView=  mInflater.inflate(R.layout.category_list_single, null, true);
        LinearLayout itemData= (LinearLayout)rowView.findViewById(R.id.itemLinear);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.foodTypeList);
        txtTitle.setText(foodTypeNames.get(position));


//        if(foodTypeLists.size()-1==position){
//            hs = new HashSet<>();
//            hs.addAll(foodTypeNames);
//            foodTypeNames.clear();
//            foodTypeNames.addAll(hs);
//            for(int i=0;i<foodTypeNames.size();i++) {
//
//                txtTitle.setText(foodTypeNames.get(i));
//            }
//        }

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sublist= new Intent(context,SubCategory.class);
                Bundle bundle= new Bundle();
                Cache.putData(CatchValue.MENU_LIST, context, foodTypeNames.get(position), Cache.CACHE_LOCATION_DISK);
                bundle.putString("fromClass","CategoryList");
                sublist.putExtra("BUNDLE",bundle);
                context.startActivity(sublist);
            }
        });
        return rowView;
    }
}