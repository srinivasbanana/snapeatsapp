package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import app5.test5.bananaapps.snapeatsapp.R;

public class Splash extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        startSplashing();
    }
    private void startSplashing() {
        int SPLASH_TIME_OUT = 5000; //3000
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                launchMainScreen();
            }
        }, SPLASH_TIME_OUT);
    }
    private void launchMainScreen() {
        Intent intent = new Intent(Splash.this, Login.class);

        startActivity(intent);
        finish();

    }
}
