package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;

/**
 * Created by BananaApps on 5/30/2017.
 */

public class ForgotPwdSuccess extends BaseActivity {
    TextView tv,subheading,check,resend;
    Button okmsg;

    public static final String PREFS_NAME = "SnapEatsVariables";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pwd_success);
        tv = (TextView)findViewById(R.id.checktext);
        subheading = (TextView)findViewById(R.id.Subheading);
        check = (TextView)findViewById(R.id.check);
        resend = (TextView)findViewById(R.id.resend);

        okmsg = (Button)findViewById(R.id.okmsg);


        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "Lato-Regular.ttf");
        tv.setTypeface(custom_font);
        subheading.setTypeface(custom_font);
        check.setTypeface(custom_font);
        resend.setTypeface(custom_font);


        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        okmsg.setTypeface(custom_font2);


        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final String spemail = settings.getString("email", "");
        tv.setText(spemail);



        okmsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgotPwdSuccess.this, Login.class));
                finish();
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check.setText("Resend successfully...");
                check.setGravity(Gravity.CENTER);
                resend.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ForgotPwdSuccess.this, ForgotPwd.class));
        finish();
        super.onBackPressed();
    }
}
