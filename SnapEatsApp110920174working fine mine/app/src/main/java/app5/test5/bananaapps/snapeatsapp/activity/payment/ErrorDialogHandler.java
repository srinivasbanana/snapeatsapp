package app5.test5.bananaapps.snapeatsapp.activity.payment;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import app5.test5.bananaapps.snapeatsapp.R;


/**
 * A convenience class to handle displaying error dialogs.
 */
public class ErrorDialogHandler {

    FragmentManager mFragmentManager;

    public ErrorDialogHandler(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }

    public void showError(String errorMessage) {
        DialogFragment fragment = ErrorDialogFragment.newInstance(R.string.validationErrors, errorMessage);
        fragment.show(mFragmentManager, "error");
    }
}
