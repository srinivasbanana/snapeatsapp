package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

import java.io.Serializable;

/**
 * Created by Dell1 on 24-Jul-17.
 */

public class ItemValues implements Serializable {

    String foodType;
    String foodDesc;
    int number;
    String price;
    public ItemValues(String foodType, String foodDesc, String price, int number) {
        this.foodType = foodType;
        this.foodDesc = foodDesc;
        this.price = price;
        this.number = number;

    }

    public String getFoodDesc() {
        return foodDesc;
    }

    public void setFoodDesc(String foodDesc) {
        this.foodDesc = foodDesc;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
