package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;


/**
 * Created by BananaApps on 5/29/2017.
 */

public class Register1 extends BaseActivity {
    ImageView register1;
    EditText fname,lname;
    TextView heading,subheading;
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register1);

        register1 = (ImageView) findViewById(R.id.register1);
        fname = (EditText)findViewById(R.id.firstname);
        lname = (EditText)findViewById(R.id.lastname);

        heading = (TextView) findViewById(R.id.heading);
        subheading = (TextView) findViewById(R.id.Subheading);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "Lato-Regular.ttf");
        fname.setTypeface(custom_font);
        lname.setTypeface(custom_font);
        subheading.setTypeface(custom_font);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        heading.setTypeface(custom_font2);

        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String temp = settings.getString("fname", "");
       // fname.setText(temp);
        temp = settings.getString("lname", "");
       // lname.setText(temp);

        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!IsEmpty(fname.getText().toString()))
                {
                    okMessage("Error","Please enter first name");
                }
            else
                if(!IsEmpty(lname.getText().toString()))
                {
                    okMessage("Error","Please enter last name");
                }
                else
                {
                    Cache.putData(CatchValue.FIRST_NAME, Register1.this, fname.getText().toString(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.LAST_NAME, Register1.this, lname.getText().toString(), Cache.CACHE_LOCATION_DISK);
//                    SharedPreferences.Editor editor2 = settings.edit();
//                    editor2.putString("fname",fname.getText().toString());
//                    editor2.putString("lname",lname.getText().toString());
//                    editor2.commit();

                    startActivity(new Intent(Register1.this, Register2.class));
                    finish();
                }
            }
        });



    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Register1.this, Login.class));
        finish();
        super.onBackPressed();
    }
}
