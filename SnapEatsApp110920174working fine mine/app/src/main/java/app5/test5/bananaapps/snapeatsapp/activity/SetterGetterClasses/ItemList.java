package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

import java.io.Serializable;

/**
 * Created by APTS on 1/17/2017.
 */
public class ItemList implements Serializable {

    private String templeName;
    private String templeUrl;
    private Double distance;
    private int sid;

    public ItemList(String templeName, String templeUrl, Double distance,int sid) {
        this.templeName = templeName;
        this.templeUrl = templeUrl;
        this.distance = distance;
        this.sid = sid;
    }

    public String getTempleName() {
        return templeName;
    }

    public String getTempleUrl() {
        return templeUrl;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }
}
