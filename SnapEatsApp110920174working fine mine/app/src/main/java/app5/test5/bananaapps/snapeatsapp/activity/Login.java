package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import java.util.HashMap;
import java.util.Map;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.BaseActivity;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

/**
 * Created by BananaApps on 5/29/2017.
 */

public class Login  extends BaseActivity {
    TextView register,forgot_pwd,skip;
    EditText em,pw;
    Button login;
    Boolean isInternetPresent = false;
    ProgressDialog progress;
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    ConnectionDetector cd;
    String userId,address1,address2,email,fname,lname,password,phone;
    ProgressDialog progressDialog;
    JSONObject resultJsonObject;
    String REGISTER_URL = "http://seapi.testersworld.com/api/User/Login";
    String responseMessage;
    String fromBasket,fromProduct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        register = (TextView) findViewById(R.id.register);
        forgot_pwd = (TextView) findViewById(R.id.forgot_password);
        login = (Button)findViewById(R.id.Login);
        em = (EditText)findViewById(R.id.email_input);
        pw = (EditText)findViewById(R.id.password);
        skip = (TextView)findViewById(R.id.skip);
        cd = new ConnectionDetector(this);
        isInternetPresent = cd.isConnectionAvailable();
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        fromBasket=getIntent().getStringExtra("fromBasket");
        fromProduct=getIntent().getStringExtra("fromProduct");
        userId = (String) Cache.getData(CatchValue.USER_ID, Login.this);
       // showAlertDialog(this, "userId ", userId, false);
        if(!TextUtils.isEmpty(userId)){

            Intent intent = new Intent(this, HomePage.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else {
            if(!TextUtils.isEmpty(fromBasket)) {
                if (fromBasket.equalsIgnoreCase("fromBasket")) {
                    skip.setVisibility(View.GONE);
                }
            }
            else if(!TextUtils.isEmpty(fromProduct)) {
                if (fromProduct.equalsIgnoreCase("fromPhoto")) {
                    skip.setVisibility(View.GONE);
                }
            }
            else{
                skip.setVisibility(View.VISIBLE);
            }
            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progress = new ProgressDialog(Login.this);
                    progress.setMessage("Please wait...");
                    progress.show();
                    progress.setCancelable(false);

                    Runnable progressRunnable = new Runnable() {
                        @Override
                        public void run() {
                            progress.dismiss();
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("fname", "");
                            editor.putString("lname", "");
                            editor.putString("email", "");
                            editor.putString("phone", "");
                            editor.putString("sppassword", "");
                           // Cache.putData(CatchValue.TOTAL_COST, getApplicationContext(),"Skip", Cache.CACHE_LOCATION_DISK);
                            editor.putString("logStatus", "Skip");
                            editor.putString("isAddressChange", "false");
                            editor.commit();
                            startActivity(new Intent(Login.this, HomePage.class));
                            finish();
                        }
                    };
                    Handler pdCanceller = new Handler();
                    pdCanceller.postDelayed(progressRunnable, 3000);
                }
            });

            Typeface custom_font = Typeface.createFromAsset(getAssets(), "Lato-Regular.ttf");
            register.setTypeface(custom_font);
            forgot_pwd.setTypeface(custom_font);
            em.setTypeface(custom_font);
            pw.setTypeface(custom_font);


            Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "MavenProLight-300.otf");
            login.setTypeface(custom_font2);


            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor2 = settings.edit();
                    editor2.putString("fname", "");
                    editor2.putString("lname", "");
                    editor2.putString("email", "");
                    editor2.putString("phone", "");
                    editor2.putString("sppassword", "");
                    editor2.commit();
                    startActivity(new Intent(Login.this, Register1.class));
                    finish();
                }
            });

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String spemail = settings.getString("email", "");
                    String sppw = settings.getString("sppassword", "");


                    if (!IsEmpty(em.getText().toString().trim())) {
                        okMessage("Error", "Please enter Email ID");
                    } else if (!IsValidEmail(em.getText().toString().trim())) {
                        okMessage("Error", "Please enter Valid Email ID");
                    } else if (!IsEmpty(pw.getText().toString().trim())) {
                        okMessage("Error", "Please enter Password");
                    } else
                    //   if(spemail.equals(em.getText().toString().trim()) && sppw.equals(pw.getText().toString().trim()) && spemail.length()!=0 && sppw.length()!=0)
                    {
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("logStatus", "Login");
                        editor.putString("isAddressChange", "false");
                        editor.commit();
                        if (isInternetPresent) {
                            new LoginTask().execute(em.getText().toString().trim(), pw.getText().toString().trim());
//                        RequestQueue queue = Volley.newRequestQueue(Login.this);
//                        showProgressDialog();
//                        Map<String, String> postParam= new HashMap<String, String>();
//                        postParam.put("email", em.getText().toString());
//                        postParam.put("password", pw.getText().toString().trim());
//
//                        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, REGISTER_URL, new JSONObject(postParam),
//                                new Response.Listener<JSONObject>() {
//
//                                    @Override
//                                    public void onResponse(JSONObject response) {
//                                        dismissProgressDialog();
//                                        Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_LONG).show();
//                                    }
//                                }, new Response.ErrorListener() {
//
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                dismissProgressDialog();
//                                Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_LONG).show();
//                            }
//                        }) {
//                            @Override
//                            public Map<String, String> getHeaders() throws AuthFailureError {
//                                HashMap<String, String> headers = new HashMap<String, String>();
//                                headers.put("Content-Type", "application/json; charset=utf-8");
//                                return headers;
//                            }
//                        };
//                        queue.add(jsonObjReq);

                        }
                        else
                        {
                            ShowNoInternetDialog();
                        }
                    }

                }
            });

            forgot_pwd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Toast.makeText(Login.this,"Forgot pwd",Toast.LENGTH_LONG).show();
                    startActivity(new Intent(Login.this, ForgotPwd.class));
                    finish();
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        showYesNo();
        //  super.onBackPressed();
    }



    private class LoginTask extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Report doInBackground(String... params) {
            try {
                JSONObject jObject = new JSONObject();
                jObject.put("email", params[0]);
                jObject.put("password", params[1]);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/User/Login");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                gerLoginResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again");
            }
        }
    }

    public void ShowNoInternetDialog() {
        showAlertDialog(this, "No Internet Connection", "Please check your network.", false);
    }
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }
    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    private void gerLoginResponse(Report response) {
        try {
            if (response.getStatus().equalsIgnoreCase("true")) {
                resultJsonObject = response.getJsonObject();
                if(resultJsonObject.length()>0&&resultJsonObject!=null) {
                    JSONObject resultJson=resultJsonObject.getJSONObject("Msg");
                    if(resultJson.length()>0&& resultJson!=null){
                        if (resultJson.getString("StatusCode").equalsIgnoreCase("200")) {
                            responseMessage = resultJson.getString("Message");
                            showToastMessage(responseMessage);
                            userId=resultJsonObject.getString("uid");
                            address1= resultJsonObject.getString("address1");
                            address2= resultJsonObject.getString("address2");
                            email= resultJsonObject.getString("email");
                            fname=resultJsonObject.getString("fname");
                            lname=resultJsonObject.getString("lname");
                            password=resultJsonObject.getString("password");
                            phone=resultJsonObject.getString("phone");

                            Cache.putData(CatchValue.USER_ID, getApplicationContext(), userId, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.Address1, getApplicationContext(), address1, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.Address2, getApplicationContext(), address2, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.EMAIL, getApplicationContext(), email, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.FIRST_NAME, getApplicationContext(), fname, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.LAST_NAME, getApplicationContext(), lname, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.PASSWORD, getApplicationContext(), password, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.PHONE, getApplicationContext(), phone, Cache.CACHE_LOCATION_DISK);
                            Cache.putData(CatchValue.AddressChanged, getApplicationContext(), "AddressNotChanged", Cache.CACHE_LOCATION_DISK);

                            if(!TextUtils.isEmpty(fromBasket)){
                                if(fromBasket.equalsIgnoreCase("fromBasket")){
                                    Intent intent = new Intent(this, Basket.class);
                                    intent.putExtra("fromProduct", "notfromProduct");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            }
                            else if(!TextUtils.isEmpty(fromProduct)){
                                if(fromProduct.equalsIgnoreCase("fromPhoto")){
                                    Intent intent = new Intent(this, CheckOut.class);
                                    intent.putExtra("fromProduct", "fromProduct");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);

                                    /*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                    transaction.replace(R.id.fragemntThree, ItemThreeFragment.newInstance());
                                    transaction.commit();*/
                                }
                            }
                            else{
                                Intent intent = new Intent(this, HomePage.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }

                        }

                    }
                    else if(resultJson.getString("isSuccess").equalsIgnoreCase("false")){
                        if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("400")){
                            responseMessage = resultJsonObject.getString("Message");
                            showToastMessage(responseMessage);
                        }
                        else if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("401")){
                            responseMessage = resultJsonObject.getString("Message");
                            showToastMessage(responseMessage);
                        }
                    }

                }
                else {
                    showToastMessage("Some thing went wrong please check once");
                }
            }

            else {
                if ((response.getStatus().equalsIgnoreCase("false"))) {
                    responseMessage = response.getMessage();
                    if (response.getErr_code() == 400) {
                        showToastMessage("Invalid Input Parameters");
                    }
                    else if (response.getErr_code() == 401) {
                        showToastMessage("User Inactive / Invalid Crenditials");
                    } else if (response.getErr_code() == 500) {
                        showToastMessage(responseMessage);
                    } else {
                        showToastMessage(responseMessage);
                    }
                }
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    new LoginTask().execute(em.getText().toString().trim(),pw.getText().toString().trim());
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }

}
