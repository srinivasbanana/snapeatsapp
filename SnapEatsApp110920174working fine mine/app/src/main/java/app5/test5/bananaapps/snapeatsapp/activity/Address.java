package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.payment.PaymentActivity;

/**
 * Created by BananaApps on 6/14/2017.
 */

public class Address extends BaseActivity {
    ImageView backArrow;
    TextView toolbartitle,add0,add1,loc,pcode,cname;
    Button save;
    String adrs0;

    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address);
        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        backArrow = (ImageView)findViewById(R.id.backArrow);
        add0 = (TextView)findViewById(R.id.adrs0);
        add1 = (TextView)findViewById(R.id.adrs1);
        loc = (TextView)findViewById(R.id.locality);
        pcode = (TextView)findViewById(R.id.postalcode);
        cname = (TextView)findViewById(R.id.countryname);
        save = (Button) findViewById(R.id.save);

        pcode.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(7)});

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Address.this, Profile.class));
                finish();
            }
        });

        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);

        adrs0 = settings.getString("adrs0", "");
        add0.setText(adrs0);

        adrs0 = settings.getString("adrs1", "");
        add1.setText(adrs0);

        adrs0 = settings.getString("locality", "");
        loc.setText(adrs0);

        adrs0 = settings.getString("postalcode", "");
        pcode.setText(adrs0);

        adrs0 = settings.getString("countryname", "");
        cname.setText(adrs0);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor2 = settings.edit();

                adrs0 = add0.getText().toString();
                editor2.putString("adrs0",adrs0);

                adrs0 = add1.getText().toString();
                editor2.putString("adrs1",adrs0);

                adrs0 = loc.getText().toString();
                editor2.putString("locality",adrs0);

                adrs0 = pcode.getText().toString();
                editor2.putString("postalcode",adrs0);

                adrs0 = cname.getText().toString();
                editor2.putString("countryname",adrs0);

                editor2.putString("isAddressChanged","true");
                editor2.commit();

                startActivity(new Intent(Address.this, PaymentActivity.class));
                finish();

                showToast("Address saved successfully...");
            }
        });
    }
}
