package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by BananaApps on 6/7/2017.
 */
import app5.test5.bananaapps.snapeatsapp.R;

public class About extends BaseActivity {
    TextView toolbartitle;
    ImageView backArrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        backArrow = (ImageView)findViewById(R.id.backArrow);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(About.this, HomePage.class));
                finish();
            }
        });
    }
}
