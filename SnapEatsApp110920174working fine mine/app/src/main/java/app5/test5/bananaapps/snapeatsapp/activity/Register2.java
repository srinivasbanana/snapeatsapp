package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;


/**
 * Created by BananaApps on 5/29/2017.
 */

public class Register2 extends BaseActivity {
    ImageView register2,left;
    EditText email2,phone;
    TextView heading,subheading;

    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register2);

        register2 = (ImageView) findViewById(R.id.register2);
        left = (ImageView) findViewById(R.id.left);
        email2 = (EditText)findViewById(R.id.email);
        phone = (EditText)findViewById(R.id.phone);

        heading = (TextView) findViewById(R.id.heading);
        subheading = (TextView) findViewById(R.id.Subheading);

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "Lato-Regular.ttf");
        email2.setTypeface(custom_font);
        phone.setTypeface(custom_font);
        subheading.setTypeface(custom_font);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        heading.setTypeface(custom_font2);

        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String temp = settings.getString("email", "");
       // email2.setText(temp);
        temp = settings.getString("phone", "");
       // phone.setText(temp);

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register2.this, Register1.class));
                finish();
            }
        });






        phone.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(11)});



        register2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!IsEmpty(email2.getText().toString().trim()))
                {
                    okMessage("Error","Please enter Email ID");
                }
                else
                    if(!IsValidEmail(email2.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter Valid Email ID");
                    }
                    else
                if(!IsEmpty(phone.getText().toString().trim()))
                {
                    okMessage("Error","Please enter Phone number");
                }
                else
                    if(!IsLength(phone.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter 11 digits Phone number");
                    }
                   else
                       if (!phone.getText().toString().substring(0,1).equals("0"))
                       {
                           okMessage("Error","Please enter correct phone number");
                       }
                else
                {
                    Cache.putData(CatchValue.EMAIL, Register2.this, email2.getText().toString().trim(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PHONE, Register2.this, phone.getText().toString().trim(), Cache.CACHE_LOCATION_DISK);

//                    SharedPreferences.Editor editor2 = settings.edit();
//                    editor2.putString("email",email2.getText().toString().trim());
//                    editor2.putString("phone",phone.getText().toString().trim());
//                    editor2.commit();
                    startActivity(new Intent(Register2.this, Register3.class));
                    finish();
                }





               /* SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
              //  SharedPreferences.Editor editor2 = settings.edit();
                String value = settings.getString("fname", "");


                okMessage("Firstname",value);


                startActivity(new Intent(Register2.this, Register3.class));
                finish();*/
            }
        });


    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Register2.this, Register1.class));
        finish();
        super.onBackPressed();
    }



}
