package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.payment.PaymentActivity;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

/**
 * Created by BananaApps on 6/14/2017.
 */

public class AddressChange extends BaseActivity {
    ImageView backArrow;
    TextView toolbartitle,add0,add1,loc,pcode,cname;
    Button save;
    String adrs0;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    ProgressDialog progressDialog;
    JSONObject resultJsonObject;
    String responseMessage,uid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_change);
        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        backArrow = (ImageView)findViewById(R.id.backArrow);
        add0 = (TextView)findViewById(R.id.adrs0);
        add1 = (TextView)findViewById(R.id.adrs1);
        loc = (TextView)findViewById(R.id.locality);
        pcode = (TextView)findViewById(R.id.postalcode);
        cname = (TextView)findViewById(R.id.countryname);
        cname.setEnabled(false);
        save = (Button) findViewById(R.id.save);
        uid=(String) Cache.getData(CatchValue.USER_ID, AddressChange.this);

        pcode.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(7)});

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddressChange.this, Profile.class));
                finish();
            }
        });

        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);
        String Str_length=(String) Cache.getData(CatchValue.AddressChanged, AddressChange.this);
        if(Str_length.equalsIgnoreCase("AddressNotChanged"))
        {
            adrs0 = settings.getString("adrs0", "");
            add0.setText(adrs0);

            adrs0 = settings.getString("adrs1", "");
            add1.setText(adrs0);

            adrs0 = settings.getString("locality", "");
            loc.setText(adrs0);

            adrs0 = settings.getString("postalcode", "");
            pcode.setText(adrs0);

            adrs0 = settings.getString("countryname", "");
            cname.setText(adrs0);
        }
        else
        {
            String a1=(String) Cache.getData(CatchValue.ChangeAddress1, AddressChange.this);
            String a2=(String) Cache.getData(CatchValue.ChangeAddress2, AddressChange.this);
            String l=(String) Cache.getData(CatchValue.ChangeLocation, AddressChange.this);
            String p=(String) Cache.getData(CatchValue.ChangePincode, AddressChange.this);


            add0.setText(a1);
            add1.setText(a2);
            loc.setText(l);
            pcode.setText(p);

           // Cache.putData(CatchValue.Address1, getApplicationContext(), "AddressChanged", Cache.CACHE_LOCATION_DISK);

        }
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (add0.getText().toString().trim().length() == 0) {
                    showToast("Please enter Address1");
                } else if (add1.getText().toString().trim().length() == 0) {
                    showToast("Please enter Address2");
                } else if (loc.getText().toString().trim().length() == 0) {
                    showToast("Please enter Location");
                } else if (pcode.getText().toString().trim().length() == 0) {
                    showToast("Please enter pincode");
                } else
                {
                    cd = new ConnectionDetector(AddressChange.this);
                    isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {

                    //new Login.LoginTask().execute(em.getText().toString().trim(), pw.getText().toString().trim());
                    new AddressChange.AddressChangeTask().execute(uid,add0.getText().toString(),add1.getText().toString(),loc.getText().toString(), pcode.getText().toString());
                }
                else
                {
                    ShowNoInternetDialog();
                }
                /*SharedPreferences.Editor editor2 = settings.edit();

                adrs0 = add0.getText().toString();
                editor2.putString("adrs0",adrs0);

                adrs0 = add1.getText().toString();
                editor2.putString("adrs1",adrs0);

                adrs0 = loc.getText().toString();
                editor2.putString("locality",adrs0);

                adrs0 = pcode.getText().toString();
                editor2.putString("postalcode",adrs0);

                adrs0 = cname.getText().toString();
                editor2.putString("countryname",adrs0);

                editor2.putString("isAddressChanged","true");
                editor2.commit();*/


              //  showToast("Address saved successfully...");
            }
            }
        });
    }
    public void ShowNoInternetDialog() {
        showAlertDialog(this, "No Internet Connection", "Please check your network.", false);
    }
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                cd = new ConnectionDetector(AddressChange.this);
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    new AddressChange.AddressChangeTask().execute(uid,add0.getText().toString(),add1.getText().toString(),loc.getText().toString(), pcode.getText().toString());
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }

    private class AddressChangeTask extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();

        }

        @Override
        protected Report doInBackground(String... params) {
            try {
                JSONObject jObject = new JSONObject();
                jObject.put("uid", params[0]);
                jObject.put("address1", params[1]);
                jObject.put("address2", params[2]);
                jObject.put("city", params[3]);
                jObject.put("pin", params[4]);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/User/EditAddress");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                getChangeAddressResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again" );
            }
        }
    }
    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }
    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    private void getChangeAddressResponse(Report response) {
        try {
            if (response.getStatus().equalsIgnoreCase("true")) {
                resultJsonObject = response.getJsonObject();
                if(resultJsonObject.length()>0&&resultJsonObject!=null) {
                    responseMessage=response.getJsonObject().getString("Message");
                    showToastMessage(responseMessage);
                    Cache.putData(CatchValue.AddressChanged, getApplicationContext(), "AddressChanged", Cache.CACHE_LOCATION_DISK);

                    Cache.putData(CatchValue.ChangeAddress1, getApplicationContext(), add0.getText().toString(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.ChangeAddress2, getApplicationContext(), add1.getText().toString(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.ChangeLocation, getApplicationContext(), loc.getText().toString(), Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.ChangePincode, getApplicationContext(), pcode.getText().toString(), Cache.CACHE_LOCATION_DISK);

                    startActivity(new Intent(AddressChange.this, Profile.class));
                    finish();
                }
                else {
                    showToastMessage("Some thing went wrong please check once");
                }
            }
            else {
                if ((response.getStatus().equalsIgnoreCase("false"))) {
                    responseMessage = response.getMessage();
                    if (response.getErr_code() == 400) {
                        showToastMessage(responseMessage);
                    }
                    else if (response.getErr_code() == 401) {
                        showToastMessage(responseMessage);
                    } else if (response.getErr_code() == 500) {
                        showToastMessage(responseMessage);
                    } else {
                        showToastMessage(responseMessage);
                    }
                }
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }

}
