package app5.test5.bananaapps.snapeatsapp.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by BananaApps on 6/7/2017.
 */
import app5.test5.bananaapps.snapeatsapp.R;

public class ContactSupport extends BaseActivity {
    TextView toolbartitle;
    ImageView backArrow;
    Button email,call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_support);

        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        backArrow = (ImageView)findViewById(R.id.backArrow);
        call = (Button)findViewById(R.id.call);
        email = (Button)findViewById(R.id.email);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactSupport.this, HomePage.class));
                finish();
            }
        });
        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert(ContactSupport.this, "No Mail Accounts", "Please setup a Mail account in order to send email.", false);

            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:9494694524"));

                if (ActivityCompat.checkSelfPermission(ContactSupport.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });


    }

    public void showAlert(Context context, String title, String message, Boolean status) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context,android.R.style.Theme_Material_Light_Dialog_Alert).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);

        alertDialog.setButton2("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(ContactSupport.this, HomePage.class));
        finish();

    }
}
