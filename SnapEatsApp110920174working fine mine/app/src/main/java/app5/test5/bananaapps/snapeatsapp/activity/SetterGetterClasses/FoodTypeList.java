package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

import java.io.Serializable;

/**
 * Created by Dell1 on 21-Jul-17.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import app5.test5.bananaapps.snapeatsapp.R;

public class FoodTypeList implements Serializable {

    String id;
    String foodType;
    public FoodTypeList(String id, String foodType) {
        this.id = id;
        this.foodType = foodType;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodType() {
        return foodType;
    }


}
