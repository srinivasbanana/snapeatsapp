package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import app5.test5.bananaapps.snapeatsapp.R;

/**
 * Created by Android on 6/25/2016.
 */
public class DrawerMenuAdapter extends BaseAdapter {

    // variable initialization
    private Context context;
    private ArrayList<SideMenu> nenuItems;

    public DrawerMenuAdapter(Context applicationContext, ArrayList<SideMenu> menuItems) {
        this.context = applicationContext;
        this.nenuItems = menuItems;
    }

    @Override
    public int getCount() {
        // return menu list size
        return nenuItems.size();
    }

    @Override
    public Object getItem(int position) {
        // return menu list position
        return nenuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        // item position
        return position;
    }

    // ViewGropu for list of item to sidemenu bar
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.sidemenu_list_row, null);

        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.menu_icon);
        // Side menu list item name
        TextView txtTitle = (TextView) convertView.findViewById(R.id.menu_title);

        imgIcon.setImageResource(nenuItems.get(position).getIcon());
        txtTitle.setText(nenuItems.get(position).getTitle());

        return convertView;
    }

}
