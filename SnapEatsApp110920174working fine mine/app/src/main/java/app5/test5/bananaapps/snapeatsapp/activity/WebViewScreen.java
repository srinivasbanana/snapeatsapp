package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;

/**
 * Created by BananaApps on 6/7/2017.
 */

public class WebViewScreen extends Activity {
    TextView toolbartitle;
    ImageView backArrow;
    Button webview;
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        backArrow = (ImageView)findViewById(R.id.backArrow);
        webview = (Button) findViewById(R.id.webview);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        webView=(WebView)findViewById(R.id.webView);
        webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl("http://snapeats.co.uk/");

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WebViewScreen.this, TermsConditions.class));
                finish();
            }
        });

        webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WebViewScreen.this, HomePage.class));
                finish();
            }
        });


    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
