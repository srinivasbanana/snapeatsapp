package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.CameraCapture;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;


/**
 * Created by BananaApps on 5/29/2017.
 */

public class Profile extends BaseActivity {
    Toolbar toolbar;
    ImageView backArrow,imageView2;
    TextView username,cpwd;
    LinearLayout change_password;
    TextView toolbartitle,city,info,fname,lname,email,phone,editAndSave;
    TextView curpwd,npwd,cfpwd;
    Button changeAddress,save;
    EditText curpwdText,npwdText,cfpwdText,fnameText,lnameText,emailText,phoneText;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    ProgressDialog progressDialog;
    String fn,ln,ph,em,uid;
    JSONObject resultJsonObject, profileResult,changePassword;
    String responseMessage,EditOrPassword="";
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    String current_password="",newPWD;
    private String userChooseTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String profileImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        backArrow = (ImageView)findViewById(R.id.backArrow);
        uid=(String) Cache.getData(CatchValue.USER_ID, Profile.this);
        if(!TextUtils.isEmpty(uid)){
            new GetUpdatedProfile().execute(uid);
        }
        fnameText = (EditText)findViewById(R.id.firstnamedata);
        lnameText = (EditText)findViewById(R.id.lastnamedata);
        emailText = (EditText)findViewById(R.id.emaildata);
        phoneText = (EditText) findViewById(R.id.phonedata);
        username = (TextView)findViewById(R.id.username);
        cpwd = (TextView)findViewById(R.id.cpwd);
        curpwdText = (EditText)findViewById(R.id.currpwd);
        npwdText = (EditText)findViewById(R.id.newpwd);
        cfpwdText = (EditText)findViewById(R.id.cnfpwd);
        editAndSave = (TextView)findViewById(R.id.editAndSave);
        imageView2=(ImageView)findViewById(R.id.imageView2);

        change_password = (LinearLayout)findViewById(R.id.cnange_password);

        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        changeAddress = (Button)findViewById(R.id.changeAddress);
        save = (Button)findViewById(R.id.save);


        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);
        final String locality = settings.getString("locality", "");
        //   current_password = settings.getString("sppassword","");
        city = (TextView)findViewById(R.id.city);
       // city.setText(locality);
        fnameText.setEnabled(false);
        lnameText.setEnabled(false);
        emailText.setEnabled(false);
        phoneText.setEnabled(false);
        phoneText.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(11)});
        editAndSave.setText("Edit");
        editAndSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editAndSave.getText().toString()=="Edit") {

                    editAndSave.setText("Save");
                    editAndSave.setTextColor(Color.GREEN);
                    fnameText.setEnabled(true);
                    lnameText.setEnabled(true);
                    //  emailText.setEnabled(false);
                    phoneText.setEnabled(true);

                }
                else {

                    if(!IsEmpty(fnameText.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter First name");
                    }
                    else if(!IsEmpty(lnameText.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter Last name");
                    }
                    else if(!IsEmpty(emailText.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter Email ID");
                    }
                    else if(!IsValidEmail(emailText.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter Valid Email ID");
                    }
                    else if(!IsEmpty(phoneText.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter Phone number");
                    }
                    else if(!IsLength(phoneText.getText().toString().trim()))
                    {
                        okMessage("Error","Please enter 11 digits Phone number");
                    }
                    else if (!phoneText.getText().toString().substring(0,1).equals("0"))
                    {
                        okMessage("Error","Please enter correct phone number");
                    }

                    else {
                        SharedPreferences.Editor editor2 = settings.edit();
                  /*      editor2.putString("fname",fnameText.getText().toString().trim());
                        editor2.putString("lname",lnameText.getText().toString().trim());
                        editor2.putString("email",emailText.getText().toString().trim());
                        editor2.putString("phone",phoneText.getText().toString().trim());
                        editor2.commit();*/

                        String fn=fnameText.getText().toString().trim();
                        String ln=lnameText.getText().toString().trim();
                        String ph=phoneText.getText().toString().trim();
                        String em=emailText.getText().toString().trim();

                        imageView2.buildDrawingCache();
                        Bitmap bitmap =  imageView2.getDrawingCache();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] b = baos.toByteArray();
                        profileImg = Base64.encodeToString(b, Base64.DEFAULT);

                        Cache.putData(CatchValue.FIRST_NAME, getApplicationContext(), fn, Cache.CACHE_LOCATION_DISK);
                        Cache.putData(CatchValue.LAST_NAME, getApplicationContext(), ln, Cache.CACHE_LOCATION_DISK);
                        Cache.putData(CatchValue.PHONE, getApplicationContext(), ph, Cache.CACHE_LOCATION_DISK);



                        username.setText(fnameText.getText().toString().trim()+" "+lnameText.getText().toString().trim());
                        editAndSave.setText("Edit");
                        editAndSave.setTextColor(getResources().getColor(R.color.snapeatcolor));
                        fnameText.setEnabled(false);
                        lnameText.setEnabled(false);
                        phoneText.setEnabled(false);

                        cd = new ConnectionDetector(Profile.this);
                        isInternetPresent = cd.isConnectionAvailable();
                        if (isInternetPresent) {
                            //new Login.LoginTask().execute(em.getText().toString().trim(), pw.getText().toString().trim());
                            EditOrPassword="";
                            new ProfileEditTask().execute(uid,fn,ln,ph,profileImg);
                        }
                        else
                        {
                            ShowNoInternetDialog();
                        }

                    }
                }
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //okMessage("Profile","profile");
                final CharSequence[] items = {getResources().getString(R.string.text_take_photo), getResources().getString(R.string.text_choose_from_gallery)};
                AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);
                builder.setTitle(getResources().getString(R.string.text_add_photo));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        boolean result = CameraCapture.checkPermission(Profile.this);

                        if (items[item].equals(getResources().getString(R.string.text_take_photo))) {
                            userChooseTask = getResources().getString(R.string.text_take_photo);
                            if (result)
                                // okMessage("cameraIntent","");
                                cameraIntent();
                        } else if (items[item].equals(getResources().getString(R.string.text_choose_from_gallery))) {
                            userChooseTask = getResources().getString(R.string.text_choose_from_gallery);
                            if (result)
                                //  okMessage("galleryIntent","");
                                galleryIntent();
                        }
                    }
                });
                builder.show();

            }
        });

        fname = (TextView)findViewById(R.id.fname);
        lname = (TextView)findViewById(R.id.lname);
        email = (TextView)findViewById(R.id.email);
        phone = (TextView)findViewById(R.id.phone);

        curpwd = (TextView)findViewById(R.id.curpwd);
        npwd = (TextView)findViewById(R.id.npwd);
        cfpwd = (TextView)findViewById(R.id.cfpwd);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);
        changeAddress.setTypeface(custom_font2);
        fname.setTypeface(custom_font2);
        save.setTypeface(custom_font2);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showToast("Current "+current_password);
                //showToast("curpwd "+curpwdText.getText().toString().trim());
                current_password=(String) Cache.getData(CatchValue.PASSWORD, Profile.this);
                if(curpwdText.getText().toString().trim().length()==0)
                {
                    showToast("Please enter current password.");
                }
                else
                if(npwdText.getText().toString().trim().length()==0)
                {
                    showToast("Please enter new password.");
                }
                else
                if(cfpwdText.getText().toString().trim().length()==0)
                {
                    showToast("Please enter Confirm password.");
                }
                else
                if(!current_password.equals(curpwdText.getText().toString().trim())) {
                    showToast("Current password is wrong \nTry again...");
                    curpwdText.setText("");
                }
                else
                if(!npwdText.getText().toString().trim().equals(cfpwdText.getText().toString().trim())) {
                    showToast("New password and Confirm passwords are mismatch. Try again...");
                    npwdText.setText("");
                    cfpwdText.setText("");
                }
                else
                {
                    newPWD=npwdText.getText().toString().trim();
                    cd = new ConnectionDetector(Profile.this);
                    isInternetPresent = cd.isConnectionAvailable();
                    if (isInternetPresent) {
                        EditOrPassword="PASSWORD";
                        //new Login.LoginTask().execute(em.getText().toString().trim(), pw.getText().toString().trim());
                        new PasswordChangeTask().execute(uid,newPWD,current_password);
                    }
                    else
                    {
                        ShowNoInternetDialog();
                    }
                    SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor2 = settings.edit();
                    editor2.putString("sppassword",npwdText.getText().toString().trim());
                    editor2.commit();
                  /*  showToast("Password changed successfully.\nPlease login again");*/

                }


            }
        });

        changeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Profile.this, AddressChange.class));
                finish();
            }
        });

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "Lato-Regular.ttf");
        fname.setTypeface(custom_font);
        lname.setTypeface(custom_font);
        email.setTypeface(custom_font);
        phone.setTypeface(custom_font);
        cpwd.setTypeface(custom_font);

        curpwd.setTypeface(custom_font);
        npwd.setTypeface(custom_font);
        cfpwd.setTypeface(custom_font);

        fnameText.setTypeface(custom_font2);
        lnameText.setTypeface(custom_font2);
        emailText.setTypeface(custom_font2);
        phoneText.setTypeface(custom_font2);



        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
     /*   final String fname = settings.getString("fname", "");
        final String lname = settings.getString("lname", "");
        final String email = settings.getString("email", "");
        final String phone = settings.getString("phone", "");*/



        String fname=(String) Cache.getData(CatchValue.FIRST_NAME, Profile.this);
        String lname=(String) Cache.getData(CatchValue.LAST_NAME, Profile.this);
        String email=(String) Cache.getData(CatchValue.EMAIL, Profile.this);
        String phone=(String) Cache.getData(CatchValue.PHONE, Profile.this);



        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Profile.this, HomePage.class));
                finish();
            }
        });

        cpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change_password.setVisibility(View.VISIBLE);
            }
        });
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(Profile.this, Login.class));
        finish();
        super.onBackPressed();
    }
    public void ShowNoInternetDialog() {
        showAlertDialog(this, "No Internet Connection", "Please check your network.", false);
    }
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                cd = new ConnectionDetector(Profile.this);
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    if(EditOrPassword.equalsIgnoreCase("PASSWORD"))
                    {
                        EditOrPassword="";
                        new PasswordChangeTask().execute(uid,newPWD,current_password);
                    }else
                        new ProfileEditTask().execute(uid,fn,ln,ph,"");
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }

    private class ProfileEditTask extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();

        }

        @Override
        protected Report doInBackground(String... params) {
            try {

                JSONObject jObject = new JSONObject();
                jObject.put("uid", params[0]);
                jObject.put("fname", params[1]);
                jObject.put("lname", params[2]);
                jObject.put("phone", params[3]);
                jObject.put("user_pic", params[4]);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/user/EditProfile");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                gerEditResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again" );
            }
        }
    }

    private class PasswordChangeTask extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();

        }

        @Override
        protected Report doInBackground(String... params) {
            try {
                JSONObject jObject = new JSONObject();
                jObject.put("uid", params[0]);
                jObject.put("password", params[1]);
                jObject.put("OldPassword", params[2]);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/user/ChangePassword");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                getChangePwdResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again" );
            }
        }
    }


    public void showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }
    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void gerEditResponse(Report response) {
        try {
            if (response.getStatus().equalsIgnoreCase("true")) {
                resultJsonObject = response.getJsonObject();
                if(resultJsonObject.length()>0&&resultJsonObject!=null) {
                    responseMessage=response.getJsonObject().getString("Message");
                    showToastMessage(responseMessage);
                    new GetUpdatedProfile().execute(uid);
                }
                else {
                    showToastMessage("Some thing went wrong please check once");
                }
            }
            else {
                if ((response.getStatus().equalsIgnoreCase("false"))) {
                    responseMessage = response.getMessage();
                    if (response.getErr_code() == 400) {
                        showToastMessage(responseMessage);
                    }
                    else if (response.getErr_code() == 401) {
                        showToastMessage(responseMessage);
                    } else if (response.getErr_code() == 500) {
                        showToastMessage(responseMessage);
                    } else {
                        showToastMessage(responseMessage);
                    }
                }
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }

    private void getChangePwdResponse(Report response) {
        try {
            if (response.getStatus().equalsIgnoreCase("true")) {

                changePassword = response.getJsonObject();
                if(changePassword.length()>0&&changePassword!=null) {
                    responseMessage=response.getJsonObject().getString("Message");
                    showToastMessage(responseMessage);
                    Cache.putData(CatchValue.USER_ID, Profile.this, "", Cache.CACHE_LOCATION_DISK);
                    // Cache.putData(CatchValue.USER_ID, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.Address1, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.Address2, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.EMAIL, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.FIRST_NAME, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.LAST_NAME, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PASSWORD, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    Cache.putData(CatchValue.PHONE, getApplicationContext(), "", Cache.CACHE_LOCATION_DISK);
                    startActivity(new Intent(Profile.this, Login.class));
                    finish();
                }
                else {
                    showToastMessage("Some thing went wrong please check once");
                }
            }
            else {

                if ((response.getStatus().equalsIgnoreCase("false"))) {
                    responseMessage = response.getMessage();
                    if (response.getErr_code() == 400) {
                        showToastMessage(responseMessage);
                    }
                    else if (response.getErr_code() == 401) {
                        showToastMessage(responseMessage);
                    } else if (response.getErr_code() == 500) {
                        showToastMessage(responseMessage);
                    } else {
                        showToastMessage(responseMessage);
                    }
                }
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.text_select_file)), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                //    showToastMessage("SELECT_FILE");
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                // showToastMessage("REQUEST_CAMERA");
                onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageView2.setImageBitmap(bm);
        //  capturedata.setVisibility(View.INVISIBLE);
        //   capturedataResult.setVisibility(View.VISIBLE);
    }
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".PNG");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageView2.setImageBitmap(thumbnail);
        // capturedata.setVisibility(View.INVISIBLE);
        // capturedataResult.setVisibility(View.VISIBLE);
    }


    private class GetUpdatedProfile extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();

        }

        @Override
        protected Report doInBackground(String... params) {
            response = new ServiceClass().getJsonObjectResponse("http://seapi.testersworld.com/api/User/Profile?UserID="+uid);
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                getUpdateResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again" );
            }
        }
    }

    private void getUpdateResponse(Report response) {
        try {
            profileResult = response.getJsonObject();
            if (profileResult.length()>0&&profileResult!=null) {
                fnameText.setText(profileResult.getString("fname"));
                lnameText.setText(profileResult.getString("lname"));
                emailText.setText(profileResult.getString("email"));
                emailText.setSelected(true);
                phoneText.setText(profileResult.getString("phone"));
                username.setText(profileResult.getString("fname")+" "+profileResult.getString("lname"));
                imageView2.setImageBitmap(bitmapConvert(profileResult.getString("user_pic")));
            }
            else {
                if ((response.getStatus().equalsIgnoreCase("false"))) {
                    responseMessage = response.getMessage();
                    if (response.getErr_code() == 400) {
                        showToastMessage(responseMessage);
                    }
                    else if (response.getErr_code() == 401) {
                        showToastMessage(responseMessage);
                    } else if (response.getErr_code() == 500) {
                        showToastMessage(responseMessage);
                    } else {
                        showToastMessage(responseMessage);
                    }
                }
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }

    private Bitmap bitmapConvert(String Image) {
        byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
