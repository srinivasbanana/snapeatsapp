package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;

public class FragmentAll extends Fragment {

    ImageView iv,iv2;
    ImageView close;
    TextView sorryText;

    public FragmentAll() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =inflater.inflate(R.layout.fragment_all, container, false);
        iv = (ImageView) view.findViewById(R.id.truiton_image);
        iv2 = (ImageView) view.findViewById(R.id.truiton_image2);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(),ItemOneFragmentTabsView.class);
                startActivity(i);
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_sorry);
                close = (ImageView)dialog.findViewById(R.id.cross);
                sorryText = (TextView)dialog.findViewById(R.id.sorryText);

                Typeface custom_font2 = Typeface.createFromAsset(getActivity().getAssets(),  "MavenProLight-300.otf");
                sorryText.setTypeface(custom_font2);

                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      //  Toast.makeText(getActivity(),"close ",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
                dialog.show();
        }
        });




        return view;
    }

}
