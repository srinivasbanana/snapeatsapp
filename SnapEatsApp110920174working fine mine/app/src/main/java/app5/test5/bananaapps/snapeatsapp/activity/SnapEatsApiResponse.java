package app5.test5.bananaapps.snapeatsapp.activity;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.ArrayList;

//import citymovies.com.citymovies.models.Movie;

public class SnapEatsApiResponse {

    @SerializedName("Restaurants")
    public ArrayList<Shop> shopArray;

    @SerializedName("Groceries")
    public ArrayList<Shop> groceriesArray;

    @SerializedName("Msg")
    public JSONObject msg;

    @SerializedName("MenuDetails")
    public ArrayList<menuDetails> menuDetails;

    @SerializedName("ItemDetails")
    public ArrayList<ItemDetails> itemDetailses;

}
