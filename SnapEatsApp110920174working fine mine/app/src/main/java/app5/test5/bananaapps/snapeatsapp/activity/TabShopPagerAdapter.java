package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import app5.test5.bananaapps.snapeatsapp.R;
/*import citymovies.com.citymovies.fragments.LatestFragment;
import citymovies.com.citymovies.fragments.NearByMoviesFragment;
import citymovies.com.citymovies.fragments.RatingMoviesFragment;
import citymovies.com.citymovies.fragments.UpcomingMoviesFragment;
import citymovies.com.citymovies.models.Movie;
import citymovies.com.citymovies.utils.Constants;*/

public class TabShopPagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private ArrayList<ShopList> All;
    private ArrayList<ShopList> Resturants;
    private ArrayList<ShopList> Groceries;
    private ArrayList<ShopList> Fashion;


    public TabShopPagerAdapter(FragmentManager fm, Context context, ArrayList<ShopList> All,
                                 ArrayList<ShopList> Resturants, ArrayList<ShopList> Groceries,
                                 ArrayList<ShopList> Fashion, int selectedId) {


        super(fm);
        mContext = context;
        this.All = All;
        this.Resturants = Resturants;
        this.Groceries = Groceries;
        this.Fashion = Fashion;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
            switch (i) {
                case 0:
                    //  FragmentAll latestFragment = new FragmentAll();
                    fragment = new All();

                    Bundle bundle = new Bundle(i);
                    bundle.putBoolean("ISFROMMOVIES", true);
                    bundle.putParcelableArrayList("LIST", All);
                    fragment.setArguments(bundle);
//                return latestFragment;
                    break;
                case 1:
                    fragment = new FragmentRestaurants();
                    Bundle bundleNearBy = new Bundle(i);
                    bundleNearBy.putBoolean("ISFROMRESTRUNT", true);
                    bundleNearBy.putParcelableArrayList("LISTData", Resturants);
                    fragment.setArguments(bundleNearBy);
//                return nearByMoviesFragment;
                    break;
                case 2:
                    fragment = new FragmentGroceries();
                    Bundle ratings = new Bundle(i);
                    ratings.putBoolean("ISFROMGroceries", true);
                    ratings.putParcelableArrayList("LISTGroceries", Groceries);
                    fragment.setArguments(ratings);
//                return ratingMoviesFragment;
                    break;
                case 3:
                    fragment = new FragmentFashion();
                    Bundle fashion = new Bundle(i);
                    fashion.putBoolean("ISFROMFashion", true);
                    fashion.putParcelableArrayList("LISTFashion", Fashion);
                    fragment.setArguments(fashion);
             /*   UpcomingMoviesFragment upcomingMoviesFragment = new UpcomingMoviesFragment();
                Bundle upcoming = new Bundle(2);
                upcoming.putBoolean(Constants.ISFROMMOVIES, true);
                upcoming.putParcelableArrayList(Constants.LIST, upcomingMovies);
                upcomingMoviesFragment.setArguments(upcoming);*/
//                return upcomingMoviesFragment;
                    break;
                default:
            }
        return fragment;


    }



    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int i) {
        String string="";
        switch (i) {
            case 0:
              string=mContext.getString(R.string.all);
                break;
            case 1:
                string= mContext.getString(R.string.restaurants);
                break;
            case 2:
                string=mContext.getString(R.string.groceries);
                break;
            case 3:
                string=mContext.getString(R.string.fashion);
            break;
            default:
        }
        return string;
    }
}
