package app5.test5.bananaapps.snapeatsapp.activity;

/**
 * Created by BananaApps on 6/1/2017.
 */
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.TextView;
import app5.test5.bananaapps.snapeatsapp.R;


public class HistoryList extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] web;
    private final String[] dist;


    public HistoryList(Activity context,
                      String[] web, String[] dist) {
        super(context, R.layout.history_list_single, web);
        this.context = context;
        this.web = web;
        this.dist = dist;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.history_list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        TextView distance = (TextView) rowView.findViewById(R.id.distancet);
        txtTitle.setText(web[position]);
        distance.setText(dist[position]);
        return rowView;
    }
}