package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

import java.io.Serializable;

/**
 * Created by Android on 6/25/2016.
 */
public class SideMenu implements Serializable {

    String title;
    int icon;

    public SideMenu(String name, int icon) {
        this.icon = icon;
        this.title = name;
    }

    public String getTitle() {

        return this.title;
    }

    public int getIcon() {

        return this.icon;
    }
}
