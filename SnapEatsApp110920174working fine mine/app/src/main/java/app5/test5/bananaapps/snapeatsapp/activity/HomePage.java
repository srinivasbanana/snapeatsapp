package app5.test5.bananaapps.snapeatsapp.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.DrawerMenuAdapter;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SideMenu;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static app5.test5.bananaapps.snapeatsapp.R.id.toolbar_title;

/**
 * Created by BananaApps on 5/31/2017.
 */

public class HomePage extends BaseActivity  implements LocationListener{

    public static final String PREFS_NAME = "SnapEatsVariables";
    private LocationManager locationManager;
    private LocationListener listener;
    private TrackGPS gps;
    double longitude;
    double latitude;
    Location location;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    ArrayList<ShopList> list;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    boolean canGetLocation = false;
    int locStatus=0;
    String userId;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    private int isHSelected,isSSelected,isCSelected,isOHSelected = 0;

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        gpsDetection();

        configure_button();
        final BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        final Menu menu = bottomNavigationView.getMenu();
        isHSelected=1;
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_item1:
                                if(isHSelected==0) {
                                    menu.getItem(0).getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                                    menu.getItem(1).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    menu.getItem(2).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    // bottomNavigationView.setBackgroundColor(getResources().getColor(R.color.snapeatcolor));
//                                        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.home_updated));
//                                        menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.search));
//                                        menu.getItem(2).setIcon(getResources().getDrawable(R.drawable.camera));
//                                        menu.getItem(3).setIcon(getResources().getDrawable(R.drawable.order_history));
                                    isHSelected=1;isSSelected=0;isCSelected=0;isOHSelected=0;
                                    selectedFragment = ItemOneFragmentNew.newInstance();
                                }
                                break;
                            case R.id.action_item2:
                                if(isSSelected==0) {
                                    menu.getItem(1).getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                                    menu.getItem(0).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    menu.getItem(2).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
//                                        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.home));
//                                        menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.search_updated));
//                                        menu.getItem(2).setIcon(getResources().getDrawable(R.drawable.camera));
//                                        menu.getItem(3).setIcon(getResources().getDrawable(R.drawable.order_history));
                                    isSSelected=1;isHSelected=0;isCSelected=0;isOHSelected=0;
                                    selectedFragment = new ItemTwoFragment();
                                }

                                break;
                            case R.id.action_item3:
                                if(isCSelected==0) {
                                    menu.getItem(2).getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                                    menu.getItem(0).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                                    menu.getItem(1).getIcon().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
//                                        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.home));
//                                        menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.search));
//                                        menu.getItem(2).setIcon(getResources().getDrawable(R.drawable.camera_updated));
//                                        menu.getItem(3).setIcon(getResources().getDrawable(R.drawable.order_history));
                                    isCSelected=1;isHSelected=0;isSSelected=0;isOHSelected=0;
                                    selectedFragment = ItemThreeFragment.newInstance();
                                }

                                break;
//                                case R.id.action_item4:
//                                    if(isOHSelected==0) {
////                                        menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.home));
////                                        menu.getItem(1).setIcon(getResources().getDrawable(R.drawable.search));
////                                        menu.getItem(2).setIcon(getResources().getDrawable(R.drawable.camera));
////                                        menu.getItem(3).setIcon(getResources().getDrawable(R.drawable.shopping_updated));
//                                        isCSelected=0;isHSelected=0;isSSelected=0;isOHSelected=1;
//                                        selectedFragment = ItemFourFragment.newInstance();
//                                    }
//                                    break;

                        }
                        if(selectedFragment!=null) {
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_layout, selectedFragment);
                            transaction.commit();
                        }
                        return true;
                    }
                });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, ItemOneFragmentNew.newInstance());
        transaction.commit();
    }
    void configure_button(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
    }
/*void configure_button(){
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.INTERNET,Manifest.permission.CALL_PHONE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.ACCESS_WIFI_STATE,Manifest.permission.ACCESS_NETWORK_STATE}
                    ,10);
        }
        return ;
    }
    locationManager.requestLocationUpdates("gps", 1000, 0, listener);
}*/

    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private Location gpsDetection() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (!gps_enabled || ! network_enabled) {
                // no network provider is enabled
                SharedPreferences settings = this.getSharedPreferences(PREFS_NAME, this.MODE_PRIVATE);
                String locEnableStatus = settings.getString("locEnableStatus", "");
                if(locEnableStatus.length()==0)
                    showSettingsAlert();
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (network_enabled) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (gps_enabled) {
                    if (location == null) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            //    here to request the missing permissions, and then overriding
                            //    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return location;
                        }
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                //latitude = 17.6868;
                                //longitude = 83.2185;

                            }
                        }
                    }

                    Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.ENGLISH);
                    String result = null;
//                  Toast.makeText(HomePage.this,"getAddressFromLocation ",Toast.LENGTH_LONG).show();
                    try {
                        List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 2);
                        if (addressList != null && addressList.size() > 0) {
                            Address address = addressList.get(0);
                            StringBuilder sb = new StringBuilder();
                            SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                            //    Log.e("SnapEatsTest","test success 2");
                            //   String  isAddressChanged = settings.getString("isAddressChanged", "");
                            //   Log.e("SnapEatsTest","isAddressChanged "+isAddressChanged);
                            //      Toast.makeText(getApplication(),"isAddressChanged length "+isAddressChanged.length(),Toast.LENGTH_LONG).show();
                            //  if(isAddressChanged.length()==0)
                            {
                                //     Log.e("SnapEatsTest","isAddressChanged 2 "+isAddressChanged.length());
                                SharedPreferences.Editor editor2 = settings.edit();
                                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                    sb.append(address.getAddressLine(i)).append("\n");
                                    editor2.putString("adrs"+i,address.getAddressLine(i).toString());
                                    if(i==0)
                                        Cache.putData(CatchValue.Delivary_adrs0, getApplicationContext(), address.getAddressLine(i).toString(), Cache.CACHE_LOCATION_DISK);
                                    else
                                    if(i==1)
                                        Cache.putData(CatchValue.Delivary_adrs1, getApplicationContext(), address.getAddressLine(i).toString(), Cache.CACHE_LOCATION_DISK);
                                }
                                sb.append(address.getLocality()).append("\n");
                                sb.append(address.getPostalCode()).append("\n");
                                sb.append(address.getCountryName());
                                editor2.putString("locality",address.getLocality().toString());
                                editor2.putString("postalcode",address.getPostalCode().toString());
                                editor2.putString("countryname",address.getCountryName().toString());

                                Cache.putData(CatchValue.Delivary_location, getApplicationContext(),address.getLocality().toString(), Cache.CACHE_LOCATION_DISK);
                                Cache.putData(CatchValue.Delivary_pincode, getApplicationContext(), address.getPostalCode().toString(), Cache.CACHE_LOCATION_DISK);
                                Cache.putData(CatchValue.Delivary_country, getApplicationContext(), address.getCountryName().toString(), Cache.CACHE_LOCATION_DISK);

                                // editor2.putLong("CurrentLatitude", Double.doubleToLongBits(address.getLatitude()));
                                // editor2.putLong("CurrentLongitude", Double.doubleToLongBits(address.getLongitude()));
                                editor2.putString("CurrentLatitude",""+latitude);
                                editor2.putString("CurrentLongitude",""+longitude);
                               // editor2.putString("CurrentLatitude",""+17.6868);
                               // editor2.putString("CurrentLongitude",""+83.2185);

                                editor2.putString("promoCode","");
                                Log.e("CurrentLatitude ",""+address.getLatitude());
                                //  editor2.putString("CurrentLongitude",""+address.getLongitude());
                                editor2.commit();
                                result = sb.toString();
                            }
                        }
                    } catch (IOException e) {

                    } finally {
                        Message message = Message.obtain();
                        if (result != null) {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            result = "Latitude: " + latitude + " Longitude: " + longitude +
                                    "\n\nAddress:\n" + result;

                            bundle.putString("address", result);
                            message.setData(bundle);
                        } else {
                            message.what = 1;
                            Bundle bundle = new Bundle();
                            result = "Latitude: " + latitude + " Longitude: " + longitude +
                                    "\n Unable to get address for this lat-long.";
                            bundle.putString("address", result);
                            message.setData(bundle);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }
    @Override
    public void onLocationChanged(Location location) {
        if(location==null) {
            Toast.makeText(getApplicationContext(),"Please Enable location",Toast.LENGTH_SHORT ).show();
        }
        else {
            // Toast.makeText(getApplicationContext(),"Current Loaction latitude, longitude"+getLatitude(location.getLatitude())+","+ getLongitude(location.getLongitude()),Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {
        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(i);
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     * */
    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(HomePage.this);
        }
    }

    /**
     * Function to get latitude
     * */
    private double getLatitude(double latitude){
        if(location != null){
            this.latitude = latitude;
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public double getLongitude(double longitude){
        if(location != null){
            this.longitude = longitude;
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     * @return boolean
     * */
    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     * */

    public void showSettingsAlert() {
        locStatus++;
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor2 = settings.edit();
        editor2.putString("locEnableStatus","Enabled");
        editor2.commit();

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                HomePage.this);
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        HomePage.this.startActivity(intent);
                        dialog.cancel();
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        startActivity(new Intent(HomePage.this, PlacesAutoCompleteActivity.class));

                    }
                });
        alertDialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        gpsDetection();
    }



    @Override
    public void onBackPressed() {
        userId = (String) Cache.getData(CatchValue.USER_ID, HomePage.this);
        if(!TextUtils.isEmpty(userId)){
            moveTaskToBack(true);
        }
        else{
            finish();
        }

    }

}

