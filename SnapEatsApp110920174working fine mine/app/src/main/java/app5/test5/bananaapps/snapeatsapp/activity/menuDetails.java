package app5.test5.bananaapps.snapeatsapp.activity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class menuDetails implements Parcelable {

    @SerializedName("menuname")
    @Expose
    private String menuname;

    @SerializedName("shopList")
    @Expose
    private ArrayList<ShopList> shopList = new ArrayList<>();


    protected menuDetails(Parcel in) {
        menuname = in.readString();
        shopList = in.createTypedArrayList(ShopList.CREATOR);

       }

    protected menuDetails(String menuname,ArrayList<ShopList> shopList) {
        this.menuname=menuname;
        this.shopList=shopList;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(menuname);
        dest.writeTypedList(shopList);
    }
    /**
     * @return The shopList
     */
   public ArrayList<ShopList> getShopList() {
        return shopList;
    }

    /**
     * @param shopList The MovieImages
     */
    public void setShopList(ArrayList<ShopList> shopList) {
        this.shopList = shopList;
    }

    public String getMenuName() {
        return menuname;
    }

    public void setMenuName(String menuname) {
        this.menuname = menuname;
    }



    public static final Creator<menuDetails> CREATOR = new Creator<menuDetails>() {
        @Override
        public menuDetails createFromParcel(Parcel in) {
            return new menuDetails(in);
        }

        @Override
        public menuDetails[] newArray(int size) {
            return new menuDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}
