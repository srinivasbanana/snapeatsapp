package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import app5.test5.bananaapps.snapeatsapp.R;

/**
 * Created by BananaApps on 6/6/2017.
 */

public class History extends BaseActivity  {
    TextView toolbartitle;
    ImageView backArrow;
    String[] web = {
            "ID : 1001",
            "ID : 1002",
            "ID : 1003",
            "ID : 1004",
            "ID : 1005",
            "ID : 1006",
            "ID : 1007",
            "ID : 1008",
            "ID : 1009",
            "ID : 1010"
    };
    String[] dist = {
            "KFC",
            "MC DONALD",
            "DOMINAS",
            "PIZZA HUT",
            "DALPHINE",
            "DASPULLA",
            "DFC",
            "KFC",
            "MC DONALD",
            "DOMINAS"
    } ;

    public static final String PREFS_NAME = "SnapEatsVariables";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        backArrow = (ImageView)findViewById(R.id.backArrow);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(History.this, HomePage.class));
                finish();
            }
        });

        ListView list;

        HistoryList adapter = new
                HistoryList(History.this, web, dist);
/*        list=(ListView)findViewById(R.id.listHistory);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("SubHistoryTitle",dist[+ position].toString());
                editor.commit();

                startActivity(new Intent(History.this, SubHistoryList.class));
                finish();
               // Toast.makeText(History.this, "You Clicked at " +dist[+ position], Toast.LENGTH_SHORT).show();
            }
        });*/
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(History.this, HomePage.class));
        finish();
        super.onBackPressed();
    }
}
