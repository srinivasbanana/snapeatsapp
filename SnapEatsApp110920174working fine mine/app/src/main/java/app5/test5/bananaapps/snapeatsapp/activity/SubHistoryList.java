package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by BananaApps on 6/14/2017.
 */
import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;

public class SubHistoryList extends BaseActivity {
    TextView subtitle;
    TextView toolbartitle,pickFrom,delTo;
    ImageView backArrow;
    ImageButton grid;
    boolean flag=false;
    ListView list;
    GridView gridv;
    Button orderAgain;

    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;

    String[] web = {
            "KFC",
            "MC DONALD"
    } ;
    Integer[] imageId = {
            R.drawable.shop6,
            R.drawable.shop3
    };
    String[] dist = {
            "Wrap with Tomato Ketchup",
            "Wrap with Tomato Ketchup"
    } ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_history_list);

        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        pickFrom = (TextView)findViewById(R.id.pickedFrom);
        delTo = (TextView)findViewById(R.id.deliveredTo);
        backArrow = (ImageView)findViewById(R.id.backArrow);
        grid = (ImageButton) findViewById(R.id.grid);
        list=(ListView)findViewById(R.id.historylist);
        gridv=(GridView)findViewById(R.id.gridview);
        orderAgain = (Button)findViewById(R.id.orderAgain);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        subtitle = (TextView)findViewById(R.id.SubHistoryTitle);

        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);
        final String SubHistory = settings.getString("SubHistoryTitle", "");
        subtitle.setText(SubHistory);
        pickFrom.setText(settings.getString("adrs0", ""));
        delTo.setText(settings.getString("adrs1", ""));

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SubHistoryList.this, History.class));
                finish();
            }
        });

        orderAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SubHistoryList.this, Basket.class));
                finish();
            }
        });
        grid .setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!flag) {
                    grid.setBackgroundResource(R.mipmap.listgrid);
                    flag=true;
                    showlist();
                    gridv.setVisibility(View.VISIBLE);
                    list.setVisibility(View.GONE);

                }
                else {
                    grid.setBackgroundResource(R.mipmap.grid);
                    flag=false;
                    showgrid();
                    gridv.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);

                }
            }
        });
    showgrid();
    }

    public void showgrid()
    {
       // SubCategoryList adapter = newSubCategoryList(SubHistoryList.this, web, imageId,dist);
      // list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // Toast.makeText(getActivity(), "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showlist()
    {
        int[] imageId2 = {
                R.drawable.shop6,
                R.drawable.shop3
        };
        CustomGrid adapter = new CustomGrid(this, web, imageId2);

        gridv.setAdapter(adapter);
        gridv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // Toast.makeText(SubCategory.this, "You Clicked at " +web2[+ position], Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(SubHistoryList.this, History.class));
        finish();
        super.onBackPressed();
    }

}
