package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.FoodTypeList;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by BananaApps on 6/9/2017.
 */

public class ItemOneFragmentTabsView extends BaseActivity {
    TextView toolbartitle;
    ImageView backArrow;
    ImageView imageView;
    ImageView close;
    TextView sorryText;
    String headerText, image, locName;
    int id;
    ListView list;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    ProgressDialog progressDialog;
    private Spinner address;
    ArrayList<ItemDetails> itemDetailses;
    ArrayList<SubCategoryList> dataList = null;
    ArrayList<String> dataListName;
    ArrayList<ShopAddress> addressList;
    CategoryList adapter;
    JSONObject resultJsonObject,resultData;
    String sid,name,foodType,itemDesc,scid;
    String fImageurl;
    String price;
    String cname,scat_desc,scat_image,scat_name,sname;
    int cid,scat_price,scstatus,ssid;
    String sno;
    BucketDB bucketDB;
    ProgressDialog  progress;
    private List<SubCategoryList> savedQty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_item_one_tabs_view);
        toolbartitle = (TextView) findViewById(R.id.toolbar_title);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        sorryText = (TextView) findViewById(R.id.address);
        imageView = (ImageView) findViewById(R.id.sImage);
        list = (ListView) findViewById(R.id.listCategories);
        address = (Spinner) findViewById(R.id.locAddress);
        headerText = getIntent().getStringExtra("LName");
        if (!TextUtils.isEmpty(headerText)) {
            toolbartitle.setText(headerText);
        }
        image = getIntent().getStringExtra("image");
        id = getIntent().getIntExtra("id",0);
        locName = getIntent().getStringExtra("Type");
        addressList  = getIntent().getParcelableArrayListExtra("address");
        dataListName=new ArrayList<>();
        for(int i=0; i<addressList.size();i++){
            dataListName.add(addressList.get(i).getAddress1());
        }
       ArrayAdapter<String> adt=new ArrayAdapter<String>(ItemOneFragmentTabsView.this,R.layout.spinner_text_view, dataListName);
       adt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       address.setAdapter(adt);
//        address.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                try {
//
//                    if (i>0){
//
//                        districtName = distList.get(i).toString().trim();
//                        districtID = distListIds.get(i).toString().trim();
//                        Cache.putData(CatchValue.DISTRICT_NAME, TempleFilterActivity.this, districtName, Cache.CACHE_LOCATION_DISK);
//
//                        isInternetPresent = cd.isConnectionAvailable();
//                        if (isInternetPresent) {
//                            new GetCitiesTask().execute();
//                        } else {
//                            ShowNoInternetDialog();
//                        }
//
//                    }
//
//                }catch (Exception ex){
//                    ex.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });

        if (!TextUtils.isEmpty(image)) {
            new DownLoadImageTask(imageView).execute(image);
        } else {
            imageView.setImageResource(R.drawable.noimage);
        }

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(), "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowBackDialog();
            }
        });

        cd = new ConnectionDetector(ItemOneFragmentTabsView.this);
        isInternetPresent = cd.isConnectionAvailable();
        if (isInternetPresent) {
          //  moveItemDetails();
           new MenuList().execute();
        } else {
            ShowNoInternetDialog();
        }
        bucketDB=new BucketDB(this);
        //moveShops();
    }

    private class MenuList extends AsyncTask<String, Void, Report> {

        Report report = new Report();
        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Report doInBackground(String... strings) {

            try {

                ServiceClass utility = new ServiceClass();
//                report = utility.getJsonResponse("http://api.androidhive.info/json/imdb_top_250.php?offset=");
                report = utility.getJsonObjectResponse("http://seapi.testersworld.com/api/Items/items");

            } catch (Exception e) {
                e.printStackTrace();
            }

            return report;
        }

        @Override
        protected void onPostExecute(Report result) {
            dismissProgressDialog();
            if (result != null) {
                GetJsonResult(result);
            } else {
                Toast.makeText(ItemOneFragmentTabsView.this, "Something went wrong, please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void GetJsonResult(Report result) {

        dataList = new ArrayList<SubCategoryList>();
        dataListName = new ArrayList<String>();

        try {
            resultJsonObject  = result.getJsonObject();
            JSONObject status = resultJsonObject.getJSONObject("Msg");
            if(status.getString("isSuccess").equals("true") && status.getString("StatusCode").equalsIgnoreCase("200")) {
                if(resultJsonObject.length()!=0 && !resultJsonObject.equals("[]")) {
                    JSONArray resultJsonArray = resultJsonObject.getJSONArray("ItemDetails");
                    if (resultJsonArray.length() != 0 && !resultJsonArray.equals("[]")) {
                        for (int i = 0; i < resultJsonArray.length(); i++) {
                            resultData = resultJsonArray.getJSONObject(i);
                            sno = String.valueOf(resultData.getInt("cid"));
                            sid = String.valueOf(resultData.getInt("sid"));
                            scid = String.valueOf(resultData.getInt("scid"));
                            foodType = resultData.getString("cname");
                            name = resultData.getString("scat_name");
                            itemDesc = resultData.getString("scat_desc");
                            price = resultData.getString("scat_price");
                            fImageurl = resultData.getString("scat_image");
                            if (sid.equals(String.valueOf(id))) {
                                if (price.length() != 0) {
                                    SubCategoryList mark = new SubCategoryList(scid,sid, foodType, name, itemDesc, price, fImageurl, sno);
                                    dataList.add(mark);
                                }
                                dataListName.add(foodType);
                            }
                        }
                        Set hs = new HashSet<>();
                        hs.addAll(dataListName);
                        dataListName.clear();
                        dataListName.addAll(hs);
                        adapter = new CategoryList(ItemOneFragmentTabsView.this, dataList, dataListName);
                        list.setAdapter(adapter);
                    } else {
                        Toast.makeText(ItemOneFragmentTabsView.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                else {
                    Toast.makeText(ItemOneFragmentTabsView.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            else {
                if (result.getErr_code() == 401) {
                    Toast.makeText(ItemOneFragmentTabsView.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (result.getErr_code() == 500) {
                    Toast.makeText(ItemOneFragmentTabsView.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ItemOneFragmentTabsView.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private class DownLoadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public DownLoadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String... urls) {
            String urlOfImage = urls[0];
            Bitmap logo = null;

            try {
                if (!urlOfImage.contains("data:image/jpeg;base64")) {
                    InputStream in = new java.net.URL(urlOfImage).openStream();
                    logo = BitmapFactory.decodeStream(in);
                } else {
                    String actualBitmap = urlOfImage.substring(0, urlOfImage.indexOf(",") + 1);
                    urlOfImage = urlOfImage.replace(actualBitmap, "");
                    logo = bitmapConvert(urlOfImage);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return logo;
        }

        protected void onPostExecute(Bitmap result) {
            if(result!=null){
                imageView.setImageBitmap(result);
            }
            else {
                imageView.setImageResource(R.drawable.noimage);
            }
        }
    }

    private Bitmap bitmapConvert(String Image) {
        byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }


    public void ShowNoInternetDialog() {
        showAlertDialog(ItemOneFragmentTabsView.this, "No Internet Connection", "Please check your network.", false);
    }
    public void ShowBackDialog() {
        if(bucketDB.getAllDetails().size()>0){
            showAlert(ItemOneFragmentTabsView.this, "Clear Basket ?", "You can only order from one place. When you leave this place your current basket will be cleared.", false);
        }
        else{
            finish();
        }

    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    new MenuList().execute();
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }
    public void showAlert(Context context, String title, String message, Boolean status) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context,android.R.style.Theme_Material_Light_Dialog_Alert).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("Clear Basket", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //   alertDialog.dismiss();
                bucketDB.removeAllDataFromChampcash();
                progress= new ProgressDialog(ItemOneFragmentTabsView.this);
                progress.setMessage("Please wait...");

                progress.show();
                progress.setCancelable(false);

                Runnable progressRunnable = new Runnable() {

                    @Override
                    public void run() {
                        if (progress != null) {
                            progress.dismiss();
                        }
                    }
                };

                Handler pdCanceller = new Handler();
                pdCanceller.postDelayed(progressRunnable, 3000);
                Intent sublist= new Intent(ItemOneFragmentTabsView.this,HomePage.class);
                sublist.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(sublist);
            }
        });
        alertDialog.setButton2("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }


    public void showProgressDialog() {
        progressDialog = new ProgressDialog(ItemOneFragmentTabsView.this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    @Override
    public void onBackPressed() {
        ShowBackDialog();
        //super.onBackPressed();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
