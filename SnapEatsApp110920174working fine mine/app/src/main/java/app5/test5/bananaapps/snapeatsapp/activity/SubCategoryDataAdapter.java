package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.FoodTypeList;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.ItemValues;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;
import com.squareup.picasso.Picasso;


import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.BucketOrder;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Banana on 7/3/2017.
 */

public class SubCategoryDataAdapter extends RecyclerView.Adapter<SubCategoryDataAdapter.MyViewHolder> {

    DecimalFormat df ;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    public static List<SubCategoryList> slotsList;
    public static List<SubCategoryList> itemValues;
    public static List<SubCategoryList> finalCheckoutList=new ArrayList<>();
    private Context context;
    BucketDB bucketDB;
    BucketOrder bucketOrder;
    public static List<String> savedItemSNoList;
    private List<SubCategoryList> savedQtyList;
    public static final String PREFS_NAME = "SnapEatsVariables";
    int number;
    String result = null;
    SharedPreferences settings;
    String value;

    public SubCategoryDataAdapter(Context cnxt, List<SubCategoryList> bList,String value) {
        this.context = cnxt;
        this.slotsList = bList;
        this.value=value;
        savedQtyList=new ArrayList<>();
        savedItemSNoList=new ArrayList<>();
        cd = new ConnectionDetector(cnxt);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.food_layout, parent, false);
        itemValues=slotsList;
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if(value.equalsIgnoreCase("initial")) {

            final SubCategoryList slots = slotsList.get(position);
            df = new DecimalFormat("0.00");

            if (!TextUtils.isEmpty(slots.getName())) {
                holder.fItemName.setText(slots.getName());
            }

            if (!TextUtils.isEmpty(slots.getItemDesc())) {
                holder.fItemNameDesc.setText(slots.getItemDesc());
            }
            if (!TextUtils.isEmpty(slots.getPrice())) {
                holder.cost.setText(df.format(Double.parseDouble(slots.getPrice())));
            }

            if (!TextUtils.isEmpty(slots.getfImageurl())) {
//           Picasso.with(context)
//                   .load(slots.getfImageurl())
//                   .transform(new RoundedTransformation(100, 0))
//                   .resize(64, 64).into(holder.title);
                new DownloadImageTask(holder.title).execute(slots.getfImageurl());
            } else {
                holder.title.setImageResource(R.drawable.ic_edit_button);
            }

            holder.addingImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Double actualCost = Double.parseDouble(slotsList.get(position).getPrice());
                    final MyViewHolder finalHolder = holder;
                    if (finalHolder.itemQuant != null) {
                        CharSequence cs = finalHolder.itemQuant.getText();
                        number = Integer.parseInt(cs.toString());
                        number++;
                        finalHolder.itemQuant.setText(String.valueOf(number));
                        finalHolder.cost.setText(df.format(number * Double.parseDouble(slots.getPrice())));

                        slots.setQuantity(String.valueOf(finalHolder.itemQuant.getText()));
                        slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));

                        bucketDB = new BucketDB(context);
                        savedQtyList = bucketDB.getIndividualDetails(slots.getFoodType());
                        if (savedQtyList != null && savedQtyList.size() > 0) {
                            for (int i = 0; i < savedQtyList.size(); i++) {
                                savedItemSNoList.add(savedQtyList.get(i).getScid());
                            }
                            if (!savedItemSNoList.contains(slotsList.get(position).getScid())) {
                                bucketDB.addChampcash(new SubCategoryList(slots.getScid(),slots.getId(), slots.getFoodType(), slots.getName(), slots.getItemDesc(), slots.getPrice(), slots.getfImageurl(), slots.getSerialNo(), String.valueOf(number), String.valueOf(number * Double.parseDouble(slots.getPrice()))));
                            } else {
                                bucketDB.updateChampcash(slots.getScid(), String.valueOf(number), String.valueOf(number * Double.parseDouble(slots.getPrice())));
                            }
                        } else {
                            bucketDB.addChampcash(new SubCategoryList(slots.getScid(),slots.getId(), slots.getFoodType(), slots.getName(), slots.getItemDesc(), slots.getPrice(), slots.getfImageurl(), slots.getSerialNo(), String.valueOf(number), String.valueOf(number * Double.parseDouble(slots.getPrice()))));
                        }

//                    slots.setQuantity(number);
//                    slots.setPrice(String.valueOf(number*actualCost));
//                    itemValues.add(slots);
                    }
                }
            });

            holder.removeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Double actualCost = Double.parseDouble(slotsList.get(position).getPrice());
                    final MyViewHolder finalHolder = holder;
                    if (finalHolder.itemQuant != null) {
                        CharSequence cs = finalHolder.itemQuant.getText();
                        number = Integer.parseInt(cs.toString());
                        number--;
                        if (number > 0) {
                            finalHolder.itemQuant.setText(String.valueOf(number));
                            holder.cost.setText(df.format(number * Double.parseDouble(slots.getPrice())));
                            slots.setQuantity(String.valueOf(finalHolder.itemQuant.getText()));
                            slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));
                            bucketDB=new BucketDB(context);
                            savedQtyList=bucketDB.getIndividualDetails(slots.getFoodType());
                            if (savedQtyList != null && savedQtyList.size() > 0) {
                                bucketDB.updateChampcash(slots.getScid(), String.valueOf(number), String.valueOf(number * Double.parseDouble(slots.getPrice())));
                            }
                            // finalCheckoutList.add(bucketOrder);

                        } else if (number == 0) {
                            finalHolder.itemQuant.setText(String.valueOf(number));
                            holder.cost.setText(df.format(actualCost));

                            slots.setQuantity(String.valueOf(finalHolder.itemQuant.getText()));
                            slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));

                            bucketDB=new BucketDB(context);
                            bucketDB.removeChampcash(slotsList.get(position).getScid());
                        }
//
//                    if (number > 0) ;
//                    {
//                        if (number == 0) {
//
//                        }
//                        if (number == 1) {
//                            holder.cost.setText("$" + df.format(number*Double.parseDouble(slots.getPrice())));
//                        }
//                        if (number > 1) {
//                            finalHolder.cost.setText("$" + df.format(number*Double.parseDouble(slots.getPrice())));
//                        }
//                    }
                    }
                }
            });
            holder.moreTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ImageView descImage;
                    Button close;
                    TextView sorryText,price,Title;
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.alert_description);
                    close = (Button) dialog.findViewById(R.id.cross);

                    descImage= (ImageView)dialog.findViewById(R.id.descImage);
                    //      descImage.setImageBitmap(new DownloadImageTask(holder.title).execute(slots.getfImageurl()));

                    if (!TextUtils.isEmpty(slots.getfImageurl())) {

                        new DownloadImageTask(descImage).execute(slots.getfImageurl());
                    } else {
                        holder.title.setImageResource(R.drawable.ic_edit_button);
                    }

                    sorryText = (TextView)dialog.findViewById(R.id.storeClosed);
                    Title = (TextView)dialog.findViewById(R.id.sorryText);
                    Title.setText(holder.fItemName.getText());
                    price = (TextView)dialog.findViewById(R.id.price);
                    String s = slots.getPrice();
                    price.setText("£"+s);
                    sorryText.setText(holder.fItemNameDesc.getText());
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });

//        try {
//            StringBuilder sb = new StringBuilder();
//            SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
//            SharedPreferences.Editor editor2 = settings.edit();
//            sb.append(slots.getName()).append("\n");
//            sb.append(slots.getItemDesc()).append("\n");
//            sb.append(slots.getPrice()).append("\n");
//            sb.append(number).append("\n");
//            editor2.putString("itemName", "" + slots.getName());
//            editor2.putString("itemDesc", "" + slots.getItemDesc());
//            editor2.putString("itemPrice", "" + slots.getPrice());
//            editor2.putString("quantity", "" + number);
//            editor2.commit();
//            result = sb.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            Message message = Message.obtain();
//            if (result != null) {
//                message.what = 1;
//                Bundle bundle = new Bundle();
//                result = "itemName: " + slots.getName() + " itemDesc: " + slots.getItemDesc() + "itemPrice: " + slots.getPrice() + " quantity " + number;
//                bundle.putString("address", result);
//                message.setData(bundle);
//            } else {
//                message.what = 1;
//                Bundle bundle = new Bundle();
//                result = "itemName: " + slots.getName() + " itemDesc: " + slots.getItemDesc() + "itemPrice: " + slots.getPrice() + " quantity " + number;
//                bundle.putString("address", result);
//                message.setData(bundle);
//            }
//        }

        }
        else if(value.equalsIgnoreCase("updated")){

            final SubCategoryList slots = slotsList.get(position);
            df = new DecimalFormat("0.00");

            if(!TextUtils.isEmpty(slots.getName()))
            {
                holder.fItemName.setText(slots.getName());
            }

      if(!TextUtils.isEmpty(slots.getItemDesc()))
        {
            holder.fItemNameDesc.setText(slots.getItemDesc());
        }
            if(!TextUtils.isEmpty(slots.getTotPrice()))
            {
                holder.cost.setText(df.format(Double.parseDouble(slots.getTotPrice())));
            }
            else{
                holder.cost.setText(df.format(Double.parseDouble(slots.getPrice())));
            }

            if(!TextUtils.isEmpty(slots.getfImageurl()))
            {
//           Picasso.with(context)
//                   .load(slots.getfImageurl())
//                   .transform(new RoundedTransformation(100, 0))
//                   .resize(64, 64).into(holder.title);
                new DownloadImageTask(holder.title).execute(slots.getfImageurl());
            }
            else {
                holder.title.setImageResource(R.drawable.ic_edit_button);
            }
            if(!TextUtils.isEmpty(slots.getQuantity())){
                holder.itemQuant.setText((String.valueOf(slots.getQuantity())));
            }
            else{
                holder.itemQuant.setText((String.valueOf(("0"))));
            }
            holder.addingImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Double actualCost=Double.parseDouble(slotsList.get(position).getPrice());
                    final MyViewHolder finalHolder = holder;
                    if (finalHolder.itemQuant != null) {
                        CharSequence cs = finalHolder.itemQuant.getText();
                        number = Integer.parseInt(cs.toString());
                        number++;


                        finalHolder.itemQuant.setText(String.valueOf(number));
                        finalHolder.cost.setText(df.format(number*Double.parseDouble(String.valueOf(slots.getPrice()))));

                        slots.setQuantity(String.valueOf( finalHolder.itemQuant.getText()));
                        slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));

                        bucketDB=new BucketDB(context);
                        savedQtyList=bucketDB.getIndividualDetails(slots.getFoodType());
                        if (savedQtyList != null && savedQtyList.size() > 0) {
                            for (int i = 0; i < savedQtyList.size(); i++) {
                                savedItemSNoList.add(savedQtyList.get(i).getScid());
                            }
                            if(!savedItemSNoList.contains(slotsList.get(position).getScid())){
                                bucketDB.addChampcash(new SubCategoryList(slots.getScid(),slots.getId(),slots.getFoodType(),slots.getName(),slots.getItemDesc(),slots.getPrice(),slots.getfImageurl(),slots.getSerialNo(),String.valueOf(number),String.valueOf(number*Double.parseDouble(slots.getPrice()))));
                            }
                            else {
                                bucketDB.updateChampcash(slots.getScid(),String.valueOf(number),String.valueOf(number*Double.parseDouble(slots.getPrice())));
                            }
                        }
                        else {
                            bucketDB.addChampcash(new SubCategoryList(slots.getScid(),slots.getId(),slots.getFoodType(),slots.getName(),slots.getItemDesc(),slots.getPrice(),slots.getfImageurl(),slots.getSerialNo(),String.valueOf(number),String.valueOf(number*Double.parseDouble(slots.getPrice()))));
                        }


//                    slots.setQuantity(number);
//                    slots.setPrice(String.valueOf(number*actualCost));

//                    itemValues.add(slots);
                    }
                }
            });

            holder.removeImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Double actualCost=Double.parseDouble(slotsList.get(position).getPrice());
                    final MyViewHolder finalHolder = holder;
                    if (finalHolder.itemQuant != null) {
                        CharSequence cs = finalHolder.itemQuant.getText();
                        number = Integer.parseInt(cs.toString());
                        number--;
                        if (number > 0) {
                            finalHolder.itemQuant.setText( String.valueOf(number));
                            holder.cost.setText(df.format(number*Double.parseDouble(slots.getPrice())));

                            slots.setQuantity(String.valueOf(finalHolder.itemQuant.getText()));
                            slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));

                            bucketDB=new BucketDB(context);
                            savedQtyList=bucketDB.getIndividualDetails(slots.getFoodType());
                            if (savedQtyList != null && savedQtyList.size() > 0) {
                                bucketDB.updateChampcash(slots.getScid(), String.valueOf(number), String.valueOf(number * Double.parseDouble(slots.getPrice())));
                            }

                        }
                        else if(number == 0){
                            finalHolder.itemQuant.setText(String.valueOf(number) );
                            holder.cost.setText(df.format(actualCost));

                            slots.setQuantity(String.valueOf(finalHolder.itemQuant.getText()));
                            slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));

                            bucketDB=new BucketDB(context);
                            bucketDB.removeChampcash(slotsList.get(position).getScid());
                        }
//
//                    if (number > 0) ;
//                    {
//                        if (number == 0) {
//
//                        }
//                        if (number == 1) {
//                            holder.cost.setText("$" + df.format(number*Double.parseDouble(slots.getPrice())));
//                        }
//                        if (number > 1) {
//                            finalHolder.cost.setText("$" + df.format(number*Double.parseDouble(slots.getPrice())));
//                        }
//                    }
                    }
                }
            });

            holder.moreTv.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    ImageView descImage;
                    TextView sorryText,price,Title;
                    Button close;
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setContentView(R.layout.alert_description);
                    close = (Button) dialog.findViewById(R.id.cross);

                    descImage= (ImageView)dialog.findViewById(R.id.descImage);
                    //      descImage.setImageBitmap(new DownloadImageTask(holder.title).execute(slots.getfImageurl()));

                    if (!TextUtils.isEmpty(slots.getfImageurl())) {

                        new DownloadImageTask(descImage).execute(slots.getfImageurl());
                    } else {
                        holder.title.setImageResource(R.drawable.ic_edit_button);
                    }

                    sorryText = (TextView)dialog.findViewById(R.id.storeClosed);
                    Title = (TextView)dialog.findViewById(R.id.sorryText);
                    Title.setText(holder.fItemName.getText());
                    price = (TextView)dialog.findViewById(R.id.price);
                    String s = slots.getPrice();
                    price.setText("£"+s);
                    sorryText.setText(holder.fItemNameDesc.getText());
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            });
        }


    }


    @Override
    public int getItemCount() {
        return slotsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView title;
        TextView fItemName;
        TextView fItemNameDesc;
        TextView cost,moreTv;
        TextView itemQuant;
        ImageView addingImage;
        ImageView removeImage;


        public MyViewHolder(View view) {
            super(view);
            title = (ImageView) view.findViewById(R.id.logoImage);
            fItemName = (TextView) view.findViewById(R.id.itemType);
            fItemNameDesc = (TextView) view.findViewById(R.id.itemDesc);
            cost = (TextView) view.findViewById(R.id.money);
            itemQuant = (TextView) view.findViewById(R.id.itemQunt);
            addingImage = (ImageView) view.findViewById(R.id.addButton);
            removeImage = (ImageView) view.findViewById(R.id.removeButton);
            moreTv = (TextView)view.findViewById(R.id.moreTv);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                if (!urldisplay.contains("data:image/jpeg;base64")) {
                    InputStream in = new java.net.URL(urldisplay).openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                } else {
                    String actualBitmap = urldisplay.substring(0, urldisplay.indexOf(",") + 1);
                    urldisplay = urldisplay.replace(actualBitmap, "");
                    mIcon11 = bitmapConvert(urldisplay);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private Bitmap bitmapConvert(String Image) {
        byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
