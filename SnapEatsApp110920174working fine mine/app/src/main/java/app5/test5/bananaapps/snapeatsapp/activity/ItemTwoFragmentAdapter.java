package app5.test5.bananaapps.snapeatsapp.activity;

/**
 * Created by BananaApps on 6/1/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ItemTwoFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ShopList> shopLists;
    private Context context;
    private LayoutInflater inflater;
    private OnShopClickListener mOnShopClickListener;

    public ItemTwoFragmentAdapter(Context context, ArrayList<ShopList> shopLists) {
        this.context = context;
        this.shopLists = shopLists;
        inflater = ((Activity) context).getLayoutInflater();
        mOnShopClickListener = new OnShopClickListener(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.fragement_item_two_adapter, viewGroup, false);
        return new ShopListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ShopList shopList = shopLists.get(position);
        ShopListViewHolder shopListViewHolder = (ShopListViewHolder) holder;
        //  Toast.makeText(context,"view holder",Toast.LENGTH_LONG).show();
        View view = shopListViewHolder.getItemView();
        //view.setTag(R.id.movie, shop);
        // view.setTag(R.id.list, shop);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   Toast.makeText(view.getContext(),"shopList.getSid() "+shopList.getSid(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, ItemOneFragmentTabsView.class);
                intent.putExtra("LName", shopList.getShop_name());
                intent.putExtra("image", shopList.getShop_image());
                intent.putExtra("id", shopList.getSid());
                Cache.putData(CatchValue.SHOP_ID, context, shopList.getSid(), Cache.CACHE_LOCATION_DISK);
                intent.putExtra("Type", shopList.getMname());
                intent.putExtra("address",shopList.getShopAddress());
                // Cache.putData(CatchValue.ARRAY_LIST, context, shopList.getShopAddress(), Cache.CACHE_LOCATION_DISK);
                context.startActivity(intent);
            }
        });

        ImageView imgItem = shopListViewHolder.getImgItem();
        TextView txtItemName = shopListViewHolder.getTxtItemName();
        TextView txtRating = shopListViewHolder.getTxtRating();

        if (imgItem != null) {
            if (!TextUtils.isEmpty(shopList.getShop_image())) {
                String s = shopList.getShop_image();
                new DownLoadImageTask(imgItem).execute(s);
                //    ("https://i1.wp.com/www.dlptoday.com/images/2016/09/20160928_fiveguys_2.jpg");
//                UIUtils.displayAsyncImage(imgItem, movie.getMainimage(),
//                        true, R.drawable.preview_load,
//                        0, 0, true);
            }
            else{
                imgItem.setImageResource(R.drawable.noimage);
            }
        }


        if (txtItemName != null) {
            Log.e("Test ","6");
            if (!TextUtils.isEmpty(shopList.getShop_name())) {
                txtItemName.setVisibility(View.VISIBLE);
                txtItemName.setText(shopList.getShop_name());
            } else {
                txtItemName.setVisibility(View.GONE);
            }
        }
        if (txtRating != null) {
            Log.e("Test ","7");
            if (!TextUtils.isEmpty(String.valueOf(shopList.getDistance()))) {
                txtRating.setVisibility(View.VISIBLE);
                //txtRating.setText(shopList.getDistance()+" miles");
                txtRating.setText(shopList.getDistance()+"");
            } else {
                txtRating.setVisibility(View.GONE);
            }
        }
    }

    private class DownLoadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;
        public DownLoadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }
        /*
            doInBackground(Params... params)
                Override this method to perform a computation on a background thread.
         */
        protected Bitmap doInBackground(String... urls) {
            String urlOfImage = urls[0];
            Bitmap logo = null;
            Log.e("Test ","9");
            try {
                if (!urlOfImage.contains("data:image/jpeg;base64")) {
                    InputStream in = new java.net.URL(urlOfImage).openStream();
                    logo = BitmapFactory.decodeStream(in);

//                    ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(myUri, "r");
//                    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
//                    logo = BitmapFactory.decodeFileDescriptor(fileDescriptor);
//                    parcelFileDescriptor.close();


                } else {
                    String actualBitmap = urlOfImage.substring(0, urlOfImage.indexOf(",") + 1);
                    urlOfImage = urlOfImage.replace(actualBitmap, "");
                    logo = bitmapConvert(urlOfImage);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return logo;
        }

        protected void onPostExecute(Bitmap result) {
            if(result!=null){
                imageView.setImageBitmap(result);
            }
            else{
                imageView.setImageResource(R.drawable.noimage);
            }
        }
    }

    private static class OnShopClickListener implements View.OnClickListener {
        private Context context;

        public OnShopClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {

            Toast.makeText(context, "Item clicked ", Toast.LENGTH_LONG).show();
            /*if (context == null) return;
            Shop shop = (Shop) v.getTag(R.id.movie);
            Intent intent = new Intent(context, MovieDetailsActivity.class);
            intent.putExtra("ItemName", shop.getName());
            context.startActivity(intent);*/
        }
    }

    private static class ShopListViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgItem;
        private TextView txtItemName;
        private TextView txtRating;


        public ShopListViewHolder(View itemView) {
            super(itemView);
        }

        public View getItemView() {
            return this.itemView;
        }

        public TextView getTxtRating() {
            if (txtRating == null) {
                txtRating = (TextView) itemView.findViewById(R.id.restaurant_distance);
            }
            return txtRating;
        }

        public TextView getTxtItemName() {
            if (txtItemName == null) {
                txtItemName = (TextView) itemView.findViewById(R.id.restaurant_name);
            }
            return txtItemName;
        }


        public ImageView getImgItem() {
            if (imgItem == null) {
                imgItem = (ImageView) itemView.findViewById(R.id.n_logo);
            }
            return imgItem;
        }

    }

    @Override
    public int getItemCount() {
        return shopLists.size();
    }

    private Bitmap bitmapConvert(String Image) {
        byte[] decodedString = Base64.decode(Image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

}