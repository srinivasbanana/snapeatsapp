/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.DrawerMenuAdapter;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SideMenu;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class ItemOneFragment extends Fragment{
    TextView profileName,profileMail;

    Toolbar toolbar;
    ImageView backArrow, profile, mapIcon;
    TextView toolbartitle;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    ArrayList<ShopList> all, resturants, groceries, fashion;
    ArrayList<menuDetails> menuDetailses;
    String locationName;
    String dLatitude, dLongitude;
    String result = null;
    ProgressDialog progressDialog;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    FragmentManager fragmentManager;
    private String locality;
    private String addressChange;
    private String isAddressChange;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private DrawerMenuAdapter mAdapter;
    private LinearLayout linearLayout;
    ArrayList<SideMenu> menuItems;
    LinearLayout sideLayout;
    String logStatus;

    public static ItemOneFragment newInstance() {
        ItemOneFragment fragment = new ItemOneFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v;
        v = inflater.inflate(R.layout.fragment_item_one, container, false);
        settings = getActivity().getSharedPreferences(PREFS_NAME, getActivity().MODE_PRIVATE);
        logStatus = settings.getString("logStatus", "");
        toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        backArrow = (ImageView) v.findViewById(R.id.backArrow);
        profile = (ImageView) v.findViewById(R.id.profile);
        mDrawerLayout = (DrawerLayout)v.findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)v.findViewById(R.id.side_menu_list);
        mDrawerLayout = (DrawerLayout)v.findViewById(R.id.drawer_layout);
        sideLayout = (LinearLayout)v.findViewById(R.id.side_menu_layout);
        sideLayout.requestDisallowInterceptTouchEvent(false);
        toolbartitle = (TextView) v.findViewById(R.id.toolbar_title);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        mapIcon = (ImageView) v.findViewById(R.id.mapIcon);
        // setupViewPager(viewPager);
        fragmentManager = getFragmentManager();
        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectionAvailable();
        if (isInternetPresent) {
            moveShops();
        } else {
            ShowNoInternetDialog();
        }

        Typeface custom_font2 = Typeface.createFromAsset(getActivity().getAssets(), "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);
        ImageView mapImag = (ImageView) v.findViewById(R.id.mapIcon);

        mapImag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PlacesAutoCompleteActivity.class));
            }
        });
        menuItems = new ArrayList<SideMenu>();

        if(logStatus.equalsIgnoreCase("Skip"))
        {
    /*        menuItems.add(new SideMenu(getResources().getString(R.string.profile), R.drawable.user));
            menuItems.add(new SideMenu(getResources().getString(R.string.OrderHistory), R.drawable.order_history));
            menuItems.add(new SideMenu(getResources().getString(R.string.cardDetails), R.drawable.card_details));*/
            menuItems.add(new SideMenu(getResources().getString(R.string.faq), R.drawable.faqs));
            menuItems.add(new SideMenu(getResources().getString(R.string.contactSupport), R.drawable.contact_support));
            menuItems.add(new SideMenu(getResources().getString(R.string.temsAndConditions), R.drawable.terms_conditions));
            menuItems.add(new SideMenu(getResources().getString(R.string.login), R.drawable.user));
        }
        else
        {
            menuItems.add(new SideMenu(getResources().getString(R.string.profile), R.drawable.user));
            menuItems.add(new SideMenu(getResources().getString(R.string.OrderHistory), R.drawable.order_history));
            menuItems.add(new SideMenu(getResources().getString(R.string.cardDetails), R.drawable.card_details));
            menuItems.add(new SideMenu(getResources().getString(R.string.faq), R.drawable.faqs));
            menuItems.add(new SideMenu(getResources().getString(R.string.contactSupport), R.drawable.contact_support));
            menuItems.add(new SideMenu(getResources().getString(R.string.temsAndConditions), R.drawable.terms_conditions));
            menuItems.add(new SideMenu(getResources().getString(R.string.logout), R.drawable.logout));
        }



        mAdapter = new DrawerMenuAdapter(getActivity(), menuItems);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getActivity(), Profile.class));
                mDrawerLayout.openDrawer(sideLayout);
                // finish();
            }
        });


        return v;

    }
/*
      private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
       // moveShops();
        adapter.addFrag(new FragmentAll(), "All");
        adapter.addFrag(new FragmentRestaurants(), "Restaurants");
        adapter.addFrag(new FragmentGroceries(), "Groceries");
       // adapter.addFrag(new FragmentFashion(), "FragmentFashion");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
*/
//  CityMoviesApiService apiService = CityMoviesApiAdapter.getClient(this).create(CityMoviesApiService.class);

    public void moveShops() {
        showProgressDialog();
        SnapEatsApiService apiService = SnapEatsApiAdapter.getClient(getActivity()).create(SnapEatsApiService.class);
        all = new ArrayList<>();
        resturants = new ArrayList<>();
        menuDetailses  = new ArrayList<>();
        groceries = new ArrayList<>();
        fashion = new ArrayList<>();
        Call<SnapEatsApiResponse> call = apiService.getShops();
        // Toast.makeText(getActivity(), "success ", Toast.LENGTH_LONG).show();
        call.enqueue(new Callback<SnapEatsApiResponse>() {

            @Override
            public void onResponse(Call<SnapEatsApiResponse> call, Response<SnapEatsApiResponse> response) {
                //       Toast.makeText(getActivity(), "success 2 ", Toast.LENGTH_LONG).show();

                dismissProgressDialog();
                if (response != null && response.isSuccessful()) {
                    //         Toast.makeText(getActivity(), "su 2", Toast.LENGTH_LONG).show();
                    menuDetailses = response.body().menuDetails;
                    int msize,bsize;
                    msize = menuDetailses.size();
                    for(int i=0;i<msize;i++) {
                        //  String name = menuDetailses.get(i).getMenuName();
                        if (menuDetailses.get(i).getMenuName().equalsIgnoreCase("Restaurants")) {
                            bsize = menuDetailses.get(i).getShopList().size();
                            for(int j=0;j<bsize;j++) {
                                resturants.add(menuDetailses.get(i).getShopList().get(j));
                            }
                        }

                        if (menuDetailses.get(i).getMenuName().equalsIgnoreCase("Groceries")) {
                            bsize = menuDetailses.get(i).getShopList().size();
                            for(int j=0;j<bsize;j++) {
                                groceries.add(menuDetailses.get(i).getShopList().get(j));

                            }
                        }

                        if (menuDetailses.get(i).getMenuName().equalsIgnoreCase("Fashion")) {
                            bsize = menuDetailses.get(i).getShopList().size();
                            for(int j=0;j<bsize;j++) {
                                fashion.add(menuDetailses.get(i).getShopList().get(j));
                            }
                        }
                    }

                    //  resturants.add(menuDetailses.get(0).getShopList().get(0));
                    //  showAlertDialog(getActivity(), "menuDetailses.get(0) ", " "+menuDetailses.get(1).getMenuName(), false);
                    for (int i = 0; i < resturants.size(); i++) {
                        all.add(resturants.get(i));
                    }
                    for (int i = 0; i < groceries.size(); i++) {
                        all.add(groceries.get(i));
                    }
                    for (int i = 0; i < fashion.size(); i++) {
                        all.add(fashion.get(i));
                    }
                    //   Toast.makeText(getActivity(),"all name "+all.size(),Toast.LENGTH_LONG).show();
                    renderTabs(all, resturants, groceries, fashion);
                }
            }

            @Override
            public void onFailure(Call<SnapEatsApiResponse> call, Throwable t) {
                dismissProgressDialog();
                Toast.makeText(getActivity(), "Failed ", Toast.LENGTH_LONG).show();
                //  finish();
            }
        });
    }

    private void renderTabs(ArrayList<ShopList> all, ArrayList<ShopList> resturants, ArrayList<ShopList> groceries, ArrayList<ShopList> fashion) {

        try {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

            TabShopPagerAdapter mTabMoviesPagerAdapter = new TabShopPagerAdapter(fragmentManager, getActivity(), all, resturants, groceries, fashion, 0);
            if (viewPager != null) {
                viewPager.setVisibility(View.VISIBLE);
                viewPager.setAdapter(mTabMoviesPagerAdapter);

                viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });
            }
            if (tabLayout != null) {
                tabLayout.setVisibility(View.VISIBLE);
                tabLayout.setupWithViewPager(viewPager);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        //  hideProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        locality = settings.getString("locality", "");
        addressChange = settings.getString("addressChange", "");
        isAddressChange = settings.getString("isAddressChange", "");
        locationName = settings.getString("placeData", "");

        //CurrentLongitude = settings.getString("CurrentData", "");
        //tLatLan=CurrentLongitude.substring(CurrentLongitude.indexOf("(")+1, CurrentLongitude.lastIndexOf(")"));
        // dLatitude=tLatLan.substring(0,tLatLan.indexOf(","));
        //dLongitude=tLatLan.substring(tLatLan.indexOf(",")+1,tLatLan.lastIndexOf(""));

        dLatitude = settings.getString("CLatitude", "");
        dLongitude = settings.getString("CLongitude", "");
        if (!TextUtils.isEmpty(dLatitude) && !TextUtils.isEmpty(dLongitude)) {
            getLoactionValues(dLatitude, dLongitude);
        } else {
            dLatitude = settings.getString("CurrentLatitude", "");
            dLongitude = settings.getString("CurrentLongitude", "");
            getLoactionValues(dLatitude, dLongitude);
        }
        if (isAddressChange.equals("true"))
            toolbartitle.setText(addressChange);
        else {
            if (TextUtils.isEmpty(locationName)) {
                toolbartitle.setText(locality);
            } else {
                toolbartitle.setText(locationName);
            }
        }


    }


    private void getLoactionValues(String dLatitude, String dLongitude) {
        try {
            StringBuilder sb = new StringBuilder();
            SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor2 = settings.edit();
            sb.append(dLatitude).append("\n");
            sb.append(dLongitude).append("\n");
            editor2.putString("CurrentLatitude", "" + dLatitude);
            editor2.putString("CurrentLongitude", "" + dLongitude);
            editor2.commit();
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Message message = Message.obtain();
            if (result != null) {
                message.what = 1;
                Bundle bundle = new Bundle();
                result = "Latitude: " + dLatitude + " Longitude: " + dLongitude;
                bundle.putString("address", result);
                message.setData(bundle);
            } else {
                message.what = 1;
                Bundle bundle = new Bundle();
                result = "Latitude: " + dLatitude + " Longitude: " + dLongitude +
                        "\n Unable to get address for this lat-long.";
                bundle.putString("address", result);
                message.setData(bundle);
            }
        }
    }

    public void ShowNoInternetDialog() {
        showAlertDialog(getActivity(), "No Internet Connection", "Please check your network.", false);
    }


    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    moveShops();
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }

    public void showProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }



    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {


        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

            displayView(position);
        }
    }


    private void displayView(int position) {
        switch (position) {

            case 0:
                if(logStatus.equalsIgnoreCase("Skip"))
                    startActivity(new Intent(getActivity(), Faqs.class));
                else
                    startActivity(new Intent(getActivity(), Profile.class));
                getActivity().finish();
                break;

            case 1:
                if(logStatus.equalsIgnoreCase("Skip"))
                    startActivity(new Intent(getActivity(), ContactSupport.class));
                else
                    startActivity(new Intent(getActivity(), History.class));

                getActivity().finish();
                break;

            case 2:
                if(logStatus.equalsIgnoreCase("Skip"))
                    startActivity(new Intent(getActivity(), TermsConditions.class));
                else
                    startActivity(new Intent(getActivity(), CardDetails.class));

                getActivity().finish();
                break;

            case 3:
                if(logStatus.equalsIgnoreCase("Skip"))
                    startActivity(new Intent(getActivity(), Login.class));
                else
                    startActivity(new Intent(getActivity(), Faqs.class));
                getActivity().finish();
                break;
            case 4:
                startActivity(new Intent(getActivity(), ContactSupport.class));
                getActivity().finish();
                break;

            case 5:
                startActivity(new Intent(getActivity(), TermsConditions.class));
                getActivity().finish();
                break;
            case 6:

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog_Alert);
                builder.setTitle("Logout");
                builder.setMessage("Do you want to exit?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Cache.putData(CatchValue.USER_ID, getActivity(), "", Cache.CACHE_LOCATION_DISK);
                        startActivity(new Intent(getActivity(), Login.class));
                        getActivity().finish();
//                                if (authMode.equalsIgnoreCase("G")) {
//                                    if (mGoogleApiClient.isConnected()) {
//                                        Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
//                                        mGoogleApiClient.disconnect();
//                                        mGoogleApiClient.connect();
//                                    }
//                                } else if (authMode.equalsIgnoreCase("F")) {
//                                    LoginManager.getInstance().logOut();
//                                }else{
//                        new SignOutTask().execute();
//                    }

                    }
                });

                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                builder.setIcon(R.mipmap.ic_action_info);
                builder.show();
                break;

            default:
                break;
        }

        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        // Close Side menu
        mDrawerLayout.closeDrawer(sideLayout);

    }

}

