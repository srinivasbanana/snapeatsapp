package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

import java.io.Serializable;

/**
 * Created by Dell1 on 28-Jul-17.
 */

public class BucketOrder implements Serializable{

    String id;
    String foodType;
    String name;
    String itemDesc;
    String price;
    String fImageurl;
    int totQty;
    int quantity;
    String totPrice;

    public BucketOrder(String id, String foodType, String name, String itemDesc, String price, String fImageurl, int totQty) {
        this.id = id;
        this.foodType = foodType;
        this.name = name;
        this.itemDesc = itemDesc;
        this.price = price;
        this.fImageurl = fImageurl;
        this.totQty=totQty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getfImageurl() {
        return fImageurl;
    }

    public void setfImageurl(String fImageurl) {
        this.fImageurl = fImageurl;
    }

    public int getTotQty() {
        return totQty;
    }

    public void setTotQty(int totQty) {
        this.totQty = totQty;
    }
}
