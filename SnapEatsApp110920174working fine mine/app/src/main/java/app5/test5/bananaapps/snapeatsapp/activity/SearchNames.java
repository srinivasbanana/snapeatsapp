package app5.test5.bananaapps.snapeatsapp.activity;

public class SearchNames {
    private String searchName;
    private String d;
    private int i;

    public SearchNames(String searchName,String d,int i) {
        this.searchName = searchName;
        this.d = d;
        this.i = i;
    }

    public String getSearchName() {
        return this.searchName;
    }

    public String getDistance()
    {
        return this.d;
    }
    public int getImageID()
    {
        return i;
    }

}