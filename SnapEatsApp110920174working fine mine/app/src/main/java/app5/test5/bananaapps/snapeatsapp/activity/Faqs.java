package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;

/**
 * Created by BananaApps on 6/7/2017.
 */

public class Faqs extends BaseActivity {
    TextView toolbartitle;
    ImageView backArrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faqs);
        toolbartitle = (TextView)findViewById(R.id.toolbar_title);
        backArrow = (ImageView)findViewById(R.id.backArrow);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbartitle.setTypeface(custom_font2);

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Faqs.this, HomePage.class));
                finish();
            }
        });


        ListView list;
        String[] web = {
                "What is SnapEats ?",
                "How does SnapEats work ?",
                "What times can I order from SnapEats",
                "Is there a minimum order value ?",
                "What if something is wrong with my order ?"



        } ;

        String[] dist = {
                "TempData is a bucket where you can dump data that is only needed for the following request. That is, anything you put into TempData is discarded after the next request completes. ",
                "We often hear about the dark web being linked to terrorist plots, drug deals, knife sales and child pornography, but beyond this it can be hard to fully understand how the dark web works and what it looks like.",
                "TempData is a bucket where you can dump data that is only needed for the following request. That is, anything you put into TempData is discarded after the next request completes. ",
                "Links are private: no search or index. They can be password protected and strictly respect Apple's security rules with UDIDs and provisioning profiles",
                "We have a friendly and knowledgeable team on hand to help with any order queries, so please drop us an email or give us a call on 01202 668545 and we'll do everything we can to sort things out. If you need to return an item we'll send you a freepost label so you can do this easily, and if you have any other"

        } ;



        FaqList adapter = new
                FaqList(Faqs.this, web, dist);
        list=(ListView)findViewById(R.id.listFaq);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // Toast.makeText(getActivity(), "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(Faqs.this, HomePage.class));
        finish();
        super.onBackPressed();
    }

}
