package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static android.content.Context.MODE_PRIVATE;

public class FragmentRestaurants extends Fragment {
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    Context context;
    String dLatitude,dLongitude;
    String result = null;
    String CurrentLatitude;
    String CurrentLongitude;
    String locationName;
    ArrayList<ShopList> list;
    ArrayList<ShopList> list1;
    ArrayList<ShopAddress> list2;
    ArrayList<ShopList> distance;
    ArrayList<Double> updatedList;
    ArrayList<ShopList> updatedDistance;
    ArrayList<String> address;
    ArrayList<ArrayList<String>> updatedAddress;
    ShopList shopList;
    ShopList newList;
    String service_lat;
    String service_lan;
    RecyclerView recyclerView;
    TextView txtNoItem;
    double d;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        return inflater.inflate(R.layout.fragment_restaurants, container, false);
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getView();
        if (view == null || getArguments() == null) return;
        // boolean isFromMovies = getArguments().getBoolean(Constants.ISFROMMOVIES);
        txtNoItem = (TextView) view.findViewById(R.id.txtNoItems);
        //  if (isFromMovies) {
        Bundle bundle = new Bundle();
        ArrayList sc = bundle.getParcelableArrayList("LISTData");
        list = getArguments().getParcelableArrayList("LISTData");
        Shop newList;
        list1=new ArrayList<>();
        if (list == null || list.isEmpty()) {
            txtNoItem.setText("No Items found");
            txtNoItem.setVisibility(View.VISIBLE);
            return;
        }
        settings = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        dLatitude=settings.getString("CLatitude", "");
        dLongitude=settings.getString("CLongitude", "");
        if(!TextUtils.isEmpty(dLatitude)&&!TextUtils.isEmpty(dLongitude)){
            getLoactionValues(dLatitude,dLongitude);
        }
        else{
            dLatitude=settings.getString("CurrentLatitude", "");
            dLongitude=settings.getString("CurrentLongitude", "");
            getLoactionValues(dLatitude,dLongitude);
        }

        recyclerView = (RecyclerView) view.findViewById(R.id.listRes);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        FragmentRestaurantsAdapter moviesDirectoryAdapter = new FragmentRestaurantsAdapter(getContext(), list1);
        recyclerView.setAdapter(moviesDirectoryAdapter);

        // }
         /* else {
            ArrayList<Theater> list = getArguments().getParcelableArrayList(Constants.LIST);
            if (list == null || list.isEmpty()) {
                txtNoMovies.setText("No Theaters");
                txtNoMovies.setVisibility(View.VISIBLE);
                return;
            }
            txtNoMovies.setVisibility(View.GONE);
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.listShops);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(linearLayoutManager);
            TheaterDirectoryAdapter theaterDirectoryAdapter = new TheaterDirectoryAdapter(getContext(), list);
            recyclerView.setAdapter(theaterDirectoryAdapter);
        }*/
    }
    public class CustomComparator implements Comparator<ShopList> {
        @Override
        public int compare(ShopList shop, ShopList t1) {
            return shop.getDistance().compareTo(t1.getDistance());
        }
    }

    public double CalculationByDistance(String StartP, String StartEp, String Endp, String EndEp) {
        double lat1 = Double.parseDouble(StartP);
        double lat2 = Double.parseDouble(StartEp);
        double lon1 = Double.parseDouble(Endp);
        double lon2 = Double.parseDouble(EndEp);

       /* double Rad = 6372.8; //meters
        double dLat = Math.toRadians(lon1 - lat1);
        double dLon = Math.toRadians(lon2 - lat2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double haverdistanceKM = (Rad * c)*1.852;
        return  haverdistanceKM;*/
        Location startLocation = new Location("start_location");
        startLocation.setLatitude(lat1);
        startLocation.setLongitude(lat2);

        Location destLocation = new Location("end_location");
        destLocation.setLatitude(lon1);
        destLocation.setLongitude(lon2);

//        double distance = startLocation.distanceTo(destLocation);
        double distance = startLocation.distanceTo(destLocation) / 1000;

        DecimalFormat format = new DecimalFormat("0.#");

//        Toast.makeText(MainActivity.this, "Distance: " + String.valueOf(distance), Toast.LENGTH_SHORT).show();

//        Toast.makeText(MainActivity.this, "Distance: " + (int)distance + "KM", Toast.LENGTH_SHORT).show();
        // Toast.makeText(MainActivity.this, "Distance: " + format.format(distance) + "KM", Toast.LENGTH_SHORT).show();
        return  Double.parseDouble(format.format(distance*0.621371));

    }
    private void getLoactionValues(String dLatitude,String dLongitude){
        try {
            StringBuilder sb = new StringBuilder();
            SharedPreferences settings = getActivity().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            SharedPreferences.Editor editor2 = settings.edit();
            sb.append(dLatitude).append("\n");
            sb.append(dLongitude).append("\n");
            editor2.putString("CurrentLatitude", "" + dLatitude);
            editor2.putString("CurrentLongitude", "" + dLongitude);
            editor2.commit();
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Message message = Message.obtain();
            if (result != null) {
                message.what = 1;
                Bundle bundle = new Bundle();
                result = "Latitude: " + dLatitude + " Longitude: " + dLongitude;
                bundle.putString("address", result);
                message.setData(bundle);
            } else {
                message.what = 1;
                Bundle bundle = new Bundle();
                result = "Latitude: " + dLatitude + " Longitude: " + dLongitude +
                        "\n Unable to get address for this lat-long.";
                bundle.putString("address", result);
                message.setData(bundle);
            }
        }
    }

    public void onResume() {
        super.onResume();
        locationName = settings.getString("placeData", "");
        //CurrentLongitude = settings.getString("CurrentData", "");
        //tLatLan=CurrentLongitude.substring(CurrentLongitude.indexOf("(")+1, CurrentLongitude.lastIndexOf(")"));
        // dLatitude=tLatLan.substring(0,tLatLan.indexOf(","));
        //dLongitude=tLatLan.substring(tLatLan.indexOf(",")+1,tLatLan.lastIndexOf(""));
        CurrentLatitude = settings.getString("CurrentLatitude", "");
        CurrentLongitude = settings.getString("CurrentLongitude", "");
        list = getArguments().getParcelableArrayList("LISTData");
        distance=new ArrayList<>();
        list1=new ArrayList<>();
        list2=new ArrayList<>();
        double d = 0.0;
        updatedDistance= new ArrayList<>();
        address=new ArrayList<>();
        updatedAddress=new ArrayList<>();
        updatedList= new ArrayList<>();
        for(int i=0;i<list.size();i++){

            //  String categoryList = list.get(i).getCategoryList();
          /*  boolean chainbusiness=list.get(i).getChainbusiness();
            String name=list.get(i).getShop_name();
            int mid=list.get(i).getMid();
            String mname=list.get(i).getMname();
            String shop_image = list.get(i).getShop_image();
            String shop_name = list.get(i).getShop_name();
            int sid=list.get(i).getSid();
            int sstatus=list.get(i).getSstatus();
            String website = list.get(i).getWebsite();*/
            list1.add(list.get(i));
            if(list.get(i).getShopAddress().size()>0){
                for(int j=0;j<list.get(i).getShopAddress().size();j++) {
                    list2.add(list1.get(i).getShopAddress().get(j));
                    if (!TextUtils.isEmpty(CurrentLatitude) && !TextUtils.isEmpty(CurrentLongitude)) {
                        service_lat=list1.get(i).getShopAddress().get(j).getSlat().replaceAll("^\\s+", "");
                        service_lan=list1.get(i).getShopAddress().get(j).getSlong().replaceAll("^\\s+", "");
                        d = CalculationByDistance(CurrentLatitude, CurrentLongitude, service_lat, service_lan);
                    }
                    updatedList.add(d);
                    // address.add(list1.get(i).getShopAddress().get(j).getAddress1());
                }
            }
            else{
                updatedList.add(0.0);
                address.add("");
            }
            if(updatedList.contains(0.0)) {
                newList = new ShopList(Collections.min(updatedList), list.get(i).getShop_name().trim(), list.get(i).getMname().trim(), list.get(i).getShop_image(), list.get(i).getSid(),list.get(i).getShopAddress());
                updatedDistance.add(newList);
                updatedList.clear();
            }
            else{
                newList = new ShopList(Collections.min(updatedList), list.get(i).getShop_name().trim(), list.get(i).getMname().trim(), list.get(i).getShop_image(), list.get(i).getSid(), list.get(i).getShopAddress());
                updatedDistance.add(newList);
                updatedList.clear();
            }
            Collections.sort(updatedDistance, new CustomComparator());
            //  newList = new ShopList(Collections.min(updatedList), list.get(i).getShop_name(), list.get(i).getMname(), list.get(i).getShop_image(), list.get(i).getSid(), address);
            // updatedDistance.add(newList);




               /* String address1 = list.get(i).getShopAddress().get(j).getAddress1();
                String address2 = list.get(i).getShopAddress().get(j).getAddress2();
                String city = list.get(i).getShopAddress().get(j).getCity();
                double phone = list.get(i).getShopAddress().get(j).getPhone();
                String pin = list.get(i).getShopAddress().get(j).getPin();
                int said = list.get(i).getShopAddress().get(j).getSaid();
                String scloseingtime = list.get(i).getShopAddress().get(j).getScloseingtime();
                int ssid = list.get(i).getShopAddress().get(j).getSid();
                String slat = list.get(i).getShopAddress().get(j).getSlat();
                String slong = list.get(i).getShopAddress().get(j).getSlong();
                String sopeningtime = list.get(i).getShopAddress().get(j).getSopeningtime();
                int uastatus = list.get(i).getShopAddress().get(j).getUastatus();

                ArrayList<ShopAddress> shopAddress = list.get(i).getShopAddress();
                if (!TextUtils.isEmpty(CurrentLatitude) && !TextUtils.isEmpty(CurrentLongitude)) {
                    d = CalculationByDistance(CurrentLatitude, CurrentLongitude, slat, slong);
                } else {
                    Toast.makeText(getActivity(), "not getting current location lat, laongitude", Toast.LENGTH_SHORT).show();
                }
             //   shopList = new ShopList(d,categoryList,shopAddress,chainbusiness,mid,mname,shop_image,shop_name,sid,sstatus,website);
              //  Toast.makeText(getActivity(),"shop_name "+shop_name,Toast.LENGTH_LONG).show();
                shopList = new ShopList(d,"",shopAddress,chainbusiness,mid,mname,shop_image,shop_name,sid,sstatus,website);
                newList = new ShopAddress(d,address1,address2,city,phone,pin,said,scloseingtime,sid,slat,slong,sopeningtime,uastatus);
                list2.add(newList);*/
            //  list1.add(shopList);
        }


        txtNoItem.setVisibility(View.GONE);
        //  Collections.sort(list2, new CustomComparator());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        FragmentRestaurantsAdapter moviesDirectoryAdapter = new FragmentRestaurantsAdapter(getContext(), updatedDistance);
        recyclerView.setAdapter(moviesDirectoryAdapter);

    }
}
