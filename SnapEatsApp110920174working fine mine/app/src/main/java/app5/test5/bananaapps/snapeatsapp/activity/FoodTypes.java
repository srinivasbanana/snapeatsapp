package app5.test5.bananaapps.snapeatsapp.activity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class FoodTypes implements Parcelable {

    @SerializedName("Artists_ID")
    private int Artists_ID;
    @SerializedName("Artists_Name")
    private String Artists_Name;
    @SerializedName("Artists_Pic")
    private String Artists_Pic;
    @SerializedName("Profession")
    private String Profession;

    protected FoodTypes(Parcel in) {
        Artists_ID = in.readInt();
        Artists_Name = in.readString();
        Artists_Pic = in.readString();
        Profession = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Artists_ID);
        dest.writeString(Artists_Name);
        dest.writeString(Artists_Pic);
        dest.writeString(Profession);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FoodTypes> CREATOR = new Creator<FoodTypes>() {
        @Override
        public FoodTypes createFromParcel(Parcel in) {
            return new FoodTypes(in);
        }

        @Override
        public FoodTypes[] newArray(int size) {
            return new FoodTypes[size];
        }
    };

    public int getArtists_ID() {
        return Artists_ID;
    }

    public void setArtists_ID(int artists_ID) {
        Artists_ID = artists_ID;
    }

    public String getArtists_Name() {
        return Artists_Name;
    }

    public void setArtists_Name(String artists_Name) {
        Artists_Name = artists_Name;
    }

    public String getArtists_Pic() {
        return Artists_Pic;
    }

    public void setArtists_Pic(String artists_Pic) {
        Artists_Pic = artists_Pic;
    }

    public String getProfession() {
        return Profession;
    }

    public void setProfession(String profession) {
        Profession = profession;
    }
}
