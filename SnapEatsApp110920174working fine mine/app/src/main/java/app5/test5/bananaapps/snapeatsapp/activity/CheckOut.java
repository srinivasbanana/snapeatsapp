package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.text.InputFilter;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigDecimal;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.payment.PaymentActivity;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

/**
 * Created by BananaApps on 6/3/2017.
 */

public class CheckOut extends BaseActivity {
    ImageView plus,minus,notification;
    TextView driverTipCount,driverTipTotal,addressLink;
    Button Btn_PaymentProcessed;
    EditText add1,add2,loc,pcode,county;
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    String adrs0;
    RelativeLayout relativeLayout;
    ImageView backArrow;

    int c=0;
    Double d=0.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout);
        plus = (ImageView)findViewById(R.id.plus);
        minus = (ImageView)findViewById(R.id.minus);
        driverTipCount = (TextView)findViewById(R.id.driverTipCount);
        driverTipTotal= (TextView)findViewById(R.id.driverTipTotal);
       // notification = (ImageView)findViewById(R.id.notification);
        Btn_PaymentProcessed = (Button)findViewById(R.id.Btn_PaymentProcessed);
        add1 = (EditText)findViewById(R.id.add1);
        add2 = (EditText)findViewById(R.id.add2);
        loc = (EditText)findViewById(R.id.add3);
        pcode = (EditText)findViewById(R.id.add4);
        county = (EditText)findViewById(R.id.add5);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        addressLink = (TextView)findViewById(R.id.addressLink);
        relativeLayout = (RelativeLayout) findViewById(R.id.checkHide);
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(v);
                return false;
            }
        });
        addressLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cache.putData(CatchValue.CHECKOUT_ADDRESS, getApplicationContext(), "checkoutAddress", Cache.CACHE_LOCATION_DISK);
                String s = getIntent().getStringExtra("fromProduct");
                Intent intent= new Intent(CheckOut.this, PlacesAutoCompleteActivity.class);
                if(s.equalsIgnoreCase("fromProduct"))
                    intent.putExtra("fromProduct", "fromProduct");
                else
                    intent.putExtra("fromProduct", "notfromProduct");
                startActivity(intent);
               // startActivity(new Intent(CheckOut.this, PlacesAutoCompleteActivity.class));
                finish();
            }
        });



       /* backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CheckOut.this, Basket.class));
                finish();
            }
        });*/

        c=5;
       // driverTipTotal.setText("£ "+((Double)Cache.getData(CatchValue.TOTAL_COST,this))+5.0);
        d=(Double)Cache.getData(CatchValue.TOTAL_COST,this);
        double d2 = (double) c;
        d = d2 + d;


        BigDecimal a = new BigDecimal(d);
        BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        driverTipTotal.setText("£ "+roundOff);
        pcode.setFilters(new InputFilter[] {  new InputFilter.LengthFilter(7)});

        settings = getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);

        adrs0 = settings.getString("adrs0", "");
        add1.setText(adrs0);

        adrs0 = settings.getString("adrs1", "");
        add2.setText(adrs0);

        adrs0 = settings.getString("locality", "");
        loc.setText(adrs0);

        adrs0 = settings.getString("postalcode", "");
        pcode.setText(adrs0);

        adrs0 = settings.getString("countryname", "");
        county.setText(adrs0);

        String k = (String) driverTipCount.getText().toString();
        c = Integer.parseInt(k);
        Cache.putData(CatchValue.DRIVER_TIP, getApplicationContext(), c, Cache.CACHE_LOCATION_DISK);

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(c!=0)
                c = c-1;
                driverTipCount.setText(""+c);
                Cache.putData(CatchValue.DRIVER_TIP, getApplicationContext(), c, Cache.CACHE_LOCATION_DISK);
                d = (Double)Cache.getData(CatchValue.TOTAL_COST,CheckOut.this);
                double d2 = (double) c;
                d = d2 + d;
                BigDecimal a = new BigDecimal(d);
                BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                driverTipTotal.setText("£ "+roundOff);
              //  driverTipTotal.setText("£ "+d);
            }
        });

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = c+1;
                driverTipCount.setText(""+c);
                Cache.putData(CatchValue.DRIVER_TIP, getApplicationContext(), c, Cache.CACHE_LOCATION_DISK);
                Double d = (Double)Cache.getData(CatchValue.TOTAL_COST,CheckOut.this);
                double d2 = (double) c;
                d = d2 + d;
                BigDecimal a = new BigDecimal(d);
                BigDecimal roundOff = a.setScale(2, BigDecimal.ROUND_HALF_EVEN);
                driverTipTotal.setText("£ "+roundOff);
             //   driverTipTotal.setText("£ "+d);
            }
        });

       /* notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CheckOut.this, Notification.class));
            }
        });*/

        Btn_PaymentProcessed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = getIntent().getStringExtra("fromProduct");
                Intent intent= new Intent(CheckOut.this, PaymentActivity.class);
                if(s.equalsIgnoreCase("fromProduct"))
                    intent.putExtra("fromProduct", "fromProduct");
                else
                    intent.putExtra("fromProduct", "notfromProduct");
                startActivity(intent);
               // startActivity(new Intent(CheckOut.this, PaymentActivity.class));
            }
        });
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    protected void hideKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
