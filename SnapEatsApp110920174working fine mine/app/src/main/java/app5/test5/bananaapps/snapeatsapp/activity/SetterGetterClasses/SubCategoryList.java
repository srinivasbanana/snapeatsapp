package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

import java.io.Serializable;

/**
 * Created by BananaApps on 6/1/2017.
 */
public class SubCategoryList implements Serializable {

    String id;
    String scid;
    String foodType;
    String name;
    String itemDesc;
    String price;
    String fImageurl;
    String quantity;
    String totQty;
    String totPrice;
    String serialNo;


    public SubCategoryList(String scid,String id, String foodType, String name, String itemDesc, String price, String fImageurl, String serialNo) {
        this.scid=scid;
        this.id = id;
        this.foodType = foodType;
        this.name = name;
        this.itemDesc = itemDesc;
        this.price = price;
        this.fImageurl = fImageurl;
        this.serialNo = serialNo;
    }

    public SubCategoryList(String scid,String id, String foodType, String name, String itemDesc, String price, String fImageurl, String serialNo, String quantity, String totalprice) {
        this.scid=scid;
        this.id = id;
        this.foodType = foodType;
        this.name = name;
        this.itemDesc = itemDesc;
        this.price = price;
        this.fImageurl = fImageurl;
        this.serialNo = serialNo;
        this.quantity = quantity;
        this.totPrice = totalprice;
    }

    public SubCategoryList() {
    }

    public String getTotQty() {
        return totQty;
    }

    public void setTotQty(String totQty) {
        this.totQty = totQty;
    }

    public String getTotPrice() {
        return totPrice;
    }

    public void setTotPrice(String totPrice) {
        this.totPrice = totPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getfImageurl() {
        return fImageurl;
    }

    public void setfImageurl(String fImageurl) {
        this.fImageurl = fImageurl;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }
}