package app5.test5.bananaapps.snapeatsapp.activity;


//import citymovies.com.citymovies.models.MovieDetailsResponse;

import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SnapEatsApiService {


    /*@GET("api/Movie/MovieDetails")
    Call<SnapEatsApiResponse> getMovies();*/

    @GET("Shops/shops")
    Call<SnapEatsApiResponse> getShops();

    @GET("Items/items")
    Call<SnapEatsApiResponse> getItems();

}
