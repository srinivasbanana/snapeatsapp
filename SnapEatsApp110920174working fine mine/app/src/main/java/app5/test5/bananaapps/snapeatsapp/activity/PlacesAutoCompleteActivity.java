package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.*;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.payment.PaymentActivity;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;
import rx.exceptions.Exceptions;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class PlacesAutoCompleteActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    protected GoogleApiClient mGoogleApiClient;
    public static final String PREFS_NAME = "SnapEatsVariables";

    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(
    new LatLng(-0, 0), new LatLng(0, 0));
    String result = null;
    private EditText mAutocompleteView;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private PlacesAutoCompleteAdapter mAutoCompleteAdapter;
    ImageView delete;
    FragmentManager fragmentManager;
    Fragment fragment;
    FragmentTransaction transaction;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        buildGoogleApiClient();
        setContentView(R.layout.activity_search);
        mAutocompleteView = (EditText)findViewById(R.id.autocomplete_places);

        delete=(ImageView)findViewById(R.id.cross);

        mAutoCompleteAdapter =  new PlacesAutoCompleteAdapter(this, R.layout.searchview_adapter, mGoogleApiClient, BOUNDS_INDIA, null);
        fragmentManager = getSupportFragmentManager();
        mRecyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        mLinearLayoutManager=new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAutoCompleteAdapter);
        delete.setOnClickListener(this);
        mAutocompleteView.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (!s.toString().equals("") && mGoogleApiClient.isConnected()) {
                    mRecyclerView.setAdapter(mAutoCompleteAdapter);
                    mAutoCompleteAdapter.getFilter().filter(s.toString());
                }else if(!mGoogleApiClient.isConnected()){
                    Log.e("TAG",Constants.API_NOT_CONNECTED);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mAutoCompleteAdapter.getItem(position);
                        final String placeId = String.valueOf(item.placeId);
                        Log.i("TAG", "Autocomplete item selected: " + item.description);
                        /*
                             Issue a request to the Places Geo Data API to retrieve a Place object with additional details about the place.
                         */
                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                                .getPlaceById(mGoogleApiClient, placeId);
                        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(PlaceBuffer places) {
                                if(places.getCount()==1) {
                                    //Do the things here on Click...
                                    try {
                                        StringBuilder sb = new StringBuilder();
                                        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                                        SharedPreferences.Editor editor2 = settings.edit();
                                        mAutocompleteView.setText(places.get(0).getName());
                                        sb.append(places.get(0).getName().toString()).append("\n");
                                        sb.append(places.get(0).getLatLng().toString()).append("\n");
                                        try {
                                            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.ENGLISH);
                                            // checkout data
                                            List<android.location.Address> addressList = geocoder.getFromLocation(places.get(0).getLatLng().latitude, places.get(0).getLatLng().longitude, 2);
                                            if (addressList != null && addressList.size() > 0) {
                                                android.location.Address address = addressList.get(0);
                                                StringBuilder sb2 = new StringBuilder();

                                                //     Log.e("SnapEatsTest","isAddressChanged 2 "+isAddressChanged.length());
                                                editor2 = settings.edit();
                                                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                                    sb2.append(address.getAddressLine(i)).append("\n");
                                                    editor2.putString("adrs" + i, address.getAddressLine(i).toString());
                                                    if (i == 0)
                                                        Cache.putData(CatchValue.Delivary_adrs0, getApplicationContext(), address.getAddressLine(i).toString(), Cache.CACHE_LOCATION_DISK);
                                                    else if (i == 1)
                                                        Cache.putData(CatchValue.Delivary_adrs1, getApplicationContext(), address.getAddressLine(i).toString(), Cache.CACHE_LOCATION_DISK);
                                                }
                                                sb2.append(address.getLocality()).append("\n");
                                                sb2.append(address.getPostalCode()).append("\n");
                                                sb2.append(address.getCountryName());
                                                editor2.putString("locality", address.getLocality().toString());
                                                editor2.putString("postalcode", address.getPostalCode().toString());
                                                editor2.putString("countryname", address.getCountryName().toString());

                                                Cache.putData(CatchValue.Delivary_location, getApplicationContext(), address.getLocality().toString(), Cache.CACHE_LOCATION_DISK);
                                                Cache.putData(CatchValue.Delivary_pincode, getApplicationContext(), address.getPostalCode().toString(), Cache.CACHE_LOCATION_DISK);
                                                Cache.putData(CatchValue.Delivary_country, getApplicationContext(), address.getCountryName().toString(), Cache.CACHE_LOCATION_DISK);

                                                // editor2.putLong("CurrentLatitude", Double.doubleToLongBits(address.getLatitude()));
                                                // editor2.putLong("CurrentLongitude", Double.doubleToLongBits(address.getLongitude()));
                                                //      editor2.putString("CurrentLatitude", "" + latitude);
                                                //      editor2.putString("CurrentLongitude", "" + longitude);
                                                // editor2.putString("CurrentLatitude",""+17.6868);
                                                // editor2.putString("CurrentLongitude",""+83.2185);

                                                // editor2.putString("promoCode", "");
                                                // Log.e("CurrentLatitude ", "" + address.getLatitude());
                                                //  editor2.putString("CurrentLongitude",""+address.getLongitude());
                                                editor2.commit();

                                            }
                                            // checkout data end
                                        }
                                        catch (Exception e)
                                        {

                                        }

                                        editor2.putString("placeData", "" + places.get(0).getName().toString());
                                        //editor2.putString("CurrentData", "" + places.get(0).getLatLng().toString());
                                        editor2.putString("CLatitude", "" + String.valueOf(places.get(0).getLatLng().latitude));
                                        editor2.putString("CLongitude", "" + String.valueOf(places.get(0).getLatLng().longitude));
                                        editor2.commit();
                                        result = sb.toString();
                                        mRecyclerView.setAdapter(null);
                                        Toast.makeText(getApplicationContext(), String.valueOf(places.get(0).getLatLng()), Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        Message message = Message.obtain();
                                        if (result != null) {
                                            message.what = 1;
                                            Bundle bundle = new Bundle();
                                            result = "placeData: " + places.get(0).getName().toString() + " Longitude: " + places.get(0).getLatLng().toString();
                                            bundle.putString("address", result);
                                            message.setData(bundle);
                                        } else {
                                            message.what = 1;
                                            Bundle bundle = new Bundle();
                                            result = "Latitude: " + places.get(0).getName().toString() + " Longitude: " + places.get(0).getLatLng().toString() +
                                                    "\n Unable to get address for this lat-long.";
                                            bundle.putString("address", result);
                                            message.setData(bundle);
                                        }
                                    }
//                                    Intent i= new Intent(PlacesAutoCompleteActivity.this, HomePage.class);
//                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                                    finish();
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putString("logStatus","Skip");
                                    editor.putString("isAddressChange","false");
                                    editor.commit();
                                    String s = (String) Cache.getData(CatchValue.CHECKOUT_ADDRESS, PlacesAutoCompleteActivity.this);
                                    if(s!=null) {
                                        if (s.equalsIgnoreCase("checkoutAddress")) {
                                            String s2 = getIntent().getStringExtra("fromProduct");
                                            Intent intent = new Intent(PlacesAutoCompleteActivity.this, CheckOut.class);
                                            if (s2.equalsIgnoreCase("fromProduct"))
                                                intent.putExtra("fromProduct", "fromProduct");
                                            else
                                                intent.putExtra("fromProduct", "notfromProduct");
                                            startActivity(intent);
                                            //  startActivity(new Intent(PlacesAutoCompleteActivity.this, CheckOut.class));
                                        }
                                    }
                                    else
                                    startActivity(new Intent(PlacesAutoCompleteActivity.this, HomePage.class));
                                    finish();
                                }
                                else {
                                   // Toast.makeText(getApplicationContext(),Constants.SOMETHING_WENT_WRONG,Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        Log.i("TAG", "Clicked: " + item.description);
                        Log.i("TAG", "Called getPlaceById to get Place details for " + item.placeId);
                    }
                })
        );
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v("Google API Callback", "Connection Done");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Google API Callback","Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));
    }

    @Override
    public void onClick(View v) {
        if(v==delete){
            mAutocompleteView.setText("");
            super.onBackPressed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mGoogleApiClient.isConnected() && !mGoogleApiClient.isConnecting()){
            Log.v("Google API","Connecting");
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mGoogleApiClient.isConnected()){
            Log.v("Google API","Dis-Connecting");
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
