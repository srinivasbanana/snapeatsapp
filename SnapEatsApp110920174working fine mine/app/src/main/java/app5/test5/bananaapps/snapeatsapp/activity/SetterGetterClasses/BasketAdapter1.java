package app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses;

/**
 * Created by BananaApps on 6/1/2017.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.activity.BasketData;

import java.util.List;


public class BasketAdapter1 extends BaseAdapter {

    private final Activity context;
   // private final String[] web;

  //  private final String[] dist;
    private List<BasketData> basketData = null;
    BasketData user;

    public BasketAdapter1(Activity context,
                          List<BasketData> basketData) {
       /// super(context, R.layout.baslet_list_single);
        this.basketData =basketData;
        this.context = context;
    }

    @Override
    public int getCount() {
        return basketData.size();
    }

    @Override
    public Object getItem(int i) {
        return basketData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View rowView = view;
        UserHolder holder = null;
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            //  rowView= inflater.inflate(R.layout.subcategory_list_single, null, true);

                rowView = inflater.inflate(R.layout.baslet_list_single, null, true);


            holder = new UserHolder();

            holder.txtTitle = (TextView) rowView.findViewById(R.id.txt);
            holder.txtDist = (TextView) rowView.findViewById(R.id.distancet);
            holder.imageView = (ImageView) rowView.findViewById(R.id.iv);
            holder.plus = (ImageView) rowView.findViewById(R.id.plus);
            holder.nm = (TextView)rowView.findViewById(R.id.countText);
            holder.minus = (ImageView) rowView.findViewById(R.id.minus);
            holder.cost = (TextView)rowView.findViewById(R.id.driverTipCount);
            rowView.setTag(holder);

        } else {

            holder = (UserHolder) rowView.getTag();
        }

        user = basketData.get(position);
        holder.txtTitle.setText(user.getWeb());
        holder.txtDist.setText(user.getDist());


        if (holder.plus != null) {
            final UserHolder finalHolder = holder;

            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalHolder.nm != null) {
                        CharSequence cs = finalHolder.nm.getText();
                        int number = Integer.parseInt(cs.toString());
                        number++;
                        finalHolder.nm.setText(""+number);
                        finalHolder.cost.setText(""+(number*Integer.parseInt(""+5)));
                    }
                }
            });

            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalHolder.nm != null) {
                        CharSequence cs = finalHolder.nm.getText();
                        int number = Integer.parseInt(cs.toString());
                        if(number>0)
                            number--;
                        finalHolder.nm.setText(""+number);
                        finalHolder.cost.setText(""+(number*Integer.parseInt(""+5)));
                    }
                }
            });
        }

        return rowView;
    }

    static class UserHolder {
        TextView txtTitle;
        TextView txtDist;
        ImageView imageView;
        ImageView plus;
        TextView nm;
        ImageView minus;
        TextView cost;
    }
}