package app5.test5.bananaapps.snapeatsapp.activity.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.stripe.android.model.Token;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.OrderPlaced;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

import static android.content.Context.MODE_PRIVATE;

/**
 * A controller for the {@link ListView} used to display the results.
 */
public class ListViewController {
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    private SimpleAdapter mAdatper;
    private List<Map<String, String>> mCardTokens = new ArrayList<Map<String, String>>();
    private Context mContext;
    BucketDB bucketDB;
    String userId;
    String paymentToken;
    Double totalAmount;
    String itemId,itemQunt,itemPrice;
    JSONObject itemObject,resultJsonObject;
    JSONArray itemArray;
    ProgressDialog progressDialog;
    private List<SubCategoryList> savedQty;
    String add1,add2,loc,pcode,county;
    int shopId;

    public ListViewController(ListView listView) {
        mContext = listView.getContext();
        mAdatper = new SimpleAdapter(
                mContext,
                mCardTokens,
                R.layout.list_item_layout,
                new String[]{"last4", "tokenId"},
                new int[]{R.id.last4, R.id.tokenId});

        listView.setAdapter(mAdatper);
        bucketDB=new BucketDB(mContext);
        savedQty=bucketDB.getAllDetails();
        userId = (String) Cache.getData(CatchValue.USER_ID, mContext);
        totalAmount=(Double) Cache.getData(CatchValue.TOTAL_COST, mContext);
        shopId=(Integer) Cache.getData(CatchValue.SHOP_ID, mContext);
        settings = mContext.getSharedPreferences(PREFS_NAME,  MODE_PRIVATE);
        add1 = settings.getString("adrs0", "");
        add2 = settings.getString("adrs1", "");
        loc = settings.getString("locality", "");
        pcode = settings.getString("postalcode", "");
        county = settings.getString("countryname", "");

        itemObject=new JSONObject();

    }

    void addToList(Token token) {
        if(!TextUtils.isEmpty(token.getId())) {
            new PaymentTask().execute(token.getId(),add1,add2, String.valueOf(shopId),loc,pcode,county);
        }
        else{
            showToastMessage("Please try again");
        }
        addToList(token.getCard().getLast4(), token.getId());
    }

    public void addToList(@NonNull String last4, @NonNull String tokenId) {
        String endingIn = mContext.getString(R.string.endingIn);
        Map<String, String> map = new HashMap<>();
        map.put("last4", endingIn + " " + last4);
        map.put("tokenId", tokenId);


        mCardTokens.add(map);
        mAdatper.notifyDataSetChanged();
    }

    private class PaymentTask extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Report doInBackground(String... params) {
            try {
                JSONObject jObject = new JSONObject();
                itemArray= new JSONArray();

                jObject.put("said", 1);
                jObject.put("uid", userId);
                jObject.put("totamt", totalAmount);
                jObject.put("drivertip", 10);
                jObject.put("pcode", 1);
                jObject.put("order_notes", "test order notes");
                jObject.put("payment_id", params[0]);
                jObject.put("D_Address1", params[1]);
                jObject.put("D_Address2", params[2]);
                jObject.put("Shop_Id", params[3]);
                jObject.put("City", params[4]);
                jObject.put("PinCode", params[5]);

                for(int i=0;i<savedQty.size();i++){
                    itemObject=new JSONObject();
                    itemId=savedQty.get(i).getScid();
                    itemQunt=savedQty.get(i).getQuantity();
                    itemPrice=savedQty.get(i).getTotPrice();
                    itemObject.put("item_Id",itemId);
                    itemObject.put("item_qty",itemQunt);
                    itemObject.put("item_price",itemPrice);
                    itemArray.put(itemObject);
                }

                jObject.put("OrderItems", itemArray);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/Order/PostOrder");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                gerLoginResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again");
            }
        }
    }



    private void gerLoginResponse(Report response) {
        try {
            resultJsonObject = response.getJsonObject();
            if(resultJsonObject.length()>0&&resultJsonObject!=null) {
                if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("200")) {
                    String resultJson = resultJsonObject.getString("Message");
                    showToastMessage(resultJson);
                    Intent i = new Intent(mContext,OrderPlaced.class);
                    mContext.startActivity(i);
                }
                else {
                    showToastMessage(response.getMessage());
                }
            }
            else {
                showToastMessage(response.getMessage());
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }


    public void showProgressDialog() {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }
    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
    private void showToastMessage(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }


}
