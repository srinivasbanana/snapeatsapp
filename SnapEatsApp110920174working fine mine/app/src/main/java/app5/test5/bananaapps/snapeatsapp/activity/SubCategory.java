package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by BananaApps on 6/5/2017.
 */
import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.FoodTypeList;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.ItemValues;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by BananaApps on 6/5/2017.
 */

//public class SubCategory extends BaseActivity {
public class SubCategory extends BaseActivity {

    Button gotobasket;
    ImageView notification;
    TextView toolbar_title;
    ImageButton grid;
    boolean flag = false;
    GridView gridv;
    RecyclerView sublistData;
    ImageView close;
    TextView attention;
    List<SubCategoryList> subCategoryData, subItemlistTypes;
    ArrayList<SubCategoryList> subCategoryUpdatedData;
    List<SubCategoryList> itemValues;
    int status = 0;
    ImageView backArrow;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    ProgressDialog progressDialog;
    JSONArray resultJsonArray;
    JSONObject resultData;
    String itemName, itemDesc, price;
    int quanty;
    String fImageurl, menuListName,menuList;
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    private SubCategoryDataAdapter subCategoryDataAdapter;
    private SubCategoryDataAdapterGrid subCategoryDataAdapterGrid;
    String fromclass;
    List<SubCategoryList> finalUpdatedList;
    BucketDB bucketDB;
    int position=0;


    private List<SubCategoryList> subCategoryList;
    private List<SubCategoryList> savedQtyList;
    private List<SubCategoryList> savedQty;
    private static List<String> savedItemSNoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_category);
        savedQtyList=SubCategoryDataAdapter.slotsList;
        savedQty=new ArrayList<>();
        bucketDB=new BucketDB(this);
        savedItemSNoList=new ArrayList<>();
        gotobasket = (Button) findViewById(R.id.gotobasket);
        grid = (ImageButton) findViewById(R.id.grid);
      //  notification = (ImageView) findViewById(R.id.notification);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        sublistData = (RecyclerView) findViewById(R.id.listSubCategory);
        backArrow = (ImageView) findViewById(R.id.backArrow);

//        menuListName = getIntent().getStringExtra("sublistype");
//        subCategoryData = (ArrayList<SubCategoryList>) getIntent().getSerializableExtra("sublisData");

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == 0) {
                    grid.setBackgroundResource(R.mipmap.listgrid);
                    subCategoryDataAdapterGrid = new SubCategoryDataAdapterGrid(SubCategory.this, subCategoryList, "initial");
                    final GridLayoutManager layoutManager = new GridLayoutManager(SubCategory.this, 2);
                    sublistData.setLayoutManager(layoutManager);
                    sublistData.setItemAnimator(new DefaultItemAnimator());
                    sublistData.setAdapter(subCategoryDataAdapterGrid);
                    subCategoryDataAdapterGrid.notifyDataSetChanged();
                    position++;
                } else if (position == 1) {
                    grid.setBackgroundResource(R.mipmap.grid);
                    subCategoryDataAdapter = new SubCategoryDataAdapter(SubCategory.this, subCategoryList, "updated");
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(SubCategory.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    sublistData.setLayoutManager(layoutManager);
                    sublistData.setItemAnimator(new DefaultItemAnimator());
                    sublistData.setAdapter(subCategoryDataAdapter);
                    subCategoryDataAdapter.notifyDataSetChanged();
                    position--;
                }
            }
        });


        gotobasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int totalQuant = 0;

                try {
                    itemValues = bucketDB.getAllDetails();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (itemValues != null && itemValues.size() > 0) {
                    for (int i = 0; i < itemValues.size(); i++) {
                        if (itemValues.get(i).getQuantity() == null) {
                            itemValues.get(i).setQuantity("0");
                        }
                        if (Integer.parseInt(itemValues.get(i).getQuantity()) > 0) {
                            totalQuant += Integer.parseInt(itemValues.get(i).getQuantity());
                        }
                    }
                }

                if (totalQuant > 0) {
                    Intent basket = new Intent(SubCategory.this, Basket.class);
                    Cache.putData(CatchValue.MENU_LIST, getApplicationContext(), menuListName, Cache.CACHE_LOCATION_DISK);
                    startActivity(basket);
                } else {
                    Toast.makeText(getApplicationContext(), "Item quantity should not be 0", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }



    private void getListData(List<SubCategoryList> subCategoryData) {

        this.subCategoryData = subCategoryData;
        subItemlistTypes = new ArrayList<>();
        subCategoryUpdatedData = new ArrayList<>();
        subCategoryList = new ArrayList<>();
        if (subCategoryData != null && subCategoryData.size() > 0) {
            if (!TextUtils.isEmpty(menuListName)) {
                for (int j = 0; j < subCategoryData.size(); j++) {
                    if (subCategoryData.get(j).getFoodType().equalsIgnoreCase(menuListName)) {
                        if (savedItemSNoList != null && savedItemSNoList.size() > 0) {
                            if (savedItemSNoList.contains(subCategoryData.get(j).getScid())) {
                                for (int i = 0; i < savedQty.size(); i++) {
                                    try {
//                                        if (savedQty.size() == subCategoryList.size()) {
//                                            break;
//                                        } else {
                                            if (savedQty.get(i).getScid().equals(subCategoryData.get(j).getScid())) {
                                                subCategoryList.add(new SubCategoryList(savedQty.get(i).getScid(),savedQty.get(i).getId(),
                                                        savedQty.get(i).getFoodType(), savedQty.get(i).getName(),
                                                        savedQty.get(i).getItemDesc(), savedQty.get(i).getPrice(), savedQty.get(i).getfImageurl(), savedQty.get(i).getSerialNo(), savedQty.get(i).getQuantity(), savedQty.get(i).getTotPrice()));
                                            }
                                        status++;

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }


                            } else {
                                subCategoryList.add(new SubCategoryList(subCategoryData.get(j).getScid(),subCategoryData.get(j).getId(),
                                        subCategoryData.get(j).getFoodType(), subCategoryData.get(j).getName(),
                                        subCategoryData.get(j).getItemDesc(), subCategoryData.get(j).getPrice(), subCategoryData.get(j).getfImageurl(), subCategoryData.get(j).getSerialNo(), subCategoryData.get(j).getTotQty(), subCategoryData.get(j).getTotPrice()));
                                status++;
                            }

                        } else {

                            subCategoryList.add(new SubCategoryList(subCategoryData.get(j).getScid(),subCategoryData.get(j).getId(),
                                    subCategoryData.get(j).getFoodType(), subCategoryData.get(j).getName(),
                                    subCategoryData.get(j).getItemDesc(), subCategoryData.get(j).getPrice(), subCategoryData.get(j).getfImageurl(), subCategoryData.get(j).getSerialNo(), subCategoryData.get(j).getTotQty(), subCategoryData.get(j).getTotPrice()));
                        }

                    }
                }
                if (status == 0) {
                    subCategoryDataAdapter = new SubCategoryDataAdapter(SubCategory.this, subCategoryList, "initial");
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(SubCategory.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    sublistData.setLayoutManager(layoutManager);
                    sublistData.setItemAnimator(new DefaultItemAnimator());
                    sublistData.setAdapter(subCategoryDataAdapter);
                    subCategoryDataAdapter.notifyDataSetChanged();
                } else if (status > 0) {
                    subCategoryDataAdapter = new SubCategoryDataAdapter(SubCategory.this, subCategoryList, "updated");
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(SubCategory.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    sublistData.setLayoutManager(layoutManager);
                    sublistData.setItemAnimator(new DefaultItemAnimator());
                    sublistData.setAdapter(subCategoryDataAdapter);
                    subCategoryDataAdapter.notifyDataSetChanged();

                }
            }

        }
    }


/*


        if (!TextUtils.isEmpty(menuListName)) {
            if(finalUpdatedList != null && finalUpdatedList.size() > 0){
            for(int i=0;i<finalUpdatedList.size();i++) {
                for (int j = 0; j < subCategoryData.size(); j++) {
                    if (subCategoryData.get(j).getFoodType().equalsIgnoreCase(menuListName)) {
                        if (finalUpdatedList.get(i).getSerialNo() == subCategoryData.get(j).getSerialNo()) {
                            SubCategoryList mark = new SubCategoryList(subCategoryData.get(j).getId(),
                                    subCategoryData.get(j).getFoodType(), subCategoryData.get(j).getName(),
                                    subCategoryData.get(j).getItemDesc(), subCategoryData.get(j).getPrice(), subCategoryData.get(j).getfImageurl(), subCategoryData.get(j).getSerialNo(), finalUpdatedList.get(i).getQuantity());
                            subItemlistTypes.add(mark);
                        }
                    }
                }
            }
                subCategoryDataAdapter = new SubCategoryDataAdapter(SubCategory.this, subItemlistTypes, "initial");
                final LinearLayoutManager layoutManager = new LinearLayoutManager(SubCategory.this);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                sublistData.setLayoutManager(layoutManager);
                sublistData.setItemAnimator(new DefaultItemAnimator());
                sublistData.setAdapter(subCategoryDataAdapter);
                subCategoryDataAdapter.notifyDataSetChanged();
            }

            else{
                for (int i = 0; i < subCategoryData.size(); i++) {
                    if (subCategoryData.get(i).getFoodType().equalsIgnoreCase(menuListName)) {
                            SubCategoryList mark = new SubCategoryList(subCategoryData.get(i).getId(),
                                    subCategoryData.get(i).getFoodType(), subCategoryData.get(i).getName(),
                                    subCategoryData.get(i).getItemDesc(), subCategoryData.get(i).getPrice(), subCategoryData.get(i).getfImageurl(), subCategoryData.get(i).getSerialNo());
                            subItemlistTypes.add(mark);
                        }

                }
                subCategoryDataAdapter = new SubCategoryDataAdapter(SubCategory.this, subItemlistTypes, "initial");
                final LinearLayoutManager layoutManager = new LinearLayoutManager(SubCategory.this);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                sublistData.setLayoutManager(layoutManager);
                sublistData.setItemAnimator(new DefaultItemAnimator());
                sublistData.setAdapter(subCategoryDataAdapter);
                subCategoryDataAdapter.notifyDataSetChanged();
            }

        } else {
            subCategoryData = (ArrayList<SubCategoryList>) BasketAdapter.itemValues;
            subCategoryDataAdapter = new SubCategoryDataAdapter(SubCategory.this, subCategoryData, "updated");
            final LinearLayoutManager layoutManager = new LinearLayoutManager(SubCategory.this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            sublistData.setLayoutManager(layoutManager);
            sublistData.setItemAnimator(new DefaultItemAnimator());
            sublistData.setAdapter(subCategoryDataAdapter);
            subCategoryDataAdapter.notifyDataSetChanged();
        }

*/


    public void ShowNoInternetDialog() {
        showAlertDialog(SubCategory.this, "No Internet Connection", "Please check your network.", false);
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    getListData(subCategoryData);
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        menuListName = (String) Cache.getData(CatchValue.MENU_LIST, SubCategory.this);
        if (!TextUtils.isEmpty(menuListName)) {
            toolbar_title.setText(menuListName);
        }
        fromclass = args.getString("fromClass");
        subCategoryData = CategoryList.foodTypeLists;
        if (fromclass.equalsIgnoreCase("CategoryList")) {
            savedQty = bucketDB.getIndividualDetails(menuListName);
            if (savedQty != null && savedQty.size() > 0) {
                for (int i = 0; i < savedQty.size(); i++) {
                    savedItemSNoList.add(savedQty.get(i).getScid());

                }
            }
        }

        else {
            fromclass.equalsIgnoreCase("Basket");
            savedQty = bucketDB.getIndividualDetails(menuListName);
            if (savedQty != null && savedQty.size() > 0) {
                for (int i = 0; i < savedQty.size(); i++) {
                    if(!savedItemSNoList.contains(savedQty.get(i).getScid())){
                        savedItemSNoList.add(savedQty.get(i).getScid());
                    }

                }

            }


        }
        cd = new ConnectionDetector(SubCategory.this);
        isInternetPresent = cd.isConnectionAvailable();
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage("Please wait...");
        progress.show();
        progress.setCancelable(false);

        Runnable progressRunnable = new Runnable() {
            @Override
            public void run() {
                progress.cancel();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 3000);

        if (isInternetPresent) {
            getListData(subCategoryData);
        } else {
            ShowNoInternetDialog();
        }
    }
}
