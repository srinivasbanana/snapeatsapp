package app5.test5.bananaapps.snapeatsapp.activity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shop implements Parcelable {

    @SerializedName("shop_image")
    @Expose
    private String url;

    @SerializedName("shop_name")
    @Expose
    private String name1;

    @SerializedName("lat")
    @Expose
    private String dLatitude;

    @SerializedName("long")
    @Expose
    private String dLongitude;
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("streetAddress")
    @Expose
    private String address;

    private Double diatsance;

    @SerializedName("address1")
    @Expose
    private String address1;

    protected Shop(Parcel in) {
        url = in.readString();
        name1 = in.readString();
        dLatitude=in.readString();
        dLongitude=in.readString();
        id=in.readString();
        address=in.readString();
        address1=in.readString();
       }

    protected Shop(Double in, String name1, String url,String id, String address,String address1) {
        diatsance = in;
        this.url=url;
        this.name1=name1;
        this.id=id;
        this.address=address;
        this.address1=address1;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Double getDiatsance() {
        return diatsance;
    }

    public void setDiatsance(Double diatsance) {
        this.diatsance = diatsance;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(name1);
        dest.writeString(dLatitude);
        dest.writeString(dLongitude);
        dest.writeString(id);
        dest.writeString(address);
        dest.writeString(address1);
    }

    public String getLogoUrl() {
        return url;
    }

    public void setLogoUrl(String logoUrl) {
        this.url = logoUrl;
    }

    public String getName() {
        return name1;
    }

    public void setName(String name){this.name1=name;}

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getdLatitude() {
        return dLatitude;
    }

    public void setdLatitude(String dLatitude) {
        this.dLatitude = dLatitude;
    }

    public String getdLongitude() {
        return dLongitude;
    }

    public void setdLongitude(String dLongitude) {
        this.dLongitude = dLongitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
