package app5.test5.bananaapps.snapeatsapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;

/**
 * Created by BananaApps on 5/30/2017.
 */

public class ForgotPwd extends BaseActivity {
    EditText frg_pwd;
    Button sendEmail;
    TextView heading,subheading;
    SharedPreferences settings;
    public static final String PREFS_NAME = "SnapEatsVariables";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_pwd);

        heading = (TextView)findViewById(R.id.heading);
        subheading = (TextView)findViewById(R.id.Subheading);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        heading.setTypeface(custom_font2);
        subheading.setTypeface(custom_font2);



        frg_pwd = (EditText)findViewById(R.id.frg_pwd);
        sendEmail = (Button)findViewById(R.id.send_email);

        sendEmail.setTypeface(custom_font2);

        Typeface custom_font3 = Typeface.createFromAsset(getAssets(),  "Lato-Regular.ttf");
        frg_pwd.setTypeface(custom_font3);


        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final String spemail = settings.getString("email", "");


        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(frg_pwd.getText().toString().trim().length()==0)
                    okMessage("Email error","Please enter Email ID");
                  else
                    if(!IsValidEmail(frg_pwd.getText().toString().trim()))
                        okMessage("Email error","Please enter valid Email ID");
              /*  else
                   if(!spemail.equals(frg_pwd.getText().toString().trim()))
                       okMessage("Email error","Email ID Not exist...");*/
                else
                   {
                       SharedPreferences.Editor editor2 = settings.edit();
                       editor2.putString("email",frg_pwd.getText().toString().trim());
                       editor2.commit();
                       startActivity(new Intent(ForgotPwd.this, ForgotPwdSuccess.class));
                       finish();
                   }
            }
        });

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ForgotPwd.this, Login.class));
        finish();
        super.onBackPressed();
    }
}
