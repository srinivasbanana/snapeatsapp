package app5.test5.bananaapps.snapeatsapp.activity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ItemDetails implements Parcelable {

    public void setCid(int cid) {
        this.cid = cid;
    }

    public void setScat_desc(String scat_desc) {
        this.scat_desc = scat_desc;
    }

    public void setScat_image(String scat_image) {
        this.scat_image = scat_image;
    }

    public void setScat_name(String scat_name) {
        this.scat_name = scat_name;
    }

    public int getCid() {
        return cid;
    }

    public String getScat_desc() {
        return scat_desc;
    }

    public String getScat_image() {
        return scat_image;
    }

    public String getScat_name() {
        return scat_name;
    }

    public int getScat_price() {
        return scat_price;
    }

    public int getScid() {
        return scid;
    }

    public int getScstatus() {
        return scstatus;
    }

    public int getSid() {
        return sid;
    }

    public String getSname() {
        return sname;
    }

    public void setScat_price(int scat_price) {
        this.scat_price = scat_price;
    }

    public void setScid(int scid) {
        this.scid = scid;
    }

    public void setScstatus(int scstatus) {
        this.scstatus = scstatus;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    @SerializedName("cid")
    @Expose
    private int cid;

    @SerializedName("cname")
    @Expose
    private String cname;

    @SerializedName("scat_desc")
    @Expose
    private String scat_desc;

    @SerializedName("scat_image")
    @Expose
    private String scat_image;

    @SerializedName("scat_name")
    @Expose
    private String scat_name;

    @SerializedName("scat_price")
    @Expose
    private int scat_price;

    @SerializedName("scid")
    @Expose
    private int scid;

    @SerializedName("scstatus")
    @Expose
    private int scstatus;

    @SerializedName("sid")
    @Expose
    private int sid;

    @SerializedName("sname")
    @Expose
    private String sname;




    protected ItemDetails(Parcel in) {
        cid = in.readInt();
        cname = in.readString();
        scat_desc = in.readString();
        scat_image = in.readString();
        scat_name = in.readString();
        scat_price = in.readInt();
        scid = in.readInt();
        scstatus = in.readInt();
        sid = in.readInt();
        sname = in.readString();

       }

    protected ItemDetails(int cid,String cname,String scat_desc,String scat_image,String scat_name,int scat_price,int scid,int scstatus,int sid,String sname) {
      //  this.cname=cname;
        this.cid = cid;
        this.cname =cname;
        this.scat_desc = scat_desc;
        this.scat_image = scat_image;
        this.scat_name = scat_name;
        this.scat_price = scat_price;
        this.scid = scid;
        this.scstatus = scstatus;
        this.sid = sid;
        this.sname = sname;

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cid);
        dest.writeString(cname);
        dest.writeString(scat_desc);
        dest.writeString(scat_image);
        dest.writeString(scat_name);
        dest.writeInt(scat_price);
        dest.writeInt(scid);
        dest.writeInt(scstatus);
        dest.writeInt(sid);
        dest.writeString(sname);


    }



    public String getCname() {
        return cname;
    }

    public void setCame(String cname) {
        this.cname = cname;
    }



    public static final Creator<ItemDetails> CREATOR = new Creator<ItemDetails>() {
        @Override
        public ItemDetails createFromParcel(Parcel in) {
            return new ItemDetails(in);
        }

        @Override
        public ItemDetails[] newArray(int size) {
            return new ItemDetails[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
}
