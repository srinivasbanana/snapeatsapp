package app5.test5.bananaapps.snapeatsapp.activity;

/**
 * Created by BananaApps on 6/1/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ArrayList;

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.MyViewHolder> {

    DecimalFormat df;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    public static List<SubCategoryList> slotsList;
    public static List<SubCategoryList> itemValues;
    private Context context;
    public static final String PREFS_NAME = "SnapEatsVariables";
    int number;
    String result = null;
    Double TotalCoast = 0.00;
    SharedPreferences settings;
    OnClickInAdapter onClickInAdapter;
    public static List<String> savedItemSNoList;
    private List<SubCategoryList> savedQtyList;
    BucketDB bucketDB;

    public BasketAdapter(Context cnxt, List<SubCategoryList> bList) {
        this.context = cnxt;
        this.slotsList = bList;
        savedItemSNoList=new ArrayList<>();

        cd = new ConnectionDetector(cnxt);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.baslet_list_single, parent, false);
        itemValues = slotsList;
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        // TotalCoast += Double.parseDouble(slotsList.get(position).getTotPrice());
        if (slotsList.size() - 1 == position) {
            holder.view1.setVisibility(View.GONE);
        }
        final SubCategoryList slots = slotsList.get(position);
        df = new DecimalFormat("0.00");

        if (!TextUtils.isEmpty(slots.getName())) {
            holder.fItemName.setText(slots.getName());
        }

        if (!TextUtils.isEmpty(slots.getItemDesc())) {
            holder.fItemNameDesc.setText(slots.getItemDesc());
        }

        if (!TextUtils.isEmpty(slots.getTotPrice())) {
            holder.cost.setText(df.format(Double.parseDouble(slots.getTotPrice())));
        }
//
//        try {
//            onClickInAdapter = (OnClickInAdapter) context;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(context.toString()
//                    + " must implement OnClickInAdapter");
//        }
//        onClickInAdapter.onAmountChanged(TotalCoast);

        holder.itemQuant.setText(String.valueOf(slots.getQuantity()));

        holder.addingImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double actualCost = Double.parseDouble(slotsList.get(position).getPrice());
                final MyViewHolder finalHolder = holder;
                if (finalHolder.itemQuant != null) {
                    CharSequence cs = finalHolder.itemQuant.getText();
                    number = Integer.parseInt(cs.toString());
                    number++;

                    finalHolder.itemQuant.setText(String.valueOf(number));
                    finalHolder.cost.setText(df.format(number * Double.parseDouble(String.valueOf(slots.getPrice()))));

                    slots.setTotQty(String.valueOf(finalHolder.itemQuant.getText()));
                    slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));

                    bucketDB=new BucketDB(context);
                    savedQtyList=bucketDB.getIndividualDetails(slots.getFoodType());
//                    TotalCoast += Double.parseDouble(slotsList.get(position).getPrice());
//                    onClickInAdapter.onAmountChanged(TotalCoast);
                    if (savedQtyList != null && savedQtyList.size() > 0) {
                        for (int i = 0; i < savedQtyList.size(); i++) {
                            savedItemSNoList.add(savedQtyList.get(i).getScid());
                        }
                        if(!savedItemSNoList.contains(slotsList.get(position).getScid())){
                            bucketDB.addChampcash(new SubCategoryList(slots.getScid(),slots.getId(),slots.getFoodType(),slots.getName(),slots.getItemDesc(),slots.getPrice(),slots.getfImageurl(),slots.getSerialNo(),String.valueOf(number),String.valueOf(number*Double.parseDouble(slots.getPrice()))));
                        }
                        else {
                            bucketDB.updateChampcash(slots.getScid(),String.valueOf(number),String.valueOf(number*Double.parseDouble(slots.getPrice())));
                        }
                    }
                    else {
                        bucketDB.addChampcash(new SubCategoryList(slots.getScid(),slots.getId(),slots.getFoodType(),slots.getName(),slots.getItemDesc(),slots.getPrice(),slots.getfImageurl(),slots.getSerialNo(),String.valueOf(number),String.valueOf(number*Double.parseDouble(slots.getPrice()))));
                    }
                    Intent i= new Intent(context,Basket.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
//                    slots.setQuantity(number);
//                    slots.setPrice(String.valueOf(number*actualCost));

//                    itemValues.add(slots);
                }
            }
        });

        holder.subImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double actualCost = Double.parseDouble(slotsList.get(position).getPrice());
                final MyViewHolder finalHolder = holder;
                if (finalHolder.itemQuant != null) {
                    CharSequence cs = finalHolder.itemQuant.getText();
                    number = Integer.parseInt(cs.toString());
                    number--;

                    if (number > 0) {

                        finalHolder.itemQuant.setText(String.valueOf(number));
                        holder.cost.setText(df.format(number * Double.parseDouble(slots.getPrice())));

                        slots.setTotQty(String.valueOf(finalHolder.itemQuant.getText()));
                        slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));
                        bucketDB=new BucketDB(context);
                        savedQtyList=bucketDB.getIndividualDetails(slots.getFoodType());
                        if (savedQtyList != null && savedQtyList.size() > 0) {
                            bucketDB.updateChampcash(slots.getScid(), String.valueOf(number), String.valueOf(number * Double.parseDouble(slots.getPrice())));
                        }

                    } else if (number == 0) {
                        finalHolder.itemQuant.setText(String.valueOf(number));
                        holder.cost.setText(df.format(actualCost));

                        slots.setTotQty(String.valueOf(finalHolder.itemQuant.getText()));
                        slots.setTotPrice(String.valueOf(finalHolder.cost.getText()));
                        bucketDB=new BucketDB(context);
                        bucketDB.removeChampcash(slotsList.get(position).getScid());
//                     TotalCoast -= Double.parseDouble(slotsList.get(position).getPrice());
//                        onClickInAdapter.onAmountChanged(TotalCoast);
                    }
                    Intent i= new Intent(context,Basket.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(i);
                }
            }
        });

        holder.removeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bucketDB=new BucketDB(context);
                bucketDB.removeChampcash(slotsList.get(position).getScid());
                Intent i= new Intent(context,Basket.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(i);
            }
        });


    }


    @Override
    public int getItemCount() {
        return slotsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView fItemName;
        TextView fItemNameDesc;
        TextView cost;
        TextView itemQuant;
        ImageView addingImage;
        ImageView subImage;
        ImageView removeImage;
        LinearLayout layout;
        View view1;

        public MyViewHolder(View view) {
            super(view);
            fItemName = (TextView) view.findViewById(R.id.itemType);
            fItemNameDesc = (TextView) view.findViewById(R.id.itemDesc);
            cost = (TextView) view.findViewById(R.id.money);
            itemQuant = (TextView) view.findViewById(R.id.itemQunt);
            addingImage = (ImageView) view.findViewById(R.id.addButton);
            subImage = (ImageView) view.findViewById(R.id.subButton);
            removeImage = (ImageView) view.findViewById(R.id.removeButton);
            view1 = view.findViewById(R.id.line2);
        }
    }

    public interface OnClickInAdapter {
        public void onAmountChanged(Double content);
    }

}