package app5.test5.bananaapps.snapeatsapp.activity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ShopList implements Parcelable {

    @SerializedName("CategoryList")
    @Expose
    private String CategoryList;

    @SerializedName("ShopAddress")
    @Expose
    private ArrayList<ShopAddress> shopAddress = new ArrayList<>();

    public ShopList(Double min, String shop_name, String mname, String shop_image, int sid, String s) {

    }


    public boolean getChainbusiness() {
        return chainbusiness;
    }

    public void setChainbusiness(boolean chainbusiness) {
        this.chainbusiness = chainbusiness;
    }

    @SerializedName("chainbusiness")
    @Expose
    private boolean chainbusiness;

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    @SerializedName("mid")
    @Expose
    private int mid;

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    @SerializedName("mname")
    @Expose
    private String mname;

    public String getShop_image() {
        return shop_image;
    }

    public void setShop_image(String shop_image) {
        this.shop_image = shop_image;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getSstatus() {
        return sstatus;
    }

    public void setSstatus(int sstatus) {
        this.sstatus = sstatus;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @SerializedName("shop_image")
    @Expose
    private String shop_image;

    @SerializedName("shop_name")
    @Expose
    private String shop_name;

    @SerializedName("sid")
    @Expose
    private int sid;

    @SerializedName("sstatus")
    @Expose
    private int sstatus;

    @SerializedName("website")
    @Expose
    private String website;

    private Double distance;
    private ArrayList<ShopAddress>  address;

    protected ShopList(Parcel in) {
        CategoryList = in.readString();
        shopAddress = in.createTypedArrayList(ShopAddress.CREATOR);
        mid = in.readInt();
        mname= in.readString();
        shop_image= in.readString();
        shop_name= in.readString();
        sid=in.readInt();
        distance=in.readDouble();
        sstatus=in.readInt();
        website= in.readString();
        address=in.createTypedArrayList(ShopAddress.CREATOR);
       }

    protected ShopList(Double in, String CategoryList,ArrayList<ShopAddress> shopAddress,Boolean chainbusiness,int mid,String mname,String shop_image,String shop_name, int sid,int sstatus,String website) {
       this.distance = in;
       this.CategoryList=CategoryList;
       this.shopAddress=shopAddress;
       this.chainbusiness=chainbusiness;
       this.mid=mid;
       this.mname=mname;
       this.shop_image=shop_image;
       this.shop_name=shop_name;
       this.sid=sid;
       this.sstatus=sstatus;
       this.website=website;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(CategoryList);
        dest.writeTypedList(shopAddress);
        dest.writeTypedList(address);
        dest.writeInt(mid);
        dest.writeString(mname);
        dest.writeString(shop_image);
        dest.writeString(shop_name);
        dest.writeInt(sid);
        dest.writeInt(sstatus);
        dest.writeString(website);
    }


    protected ShopList(Double in,String mname, String type, String shop_image,int sid, ArrayList<ShopAddress> shopAddress) {
        this.distance = in;
        this.shop_image=shop_image;
        this.mname=type;
        this.shop_name= mname;
        this.sid=sid;
        this.shopAddress=shopAddress;
    }
    protected ShopList(String type, String shop_image,Double in,int sid, ArrayList<ShopAddress> shopAddress) {
        this.distance = in;
        this.shop_image=shop_image;
        this.mname=type;
        this.shop_name= mname;
        this.sid=sid;
        this.shopAddress=shopAddress;
    }
    protected ShopList(Double in) {
        this.distance = in;
    }
    public String getCategoryList() {
        return CategoryList;
    }

    public void setCategoryList(String CategoryList) {
        this.CategoryList = CategoryList;
    }

    /**
     * @return The shopList
     */
    public ArrayList<ShopAddress> getShopAddress() {
        return shopAddress;
    }

    /**
     * @param shopAddress The MovieImages
     */
    public void setShopAddress(ArrayList<ShopAddress> shopAddress) {
        this.shopAddress = shopAddress;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDiatsance(Double diatsance) {
        this.distance = diatsance;
    }


    public static final Creator<ShopList> CREATOR = new Creator<ShopList>() {
        @Override
        public ShopList createFromParcel(Parcel in) {
            return new ShopList(in);
        }

        @Override
        public ShopList[] newArray(int size) {
            return new ShopList[size];
        }
    };

    public ArrayList<ShopAddress> getAddress() {
        return address;
    }

    public void setAddress(String diatsance) {
        this.address = address;
    }


    @Override
    public int describeContents() {
        return 0;
    }
}
