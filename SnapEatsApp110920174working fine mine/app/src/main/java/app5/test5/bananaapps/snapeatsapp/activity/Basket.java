package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.activity.BucketDB.BucketDB;
import app5.test5.bananaapps.snapeatsapp.activity.SetterGetterClasses.SubCategoryList;
import app5.test5.bananaapps.snapeatsapp.activity.payment.PaymentActivity;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;

/**
 * Created by BananaApps on 6/5/2017.
 */

public class Basket extends BaseActivity  implements BasketAdapter.OnClickInAdapter {
    public static final String PREFS_NAME = "SnapEatsVariables";
    SharedPreferences settings;
    TextView snapCode;
    Button checkout;
    List<SubCategoryList> subCategoryData;
    List<SubCategoryList> checkoutCategoryData;
    ImageView backArrow;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    TextView toolbar_title;
    TextView totalAmount;
    ImageView notification;
    RecyclerView basketData;
    BasketAdapter basketAdapter;
    BucketDB bucketDB;
    DecimalFormat df ;
    String intent;
    Double totalCostActivity=0.00;
    ProgressDialog progress;
    String userId;
    private List<SubCategoryList> savedQty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basket);
        bucketDB=new BucketDB(this);
        savedQty=bucketDB.getAllDetails();
        userId = (String) Cache.getData(CatchValue.USER_ID, Basket.this);
        if(savedQty.size()==0 || savedQty==null ){
            Intent sublist= new Intent(Basket.this,HomePage.class);
            sublist.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(sublist);
        }
        subCategoryData=new ArrayList<>();
        checkout = (Button)findViewById(R.id.checkOut);
       // snapCode = (TextView)findViewById(R.id.snapCode);
       // notification = (ImageView) findViewById(R.id.notification);

        totalAmount = (TextView) findViewById(R.id.totalAmount);
        basketData = (RecyclerView) findViewById(R.id.listBasket);
        backArrow = (ImageView) findViewById(R.id.backArrow);
        df = new DecimalFormat("0.00");

        toolbar_title = (TextView)findViewById(R.id.toolbar_title);
        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        toolbar_title.setTypeface(custom_font2);

        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String temp = settings.getString("promoCode","");
        if(temp.length()!=0)
            snapCode.setText(temp);

        try {
            // intent=getIntent().getStringExtra("from_class");
            intent=(String) Cache.getData(CatchValue.MENU_LIST, Basket.this);

           /* if(intent.equalsIgnoreCase("from_adapter")){
           subCategoryData = BasketAdapter.itemValues;
           onAmountChanged(totalCostActivity);
       }
       else {
           subCategoryData = SubCategoryDataAdapter.itemValues;
           for(int i=0;i<subCategoryData.size();i++){
               totalCostActivity+=Double.parseDouble(subCategoryData.get(i).getTotPrice());
           }
           totalAmount.setText(String.valueOf("$"+df.format(totalCostActivity+5)));
       }
        }catch (Exception e){
            e.printStackTrace();
        }*/
            subCategoryData = savedQty;
            for(int i=0;i<subCategoryData.size();i++) {

                if (subCategoryData.get(i).getTotPrice() == null) {
                    subCategoryData.get(i).setTotPrice("0");
                }
                totalCostActivity += Double.parseDouble(subCategoryData.get(i).getTotPrice());
            }
            totalAmount.setText(String.valueOf("£"+df.format(totalCostActivity+5)));
            Cache.putData(CatchValue.TOTAL_COST, getApplicationContext(), (totalCostActivity+5), Cache.CACHE_LOCATION_DISK);
        }catch (Exception e) {
            e.printStackTrace();
        }

/*        snapCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(getApplication(),"toast ",Toast.LENGTH_SHORT).show();
                Intent promo_code = new Intent(Basket.this, Snap_Promo_Code.class);
                promo_code.putExtra("from_class", "from_class");
                startActivity(promo_code);
                //   startActivity(new Intent(Basket.this, Snap_Promo_Code.class));
                //    finish();
            }
        });*/

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sublist= new Intent(Basket.this,SubCategory.class);
                Bundle bundle= new Bundle();
                bundle.putString("sublistype",intent);
                bundle.putString("fromClass","Basket");
                sublist.putExtra("BUNDLE",bundle);
                sublist.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(sublist);
            }
        });


        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(userId)){

                    Intent intent= new Intent(Basket.this, CheckOut.class);
                        intent.putExtra("fromProduct", "notfromProduct");
                    Cache.putData(CatchValue.ORDER_FROM, getApplicationContext(), "NotPhotoOder", Cache.CACHE_LOCATION_DISK);
                    startActivity(intent);

                  //  startActivity(new Intent(Basket.this, CheckOut.class));
                }
                else{
                    showCheckOutAlertDialog(Basket.this, "Snap Eats", "Want to CheckOut please Login to Continue.", false);
                }
            }
        });

        progress = new ProgressDialog(this);
        progress.setMessage("Please wait...");
        progress.show();
        progress.setCancelable(false);
        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                if (progress != null) {
                    progress.dismiss();
                }
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 3000);


        cd = new ConnectionDetector(Basket.this);
        isInternetPresent = cd.isConnectionAvailable();
        if (isInternetPresent) {
            getListData(savedQty);
        } else {
            ShowNoInternetDialog();
        }


    }


    private void getListData(List<SubCategoryList> subCategoryData) {
        if(subCategoryData.size()>0) {
            basketAdapter = new BasketAdapter(Basket.this, subCategoryData);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(Basket.this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            basketData.setLayoutManager(layoutManager);
            basketData.setItemAnimator(new DefaultItemAnimator());
            basketData.setAdapter(basketAdapter);
            basketAdapter.notifyDataSetChanged();
        }

    }

    public void ShowNoInternetDialog() {
        showAlertDialog(Basket.this, "No Internet Connection", "Please check your network.", false);
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    getListData(subCategoryData);
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }
    public void showCheckOutAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton2("Yes, please", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    Intent i=new Intent(Basket.this, Login.class);
                    i.putExtra("fromBasket","fromBasket");
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }
    @Override
    public void onResume() {
        super.onResume();
        //CurrentLongitude = settings.getString("CurrentData", "");

        //tLatLan=CurrentLongitude.substring(CurrentLongitude.indexOf("(")+1, CurrentLongitude.lastIndexOf(")"));
        // dLatitude=tLatLan.substring(0,tLatLan.indexOf(","));
        //dLongitude=tLatLan.substring(tLatLan.indexOf(",")+1,tLatLan.lastIndexOf(""));
    }


    @Override
    public void onAmountChanged(Double content) {
        totalAmount.setText(String.valueOf("£"+df.format(content+5)));
    }

    @Override
    public void onBackPressed() {
        Intent sublist= new Intent(Basket.this,SubCategory.class);
        Bundle bundle= new Bundle();
        bundle.putString("subtype",intent);
        Cache.putData(CatchValue.MENU_LIST, getApplicationContext(), intent, Cache.CACHE_LOCATION_DISK);
        bundle.putString("fromClass","Basket");
        sublist.putExtra("BUNDLE",bundle);
        sublist.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(sublist);
        finish();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
    }
}
