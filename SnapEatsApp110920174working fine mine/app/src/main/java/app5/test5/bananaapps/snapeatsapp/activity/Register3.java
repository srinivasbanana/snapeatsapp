package app5.test5.bananaapps.snapeatsapp.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app5.test5.bananaapps.snapeatsapp.R;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ConnectionDetector;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.Report;
import app5.test5.bananaapps.snapeatsapp.ServiceClass.ServiceClass;
import app5.test5.bananaapps.snapeatsapp.activity.session.Cache;
import app5.test5.bananaapps.snapeatsapp.activity.session.CatchValue;


/**
 * Created by BananaApps on 5/29/2017.
 */

public class Register3 extends BaseActivity {
    ImageView left;
    Button register3;
    EditText pwd,cnfpwd;
    String fName,lName,email,phone;
    TextView heading,subheading,endheading;
    Boolean isInternetPresent = false;
    ProgressDialog progressDialog;
    ConnectionDetector cd;
    String password;
    JSONObject resultJsonObject;
    String responseMessage;
    public static final String PREFS_NAME = "SnapEatsVariables";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register3);

        register3 = (Button) findViewById(R.id.register3);
        left = (ImageView) findViewById(R.id.left);
        pwd = (EditText)findViewById(R.id.password);
        cnfpwd = (EditText)findViewById(R.id.cnfpassword);

        heading = (TextView) findViewById(R.id.heading);
        subheading = (TextView) findViewById(R.id.Subheading);
        endheading  = (TextView) findViewById(R.id.endHeading);

        fName=(String) Cache.getData(CatchValue.FIRST_NAME, Register3.this);
        lName=(String) Cache.getData(CatchValue.LAST_NAME, Register3.this);
        email=(String) Cache.getData(CatchValue.EMAIL, Register3.this);
        phone=(String) Cache.getData(CatchValue.PHONE, Register3.this);
        cd = new ConnectionDetector(Register3.this);
        isInternetPresent = cd.isConnectionAvailable();

        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "Lato-Regular.ttf");
        pwd.setTypeface(custom_font);
        cnfpwd.setTypeface(custom_font);
        subheading.setTypeface(custom_font);
        endheading.setTypeface(custom_font);

        Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "MavenProLight-300.otf");
        heading.setTypeface(custom_font2);

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register3.this, Register2.class));
                finish();
            }
        });

        register3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!IsEmpty(pwd.getText().toString().trim()))
                {
                    okMessage("Error","Please enter Password");
                }
                else
                if(!IsEmpty(cnfpwd.getText().toString().trim()))
                {
                    okMessage("Error","Please enter Confirm password");
                }
                else
                    if (!pwd.getText().toString().trim().equals(cnfpwd.getText().toString().trim()))
                    {
                        okMessage("Error","Your password and confirmation password do not match");
                        pwd.setText("");
                        cnfpwd.setText("");
                        pwd.findFocus();
                    }
                else
                {
                    SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("logStatus","Login");
                    editor.putString("isAddressChange","false");
                    editor.commit();
//                    SharedPreferences.Editor editor2 = settings.edit();
//                    editor2.putString("sppassword",pwd.getText().toString().trim());
//                    editor2.putString("isAddressChange","false");
//                    editor2.putString("logStatus","MoreMenu");
//                    editor2.commit();
                    password=pwd.getText().toString().trim();
                    if (isInternetPresent) {
                        new RegisterTask().execute(fName,lName,email,phone,password);

                    } else {
                        ShowNoInternetDialog();
                    }

                }

            }
        });


    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(Register3.this, Register2.class));
        finish();
        super.onBackPressed();
    }


    private class RegisterTask extends AsyncTask<String,Void,Report> {

        Report response = new Report();

        @Override
        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected Report doInBackground(String... params) {
            try {
                JSONObject jObject = new JSONObject();
                jObject.put("fname", params[0]);
                jObject.put("lname",  params[1]);
                jObject.put("email", params[2]);
                jObject.put("phone",params[3]);
                jObject.put("password",params[4]);
                response = new ServiceClass().getJsonObjectResponse(jObject,"http://seapi.testersworld.com/api/user/Register");
            } catch (JSONException e) {
                showToastMessage("Server couldn't respond,Please try again");
            }
            return response;
        }

        @Override
        protected void onPostExecute(Report response) {
            dismissProgressDialog();
            if (response!=null){
                gerLoginResponse(response);
            }
            else {
                showToastMessage("Server couldn't respond,Please try again");
            }
        }
    }

    private void gerLoginResponse(Report response) {
        try {
            if (response.getStatus().equalsIgnoreCase("true")) {
                resultJsonObject = response.getJsonObject();
                if (resultJsonObject.getString("isSuccess").equalsIgnoreCase("true")) {
                    if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("200")) {
                        showToastMessage(resultJsonObject.getString("Message"));
                        Intent i = new Intent(Register3.this, HomePage.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                    }
                }
                else if(resultJsonObject.getString("isSuccess").equalsIgnoreCase("false")){
                    if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("400")){
                        responseMessage = resultJsonObject.getString("Message");
                        showToastMessage(responseMessage);
                    }
                    else if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("401")){
                        responseMessage = resultJsonObject.getString("Message");
                        showToastMessage(responseMessage);
                    }
                    else if (resultJsonObject.getString("StatusCode").equalsIgnoreCase("500")){
                        responseMessage = resultJsonObject.getString("Message");
                        showToastMessage(responseMessage);
                    }
                }
            }


            else {
                if ((response.getStatus().equalsIgnoreCase("false"))) {
                    responseMessage = response.getMessage();
                    showToastMessage("test22 "+resultJsonObject.getString("Message"));
                 /*   if (response.getErr_code() == 400) {
                        showToastMessage(resultJsonObject.getString("Message"));
                    }
                    else if (response.getErr_code() == 401) {
                        showToastMessage("Email already Registered");
                    } else if (response.getErr_code() == 500) {
                        showToastMessage(responseMessage);
                    } else {
                        showToastMessage(responseMessage);
                    }*/
                }
            }
        } catch (JSONException ex) {
            showToastMessage("Server couldn't respond,Please try again");
        }

    }

    public void ShowNoInternetDialog() {
        showAlertDialog(Register3.this, "No Internet Connection", "Please check your network.", false);
    }


    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setIcon((status) ? R.mipmap.ic_action_checked : R.mipmap.ic_action_warning);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                isInternetPresent = cd.isConnectionAvailable();
                if (isInternetPresent) {
                    new RegisterTask().execute(fName,lName,email,phone,password);
                } else {
                    ShowNoInternetDialog();
                }

            }
        });
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
        textView.setTextSize(16);
    }


    public void showProgressDialog() {
        progressDialog = new ProgressDialog(Register3.this);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
